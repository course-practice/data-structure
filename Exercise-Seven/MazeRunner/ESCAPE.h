#include <stdio.h>
#include <stdlib.h>

int getDegree(int i, int j, int m, int n)
{
	int degree = 4;
	if ( i == 0 ) degree -= 1;
	if ( j == 0 ) degree -= 1;
	if ( i == m ) degree -= 1;
	if ( j == m ) degree -= 1;
	return degree;
}

int validMove(int i, int j, int m, int n)
{
	if ( i < 0 || i >= m ) return 0;
	if ( j < 0 || j >= n ) return 0;
	return 1;
}
