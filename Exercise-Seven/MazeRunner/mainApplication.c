#include <stdio.h>
#include "STACK.h"
#include "ESCAPE.h"
#define M 10
#define N 10

// function prototype
void printMap(int mat[M][N]);
int findTheWayOut(int mat[M][N], Stack *path, Tiles *state );
int DFS( int mat[M][N], int explored[M][N], Stack *path, Tiles* departure, int goalState, int depth, int limit );
int IDS( int mat[M][N], int dRow, int dCol );
int main(void)
{
	Stack path;
	path.count = 0;
	path.sp = NULL;
	path.ep = NULL;
	int matrix[M][N] = {
		{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 0 }, 
		{ 2, 0, 1, 0, 1, 1, 0, 1, 0, 0 }, 
		{ 0, 0, 1, 0, 1, 0, 0, 1, 0, 0 }, 
		{ 1, 1, 1, 1, 1, 0, 0, 1, 1, 1 }, 
		{ 0, 1, 0, 0, 0, 1, 0, 1, 0, 1 },
		{ 1, 1, 1, 0, 0, 1, 1, 1, 0, 1 }, 
		{ 2, 0, 1, 0, 0, 1, 0, 0, 0, 1 }, 
		{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 1 }, 
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 3 }, 
		{ 0, 1, 0, 0, 0, 1, 0, 1, 1, 0 }
	};
	int i, j, outcome;
	scanf("%d%d", &i, &j);
	Tiles *tiles = createTile(i,j,'S');
	/*
	outcome = findTheWayOut(matrix, &path, tiles);
	if ( outcome == 1 )	{
		puts("ended");
		printPath(&path);
	}
	*/
	outcome = IDS( matrix, i, j );
	// else printf("not found\n");
	// puts("releasing memory");
	// destroyStack(&path);
	getchar();
	return 0;
}

int DFS( int mat[M][N], int explored[M][N], Stack *path, Tiles* departure, int goalState, int depth, int limit )
{
	int row, col, freedom, i;
	int actions[4][2] = {{1,0},{-1,0},{0,1},{0,-1}};
	if ( contains( path, departure r) ) return 0;
	if ( explored[departure->i][depature->j] == 1 )
	{
		printf("explored");
		explored[departure->i][departure->j] = 0;
		return 0;
	}
	explored[departure->i][departure->j] = 1;
	push( path, departure );
	if ( mat[departure->i][departure->j] == goalState )
	{
		printf("\nCongratulation!!!\n");
		return 1;
	}
	if ( depth >= limit ) 
	{
		explored[departure->i][departure->j] = 0;
		pop(path);
		return 0;
	}
	depth++;
	row = departure->i;
	col = departure->j;
	freedom = getDegree( row, col, M, N );
	if ( freedom >= 1 )
	{
		for ( i = 0; i < 4; i++ )
		{
			if ( validMove(row+actions[i][0], col+actions[i][1], M, N) && mat[row+actions[i][0]][col+actions[i][1]] != 0 ) 
			{
				printf("loc : <%d,%d>\n", row+actions[i][0], col+actions[i][1]);
				Tiles *current = createTile(row+actions[i][0], col+actions[i][1],'1');
				if ( DFS( mat, explored, path, current, 3, depth, limit ) == 1 ) return 1;
				// push(&branches, createBranch(row+actions[i][0], col+actions[i][1]));
			}
		}
		pop(path);
		explored[row][col] = 0;
	}
	return 0;
}
int IDS( int mat[M][N], int dRow, int dCol )
{
	Tiles *departure = NULL;
	Stack path; path.count = 0; path.ep = NULL; path.sp = NULL;
	int goalState = 3, probe, limit = 100, status, i, j; 
	int explored[M][N] = { 0 };
	for ( probe = 1; probe < limit; probe++ )
	{
		departure = createTile( dRow, dCol, '1' );
		status = DFS( mat, explored, &path, departure, goalState, 0, probe );
		if ( status )
		{
			printMap(explored);
			printPath(&path);
			return 1;
		}
		destroyStack(&path); path.ep = NULL; path.sp = NULL; path.count = 0;
		for ( i = 0; i < M; i++ )
		{
			for ( j = 0; j < N; j++ )
			{
				explored[i][j] = 0;
			}
		}
	}
}
int findTheWayOut(int mat[M][N], Stack *path, Tiles *state )
{
	int visited[M][N] = {0}, Found = 0, row = state->i, col = state->j, i, j, freedom;
	int actions[4][2] = {{1,0},{-1,0},{0,1},{0,-1}};
	Stack branches;
	branches.count = 0;
	branches.ep = NULL;
	branches.sp = NULL; 
	Tiles *current;
	char f = '1';
	if ( !validMove(row, col, M, N) || mat[row][col] != 2 ) { printf("invalid entrance"); return 0;	}
	while ( !Found )
	{
		printf("loop\n");
		if ( visited[row][col] == 0 && mat[row][col] != 0 )
		{
			visited[row][col] = 1;
			printMap(visited);
			current = createTile(row, col, f);
			push(path, current);
			puts("latest path");
			printPath(path);
			if ( mat[row][col] == 3 ) {
				printf("FOUND!!!\n");
				if ( branches.count > 0 ){				
					destroyStack(&branches);
					printPath(&branches);
					puts("ENDED");
				}
				else
					puts("what else");
				puts("dlt finish");
				return 1;
			}
			freedom = getDegree(row, col, M, N);
			printf("freedom : %d\n", freedom);
			if ( freedom >= 1 )
			{
				for ( i = 0; i < 4; i++ )
				{
					if ( validMove(row+actions[i][0], col+actions[i][1], M, N) && mat[row+actions[i][0]][col+actions[i][1]] != 0 ) 
					{
						printf("loc : <%d,%d>\n", row+actions[i][0], col+actions[i][1]);
						push(&branches, createBranch(row+actions[i][0], col+actions[i][1]));
					}
				}
				if ( freedom > 1 )
					branches.ep->f = 'B';
				printf("branch size : %d\n", branches.count);
				printPath(&branches);
				//getchar();
				current = pop(&branches);
				row = current->i;
				col = current->j;
				f = current->f;
				free(current);
			}
			if ( freedom == 1 )
			{
				current = pop(&branches);
				row = current->i;
				col = current->j;
				f = '1';
				free(current);
			}
			else if ( freedom == 0 )
			{ 
				f = '1';
				while ( path->ep->f != 'B' )
				{
					current = pop(path);
					printf("poping <%d,%d>\n", current->i, current->j);
					if ( path->count == 0 )
					{
						puts("NO WAY OUT!!!");
						return 0;
					}
					free(current);
				}
				current = pop(path);
				free(current);
				current = pop(&branches);
				row = current->i;
				col = current->j;
			}
		}
		else
		{
			current = pop(path);
			free(current);
			if ( branches.count > 0 )
			{
				current = pop(&branches);
				row = current->i;
				col = current->j;
				f = 'B';
			}
			else puts("out of branche");
		}
	}
	destroyStack(&branches);
	return 0;
}
void printMap(int mat[M][N])
{
	int i, j;
	for ( i = 0; i < M; i++ )
	{
		for ( j = 0; j < N; j++ )
			printf(" %d ", mat[i][j]);
		puts("");
	}
}















