typedef struct TILES{
	int i, j;
	char f;
	struct TILES *prev, *next;
}Tiles;

typedef struct STACK{
	int count;
	Tiles *sp, *ep;
}Stack;

Stack *createStack()
{
	Stack *s = (Stack*)malloc(sizeof(Stack));
	if ( s != NULL )
	{
		s->count = 0;
		s->ep = NULL;
		s->sp = NULL;
	}
	return s;
}
void push(Stack *stack, Tiles *tiles)
{
	if ( stack->count == 0 )
		stack->sp = tiles;
	else
	{
		stack->ep->next = tiles;
		tiles->prev = stack->ep;
	}
	stack->ep = tiles;
	stack->count += 1;	
}

Tiles* pop(Stack *stack)
{
	Tiles *tail = stack->ep;
	stack->ep = stack->ep->prev;
	if ( stack->count == 1 )
		stack->sp = NULL;
	stack->count -= 1;
	return tail;
}
void destroyStack(Stack *stack)
{
	if ( stack->count != 0 )
	{
		// Tiles *current = stack->sp;
		Tiles *dltLink;
		while ( stack->count != 0 )
		{
			dltLink = pop(stack);
			free(dltLink);
		}
		puts("fad");
	}
	else
		puts("nothing to delete");
}

Tiles *createTile(int i, int j, char f)
{
	Tiles *t = (Tiles*)malloc(sizeof(Tiles));
	if ( t != NULL )
	{
		t->f = f;
		t->i = i;
		t->j = j;
		t->next = NULL;
		t->prev = NULL;
	}
	return t;
}
Tiles *createBranch(int i, int j)
{
	Tiles *t = (Tiles*)malloc(sizeof(Tiles));
	if ( t != NULL )
	{
		t->i = i;
		t->j = j;
		t->next = NULL;
		t->prev = NULL;
	}
	return t;
}

char peek(Stack *stack)
{
	return stack->ep->f;
}

void printPath(Stack *stack)
{
	Tiles *current = stack->sp;
	while ( current != NULL )
	{
		printf("<%d,%d>\n", current->i, current->j);
		current = current->next;
	}
	printf("EOP\n");
}

int contains( Stack *stack, Tiles *departure )
{
	Tiles *current = stack->sp;
	while ( current != NULL )
	{
		if ( current->i == departure->i && current->j == departure->j ) return 1;
		current = current->next;
	}
	return 0;
}
