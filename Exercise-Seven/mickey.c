#include <stdio.h>
#include <stdlib.h>
#define M 5
#define N 5

// struct definition
typedef struct NODE{
	int i, j;
	char flag;
	struct node *prev, *next;
}Node;
typedef struct STACK{
	int count;
	Node *surface, *start;
}Stack;
typedef struct POINTER{
	void *address;
	struct POINTER *prev, *next;
}Pointer;
typedef struct REGISTER{
	Pointer *fp, *bp, *sp;
}Process;

// function prototype
void printMap(int mat[M][N]);
int peek(int mat[M][N], int i, int j);
int getDegree(int i, int j);
int validMove(int i, int j);
Node *createNode(int i, int j, char f);
Node *pop(Stack *stack);
void push(Stack *stack, Node *node);
Stack *createStack();
Stack *releaseStack(Stack *stack);
Process *createProcess();
Process *destroyProcess(Process *proc);
void printMap(int mat[M][N]);
int escape(int mat[M][N], Stack *stack, Process *proc, int i, int j);

int main(void) 
{
	int matrix[M][N] = {
		{ 1, 1, 1, 1, 1 }, // 0
		{ 2, 0, 0, 0, 1 }, // 1
		{ 1, 1, 0, 1, 1 }, // 2
		{ 1, 1, 0, 0, 3 }, // 3
		{ 1, 1, 0, 1, 1 } // 4
	};
	
	printMap(matrix);
	Stack *path = createStack();
	Process *proc = createProcess();
	int i, j;
	scanf( "%d%d", &i, &j );
	int outcome = escape( matrix, &path, &proc, i, j );
	getchar();
	return 0;
}
//
void updateProc(Process *proc, Node *newNode)
{
	if ( proc->bp == NULL ) proc->bp = newNode;
	if ( newNode->flag == '0' )
	{
		
	}
}
//
int escape(int mat[M][N], Stack *stack, Process *proc, int i, int j)
{
	if ( mat[i][j] == 1 ) return 0;
	Node *newNode = createNode(i, j,'X');
	Node *tempNode = NULL;
	int visited[M][N] = {0};
	int FOUND = 0, row = i, column = j, index;
	const int action[2][2] = {{1,0},{-1,0},{0,1},{0,-1}};
	
	if( newNode != NULL )
	{
		push(stack, &newNode);
		while ( !FOUND )
		{
			
			if ( visited[row][column] != 0 )
			{
				Node *steps = createNode(row, column, 'W');
				push(stack, &steps);
				if ( mat[row][column] == 3 ) return 1;
				visited[row][column] = 1; // mark as visited
				int df = getDegree(row, column);
				if ( df > 1 )
				{
					// branch
					for ( index = 0; index < 4; index++ )
					{
						if ( validMove( row + action[index][0], column + action[index][1] ) )
						{
							Node *branches = createNode( row + action[index][0], column + action[index][1], '0' );
							updateProc(proc, &branches);
						}
					}
					
				}
				else if ( df == 1 )
				{
					// continuous
					
				}
				else
				{
					// dead end
					
				}
			}
			else
			{
				puts("previously visited location");
			}
			
		}		
		return 1;
	}
	return 0;
}
//
int peek(int mat[M][N], int i, int j)
{
	return mat[i][j];
}
//
int getDegree(int i, int j)
{
	int freedom = 4;
	if ( i == 0 ) freedom -= 1;
	if ( i == ( M-1 ) ) freedom -= 1;
	if ( j == 0 ) freedom -= 1;
	if ( j == ( N-1 ) ) freedom -= 1;
	return freedom;
}
//
int validMove(int i, int j)
{
	if ( i < 0 || i >= M ) return 0;
	if ( j < 0 || j >= N ) return 0;
	return 1;
}
//
Node *createNode(int i, int j, char f)
{
	Node *step = (Node*)malloc(sizeof(Node));
	if ( step != NULL )
	{
		(*step).i = i;
		(*step).j = j;
		(*step).flag = f;
		(*step).prev = NULL;
		(*step).next = NULL;
	}
	return step;
}
Node *pop(Stack *stack)
{
	Node *step = (*stack).surface;
	(*stack).surface = (*step).prev;
	(*stack).count -= 1;
	if ( (*stack).count == 0 )
		(*stack).start = NULL;
	return step;
}
//
void push(Stack *stack, Node *node)
{
	if ( (*stack).count == 0 )
		(*stack).start = node;
	else
		(*(*stack).surface).next = node;
	(*node).prev = (*stack).surface;
	(*stack).surface = node;
	(*stack).count += 1;
}
//
Stack *createStack()
{
	Stack *s = (Stack*)malloc( sizeof( Stack ) );
	if ( s != NULL )
	{
		s->count = 0;
		s->start = NULL;
		s->surface = NULL;
	}
	return s;
}
//
Stack *releaseStack(Stack *stack)
{
	Node * current = (*stack).surface, *dltNode;
	while ( current != NULL )
	{
		dltNode = current;
		current = (*current).next;
		free(dltNode);
	}
	free(stack);
	return NULL;
}
//
Process *createProcess()
{
	Pointer *proc = (Pointer*)malloc(sizeof(Pointer));
	if ( proc != NULL )
	{
		(*proc).address = NULL;
		(*proc).bp = NULL;
		(*proc).fp = NULL;
		(*proc).sp = NULL;
	}
	return proc;
}
//
Process *destroyProcess(Process *proc)
{
	Node *step = (*proc).bp, *dltStep;
	while ( step != NULL )
	{
		dltStep = step;
		step = (*step).next;
		free(dltStep);
	}
	free(proc);
	return NULL;
}
//
void printMap(int mat[M][N])
{
	int i, j;
	for ( i = 0; i < M; i++ )
	{
		for ( j = 0; j < N; j++ )
		{
			switch(mat[i][j])
			{
				case 0:
					printf(" + ");
					break;
				case 1:
					printf(" * ");
					break;
				case 2:
					printf(" S ");
					break;
				case 3:
					printf(" E ");
					break;
			}
		}
		puts("");
	}
}
//
