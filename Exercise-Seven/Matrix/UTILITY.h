#define SIZE 20 

void printMat(int mat[SIZE][SIZE])
{
	int i, j;
	for ( i = 0; i < SIZE; i++ )
	{
		for ( j = 0; j < SIZE; j++ )
			printf("%4d ", mat[i][j]);
		puts("\n");
	}
}
