// FindLeastCost(departure, destination, &pathway, dirMat, visited, closestCity, &leastCost); // obsolete
// int FindLeastCost(int departure, int destination, Stack *path, int mat[SIZE][SIZE], int visited[SIZE], int closestCity[SIZE][2], int *leastCost);
// int DFS(int departure, int destination, Stack *path, int mat[SIZE][SIZE], int visited[SIZE], int closestCity[SIZE][2], int *leastCost);
/* 
int FindLeastCost(int departure, int destination, Stack *path, int mat[SIZE][SIZE], int visited[SIZE], int closestCity[SIZE][2], int *leastCost)
{
	return DFS(departure, destination, path, mat, visited, closestCity, leastCost);
}

int DFS(int current, int destination, Stack *path, int mat[SIZE][SIZE], int visited[SIZE], int closestCity[SIZE][2], int *leastCost)
{
	int i, status, flag = 0;
	printf("current : %d\n", current);
	if ( visited[current] == 1 )
	{
		printf("visited\n");
		return 0;
	}
	// push to path
	push(path, createCity(current, '1'));
	if ( current == destination )
	{
		// update cost
		if ( closestCity[current][1] < *leastCost ) *leastCost = closestCity[current][1];
		int c = pop(path);
		printf("Found: %4d\n", c);
		visited[current] = 0;
		return 1;
	}
	visited[current] = 1;
	for ( i = 0; i < SIZE; i++ )
	{
		if ( mat[current][i] != 0 && current != i )
		{
			if ( closestCity[i][1] == 0 || closestCity[current][1] + mat[current][i] < closestCity[i][1] )
			{
				closestCity[i][0] = current;
				closestCity[i][1] = closestCity[current][1] + mat[current][i];
			}
			else if ( closestCity[i][1] > *leastCost )
			{
				printf(">> This path is too costly\n");
				continue;
			}
			status = DFS( i, destination, path, mat, visited, closestCity, leastCost );
			if ( status ) flag = 1;
		}
	}
	if ( flag ) return 1;
	// pop path
	int c = pop(path);
	printf("Failed: %4d\n", c);
	visited[current] = 0; 
	return 0;
}
*/ 
