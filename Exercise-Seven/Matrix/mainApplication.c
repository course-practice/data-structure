#include <stdio.h>
#include <stdlib.h>
#include "RF.h"
#include "UTILITY.h"
#include "STACK.h"
// self define variable
#define SIZE 20 
// function prototype
int findPath( int departure, int destination, Stack *pathway, int mat[SIZE][SIZE], int closestCity[SIZE][2] );
// main program
int main(void)
{
	// char *path = "data.txt";
	// char *path = "copy.txt";
	char *path = "test.txt";
	int dirMat[SIZE][SIZE] = { 0 };
	// int visited[SIZE] = { 0 };
	int closestCity[SIZE][2] = { 0 };
	int leastCost = INT_MAX, m;
	Stack pathway; initializer(&pathway);
	// Read input from file
	readData(dirMat, path, SIZE); printMat(dirMat); // debugger for file input
	int departure, destination;
	printf("%20s", ">> Departure City : ");
	scanf("%d", &departure);
	while ( departure < 0 || departure >= SIZE )	scanf("%d", &departure);
	printf("%20s", ">> Destination City : ");
	scanf("%d", &destination);
	leastCost = findPath(departure, destination, &pathway, dirMat, closestCity);
	// found
	if ( leastCost < INT_MAX )
	{
		printf("\n\nLeast Cost : %4d\n", leastCost);
		push(&pathway, createCity(destination,'1'));
		m = destination;
		while ( m != departure )
		{
			m = closestCity[m][0];	
			push(&pathway,createCity(m,'1'));	
		}
		// output the shortest path
		printPath(pathway);
	}
	else
		printf("\n\n>> No solution was found\n");
	getchar();
	return 0;
}
int getLeastCost(int closestCity[SIZE][2], int mat[SIZE][SIZE], int departure, int destination)
{
	puts("fadfad");
	int cost = 0, c, p;
	c = destination;
	while ( c != departure )
	{
		p = c;
		c = closestCity[c][0];
		printf("\n%d -> %d = %d\n", c, p, mat[c][p]);
		cost += mat[c][p];
	}
	return cost;
}
int findPath(int departure, int destination, Stack *pathway, int mat[SIZE][SIZE], int closestCity[SIZE][2] )
{
	int visited[SIZE] = { 0 };
	int current, leastCost = INT_MAX, flag, i;
	push(pathway, createCity(departure, '1')); // push the departure to the stack
	City *c;
	while ( 1 ) // pathway->count != 0
	{
		current = pathway->sp->index; // peek at the top of the stack
		printf("\nCurrent : %d\n", current);
		c = pathway->sp;
		printf("\n@");
		while ( c != NULL )
		{
			if ( c->tag != 'B' ) // c->tag != 'B'
				printf("%2d >> ", c->index );
			c = c->link;
		} printf("TOP\n");
		if ( visited[current] == 1 )
		{
			printf( ">> Visited : %d\n", pop(pathway) ); // one step backward
			// pop(pathway);
			if ( pathway->count == 0 ) break; // empty stack, escape criteria
			continue;
		}
		if ( current == destination )
		{
			leastCost = getLeastCost(closestCity, mat, departure, destination);
			// if ( closestCity[current][1] < leastCost ) leastCost = closestCity[current][1];
			printf(">> FOUND at cost : %d, %d\n", closestCity[current][1], closestCity[current][1] );
			visited[current] = 0;
			if ( BackTracking(pathway, visited) == 0 ) break; // empty stack, escape criteria
			else
				pathway->sp->tag = '1'; // swith mode from branch to continuous
			continue;
		}
		visited[current] = 1;
		flag = 0;
		for ( i = 0; i < SIZE; i++ )
		{	// explore possible action
			if ( mat[current][i] != 100 && i != current && i != departure )
			{
				printf("> Child : %d\n", i);
				if ( closestCity[i][1] == 0 || closestCity[current][1] + mat[current][i] < closestCity[i][1] )
				{	// found a shorter a way to reach child city
					printf(">> update cost : %d -> %d\n", closestCity[i][1], (closestCity[current][1] + mat[current][i]) );
					closestCity[i][0] = current;
					closestCity[i][1] = closestCity[current][1] + mat[current][i];
				}
				else if ( closestCity[i][1] + mat[current][i] >= leastCost ) 
				{	// this child city will not be added to the stack since the cost is expected to be higher than the least cost
					printf(">> expected Cost is too high : %4d, %4d\n", (closestCity[i][1] + mat[current][i]), leastCost );
					continue;
				}
				printf(">> pushing %d to stack\n", i);
				push(pathway, createCity(i, 'B')); // push child city to stack, mark each as 'B' (branch)
				flag = 1; // mark as 1 if at least 1 child city is added
			}
		}
		// printf("flag : %d, size : %d\n", flag, pathway->count ); // debugger
		if ( flag == 0 )
		{
			printf(">> No new city was added\n");
			visited[current] = 0;
			if ( BackTracking(pathway, visited) == 0 ) break; // empty stack, escape criteria
		}
		pathway->sp->tag = '1'; // swith mode from branch to continuous
		// getchar(); 
	}
	if ( leastCost != INT_MAX ) leastCost = getLeastCost(closestCity, mat, departure, destination );
	return leastCost; // INT_MAX if not found
}
