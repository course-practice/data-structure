typedef struct CITY{
	int index;
	char tag;
	struct CITY *link;
}City;
typedef struct STACK{
	int count;
	City *sp, *ep;
}Stack;

void initializer(Stack *stack)
{
	stack->count = 0;
	stack->ep = NULL;
	stack->sp = NULL;
}

City *createCity( int index, char tag )
{
	City *c = (City*)malloc(sizeof(City));
	if ( c != NULL )
	{
		c->index = index;
		c->tag = tag;
		// c->link = NULL;
	}
	return c;
}

int pop(Stack *stack)
{
	City *current = stack->sp;
	int c = current->index;
	stack->sp = stack->sp->link;
	free( current );
	stack->count -= 1;
	return c;
}

void push(Stack *stack, City *city )
{
	city->link = stack->sp;
	stack->sp = city;
	stack->count += 1;
}

void printPath(Stack stack)
{
	printf(">> Solution\n");	
	City *current = stack.sp;
	while ( current != NULL )
	{
		printf("%d >> ", current->index );
		current = current->link;	
	}
	printf(">> End Of Solution\n");	
}

int BackTracking(Stack *stack, int visited[SIZE] )
{
	printf("Backtracking >> ");
	while ( stack->count != 0 && stack->sp->tag != 'B' )
	{
		int k = pop(stack);
		visited[k] = 0;
		printf("%d >> ", k );
	}
	return stack->count;
}

