#include <stdio.h>
#include <stdlib.h>
#define N 256

/*
	test-case :
		� Invalid expression: ((2 + 3) * 5 -4/3
		� Invalid expression: ))2 + 3(( * 5 -4/3
		� Invalid expression: ()()2 + 3 * 5 -4/3
		� Invalid expression: 2 + (3 * ) 5 -4/3
*/

// structure definition
struct listNode{
	int id;
	struct listNode *next;
	struct listNode *prev;
};
typedef struct listNode ListNode;
typedef ListNode *Node;

struct stackTag{
	int count;
	Node head;
	Node tail;
};
typedef struct stackTag StackTag;
typedef StackTag *Stack;

// function prototype
Stack createStack();
void destroyStack();

int push(Stack *stack, int id);
int pop(Stack *stack);
int peek(Stack *stack);
int parser(Stack *stack, char expression[], int size);
int main(void)
{
	char expression[N] = { 0 };
	Stack stack = createStack();
	scanf("%256[^\n]s", expression);
	printf("input : %s\n", expression);
	printf("#result : %d", parser(&stack, expression, N));
	
	destroyStack(&stack);
	getchar();
	return 0;	
} 

Stack createStack()
{
	Stack stack = (Stack)malloc(sizeof(StackTag));
	if ( stack != NULL )
	{
		stack->count = 0;
		stack->head = NULL;
		stack->tail = NULL;
	}
	return stack;
}

void destroyStack(Stack *stack)
{
	while ( (*stack)->count != 0 )
	{
		pop(stack);
	}
	free(stack);
}

int push(Stack *stack, int id)
{
	Node node = (Node)malloc(sizeof(ListNode));
	if ( node != NULL )
	{
		node->id = id;
		node->next = NULL;
		if ( (*stack)->count > 0 )
		{
			node->prev = (*stack)->tail;
			(*stack)->tail->next = node;
		}
		else
		{
			node->prev = NULL;
			(*stack)->head = node;
		}
		(*stack)->tail = node;
		((*stack)->count)++;
	}
	return (*stack)->count;
}

int pop(Stack *stack)
{
	int id = 0;
	if ( (*stack)->count > 0 )
	{
		Node node = (*stack)->tail;
		if ( (*stack)->count > 1 )
		{
			(*stack)->tail = node->prev;
		}
		else
		{
			(*stack)->head = NULL;
			(*stack)->tail = NULL;
		}
		id = node->id;
		free(node);
		((*stack)->count)--;
	}
	return id;
}

int peek(Stack *stack)
{
	int id = 0;
	if ( (*stack)->count > 0 )
		id = (*stack)->tail->id;
	return id;
}

void print(Stack *stack){
	// puts("print()");
	Node node = (*stack)->head;
	while( node != NULL ){
		printf("%c", node->id);
		node = node->next;
	}
} 

int parser(Stack *stack, char expression[], int size)
{
	int i, prevId, true=1, false=0,stat = true;
	Stack OpStack = createStack();
	
	for ( i = 0; i < size && expression[i] != 0; i++ )
	{
		printf("r : %d || char : %c\n", i, expression[i]);
		if ( expression[i] >= 97 && expression[i] <= 122 )
			continue;
		prevId = peek(&OpStack);
		printf("peek : %d\n", prevId);
		if ( prevId == 0 )
		{
			if ( expression[i] == 41 )
			{
				stat = false;
				break;
			}
			puts("first op");
			push(&OpStack, expression[i]);	 
			continue;
		}
		if ( prevId == 40 )
		{
			if ( expression[i] == 41 && expression[i-1] == 40 )
			{
				stat = false;
				break;	
			}
			push(&OpStack, expression[i]);
			continue;
		}
		if ( expression[i] == 43 || expression[i] == 45 )
		{
			while ( OpStack->count > 0 )
			{
				if ( peek(&OpStack) != 40 )
					push(stack, pop(&OpStack));
				else
					break;
			}
			push(&OpStack, expression[i]);
			continue;
		}
		if ( expression[i] == 42 || expression[i] == 47 )
		{
			if ( prevId == 42 || prevId == 47 )
				push(stack, pop(&OpStack));
			push(&OpStack, expression[i]);
			continue;
		}
		// right bracket
		puts("right bracket");
		/*
		if ( expression[i-1] < 97 )
		{
			stat = false;
			break;
		}
		*/
		puts("clearing up");
		print(&OpStack);
		printf("\nsize : %d\n", OpStack->count);
		while ( OpStack->count > 0 )
		{
			printf("clearing : %d\n", peek(&OpStack));
			if ( peek(&OpStack) != 40 )
				push(stack, pop(&OpStack));
			else
			{
				puts("kill left bracket");
				pop(&OpStack);
				break;	
			}
		}
		printf("\nsize : %d\n", OpStack->count);
	}
	puts("end for");
	if ( stat == 1)
	{
		while ( OpStack->count > 0 )
		{
			printf("clearing : %d\n", peek(&OpStack));
			if ( peek(&OpStack) != 40 )
				push(stack, pop(&OpStack));
			else
			{
				stat = false;
				break;
			}
		}
	}

	
	destroyStack(&OpStack);
	return stat;
}
