int writeHeader(char *path, Queue *queue, int len)
{
	FILE *fptr;
	if ( (fptr = fopen(path, "wb")) == NULL )
	{
		printf("\n>> Exception >>\n");
		return 0;
	}
	int offset = queue->size; printf("Offset : %d\n", offset);
	Block *current = queue->top;
	// top row
	fwrite(&len, 4, 1, fptr);
	fwrite(&offset, 1, 1, fptr);
	// frequency table
	while ( current != NULL )
	{
		char alpha = current->node->a.alpha; // printf("%c", alpha);
		int freq = current->node->a.freq; // printf("%d\n", freq);
		fwrite(&alpha, 1, 1, fptr);
		fwrite(&freq, 4, 1, fptr);
		current = current->link;
	}
	fclose(fptr);
	return 1;
}

int readHeader(char *path, Queue *queue, int *len, char buffer[])
{
	FILE *fptr;
	if ( (fptr = fopen(path, "rb")) == NULL )
	{
		printf("\n>> Exception >>\n");
		return 0;
	}
	char byte, data, temp = 0;
	const zero = 00000000; const one = 00000001; 
	int seq, os, bi;
	seq = os = 0;
	// top row
	printf("<HEADER>\n");
	fread(len, 4, 1, fptr);	printf("DOCLEN : %d\n", *len);
	fread(&byte, 1, 1, fptr); os |= byte; printf("OFFSET : %d\n", (int)byte);
	// frequency table
	while ( seq <  os )
	{
		char alpha; unsigned int freq;
		fread(&alpha, 1, 1, fptr);
		fread(&freq, 4, 1, fptr); // printf("%c %u\n", alpha, freq);
		seq++;
		os |= byte;
		enqueue(queue, createNode(alpha, freq, 1));
		// orderedInsert(queue, createNode(alpha, freq, 1));
	}
	puts("</Header>\n");
	bi = 0;
	while ( fread(&data, 1, 1, fptr ) == 1 )
	{
		// printf("%c", data); 
		// printf("%d\n", (int)data);
		for ( seq = 0; seq < 8; seq++ )
		{
			temp |= data;
			temp >>= 7;
			// temp <<= seq;
			// temp >>= ( 8 - seq )+ 1;
			data <<= 1;
			if ( (int)temp*-1 == (int)one )
				buffer[bi] = '1';
			else
				buffer[bi] = '0';
			//printf("%d ", (int)temp*-1);
			//printf("%d\n", (int)data);
			bi++; temp &= zero;
		}
		//getchar();
	}
	// for ( seq = 0; seq < bi; seq++ ) printf("%c", buffer[seq]);
	
	os = byte | os;
	return bi;
}

int writeByte(char *path, char byte)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "ab")) == NULL )
	{
		printf("\n>> Exception >>\n");
		return 0;
	}
	fwrite(&byte, 1, 1, fptr);
	fclose(fptr);
	return 1;
}

int readFile( char *path, char buffer[], int buflen)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "r")) == NULL )
	{
		printf("\n>> Exception >>\n");
		return 0;
	}
	int seq, space = 0, slen = 0; char byte;
	for ( seq = 0; seq < buflen; seq++ )
	{
		if ( fread(&buffer[seq], 1, 1, fptr) == 0 ) break;
		if ( buffer[seq] == 0  ) {
			if ( ++space > 7 ) break;
		}
		else space = 0;
		slen++; // printf("%c", buffer[seq]);
	} // puts("");
	// slen -= 7;
	fclose(fptr);
	return slen;
}

int writeFile( char *path, char buffer[], int buflen)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "wb")) == NULL )
	{
		printf("\n>> Exception >>\n");
		return 0;
	}
	int seq;
	for ( seq = 0; seq < buflen; seq++ )
		fwrite(&buffer[seq], 1, 1, fptr);
	return 1;
}
