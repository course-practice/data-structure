typedef struct CHARACTER{
	char alpha;
	unsigned int freq;
}Alpha;

typedef struct NODE{
	char t;
	Alpha a;
	struct NODE *left, *right;
}Node;

typedef struct BLOCK{
	Node *node;
	struct BLOCK *link;
}Block;

typedef struct QUEUE{
	int size;
	Block *top, *tail;
}Queue;

// Initializer
void initQ(Queue *queue)
{
	if ( queue != NULL )
	{
		queue->size = 0;
		queue->top = NULL;
	}
}
// creator
Node *createNode(char a, unsigned int f, char t)
{
	Node *node = (Node*)malloc(sizeof(Node));
	if ( node != NULL )
	{
		node->a.alpha = a;
		node->a.freq = f;
		node->t = t;
		node->left = node->right = NULL;
	}
	return node;
}

Block *createBlock(Node *node)
{
	Block *block = (Block*)malloc(sizeof(Block));
	if ( block != NULL )
	{
		block->link = NULL;
		block->node = node;
	}
	return block;
}

Node *dequeue(Queue *queue)
{
	Block *block = queue->top;
	if ( queue->size == 1 )
	{
		queue->top = NULL;
		queue->tail = NULL;
	}
	else queue->top = queue->top->link;
	queue->size -= 1;
	Node *node = block->node;
	free(block);
	return node;
}
void enqueue(Queue *queue, Node *node)
{
	Block *block = createBlock(node);
	if ( queue->size > 0 ) queue->tail->link = block;
	else queue->top = block;
	queue->size += 1;
	queue->tail = block;
}
void orderedInsert(Queue *queue, Node *node)
{
	Block *newBlock = createBlock(node);
	Block *current = queue->top, *prev = NULL;
	while ( current != NULL && current->node->a.freq < node->a.freq )
	{
		prev = current;
		current = current->link;
	}
	if ( prev == NULL )
	{
		newBlock->link = queue->top;
		queue->top = newBlock;
	}
	else
	{
		prev->link = newBlock;
		newBlock->link = current;
	}
	queue->size += 1;
}

void printQueue(Queue *queue)
{
	Block *current = queue->top;
	int i = 0;
	while ( current != NULL )
	{
		printf("%c -> %4d%s", current->node->a.alpha, current->node->a.freq, (((i+1)%5==0)?"\n":"\t"));
		current = current->link; i++;
	} puts("#");
}

