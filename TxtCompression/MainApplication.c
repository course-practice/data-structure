#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
// constant variable
#define CODE 256
#define BUFLEN 100000
#define BINLEN 1000000
// self define library
#include "STRUCTURE.h"
#include "RFILE.h"
// function prototype
void getFrequency(int fTable[CODE], char *doc, int len);
Queue *buildPriorityQueue(int fTable[CODE]);
Node *buildHuffmanTree(Queue *queue);
int mapper(Node *root, char target, char buffer[9], int index);
void encoding(char *path, char *doc, int slen, Node *root );
int decoding(char data[BINLEN], int blen, int clen, Node *root, char dest[BUFLEN]);

void inOrder(Node *root)
{
	if ( root == NULL ) {
		printf("NULL\n");
		return;
	}
	printf("L ");
	inOrder(root->left);
	printf("%c ", root->a.alpha );
	printf("R ");
	inOrder(root->right);
}
int main(void)
{
	char *SRC = "source.txt";
	char *DEST = "destination.txt";
	char *DECOM = "decompressed.txt";
	
	// Compression
	char buffer[BUFLEN]; memset(buffer, '\0', BUFLEN);
	int fTb[CODE] = { 0 }, slen;
	slen = readFile(SRC, buffer, BUFLEN); // printf("Str Len : %d\n", slen);
	getFrequency(fTb, buffer, slen);
	Queue *queue = buildPriorityQueue(fTb); // puts("QQ\n"); printQueue(queue);
	writeHeader(DEST, queue, slen);
	Node *root = buildHuffmanTree(queue); // puts("put"); inOrder(root);
	encoding(DEST, buffer, slen, root);
	
	// Decompression
	char buff[BINLEN]; memset(buff, '\0', BINLEN);
	char decompressed[BUFLEN]; memset(decompressed, '\0', BUFLEN);
	int clen = 0, blen = 0, dlen, seq;
	Queue *cqueue = (Queue*)malloc(sizeof(Queue));
	cqueue->size = 0; cqueue->top = NULL; cqueue->tail = NULL;
	blen = readHeader(DEST, cqueue, &clen, buff); // printf("Bit Len : %d\n", blen); puts("QQ\n"); printQueue(cqueue);
	// for ( seq  = 0; seq < blen; seq++ ) printf("%c", buff[seq]);
	Node *base = buildHuffmanTree(cqueue); // puts("put"); inOrder(base);
	dlen = decoding(buff, blen, clen, base, decompressed); // printf("\nData Len : %d\n", dlen);
	// for ( seq = 0; seq < dlen; seq++ ) printf("%c", decompressed[seq]);
	writeFile(DECOM, decompressed, dlen);
	return 0;
}

void getFrequency(int fTable[CODE], char *doc, int len)
{
	int seq;
	for ( seq = 0; seq < len; seq++ ) fTable[(int)doc[seq]] += 1;
}

Queue *buildPriorityQueue(int fTable[CODE])
{
	Queue *queue = (Queue*)malloc(sizeof(Queue));
	if ( queue != NULL )
	{
		queue->size = 0; queue->top = NULL; queue->tail = NULL;
		int seq;
		for ( seq = 0; seq < CODE; seq++ )
		{
			if ( fTable[seq] > 0 )
				orderedInsert(queue, createNode((char)seq, fTable[seq], 1));
		}
	}
	return queue;
}

Node *buildHuffmanTree(Queue *queue)
{
	while ( queue->size > 1 )
	{
		Node *n1 = dequeue(queue);
		Node *n2 = dequeue(queue);
		Node *parent = createNode(0, n1->a.freq + n2->a.freq, 0);
		parent->left = n1; parent->right = n2;
		orderedInsert(queue, parent); 
	}
	return dequeue(queue);
}

int mapper(Node *root, char target, char buffer[9], int index)
{
	if ( root == NULL ) return 0;
	if ( root->a.alpha == target ) return index;
	buffer[index] = '0';
	int blen = mapper(root->left, target, buffer, index+1);
	if ( !blen )
	{
		buffer[index] = '1';
		blen = mapper(root->right, target, buffer, index+1);
	}
	return blen;
}

void encoding(char *path, char *doc, int slen, Node *root )
{
	char byte = 0, buffer[9] = { 9 };
	int seq, blen, i, bi; seq = i = bi = 0;
	const char zero = 00000000; const char one = 00000001;
	for ( seq = 0; seq < slen; seq++ )
	{
		memset( buffer, '\0', 9 );
		blen = mapper(root, doc[seq], buffer, 0);
		for ( bi = 0; bi < blen; bi++ )
		{
			if ( buffer[bi] == '1' ){
				byte |= one; // printf("1");
			}
			else{
				byte |= zero; // printf("0");
			}
			i++;
			if ( i == 8 )
			{
				writeByte(path, byte);
				i = 0; byte &= zero;
			}
			else byte <<= 1;
		}
	}
	int diff = 8 - i;
	byte <<= diff;
	writeByte(path, byte); // puts("");
}

// EOP
int decoding(char data[BINLEN], int blen, int clen, Node *root, char dest[BUFLEN])
{
	int seq, i = 0;
	Node *prev = NULL, *current = root;
	for ( seq = 0; seq < blen; seq++ )
	{
		prev = current;
		if ( data[seq] == '1' )
			current = current->right;
		else current = current->left;
		if ( current == NULL )
		{
			seq--;
			dest[i] = prev->a.alpha;// printf("%c", dest[i]);
			i++;
			current = root;
			if ( i == clen ) break;
		}
	}
	return i; // slen
}
