// structure definition
typedef struct NODE {
	char id;
	double value;
	struct NODE *prev, *next;
}Node;

typedef struct STACK {
	int count;
	Node *sp, *ep;
}Stack;

Stack *createStack()
{
	Stack *s = (Stack*)malloc(sizeof(Stack));
	if ( s != NULL )
	{
		s->count = 0;
		s->ep = NULL;
		s->sp = NULL;
	}
	return s;
}
Node *createNode( char id )
{
	Node *n = (Node*)malloc(sizeof(Node));
	if ( n != NULL )
	{
		n->id = id;
		//printf("id : %c\n", n->id);
		n->next = NULL;
		n->prev = NULL;
	}
	return n;
}
void push(Stack *stack, Node *node)
{
	if ( stack->count == 0 )
		stack->sp = node;
	else
	{
		stack->ep->next = node;
		node->prev = stack->ep;
	}
	stack->ep = node;
	stack->count += 1;
	//printList(stack);
	//printf("%c %d\n", node->id, stack->count);
	//getchar();
}
Node *pop(Stack *stack)
{
	//puts("pop");
	Node *n = stack->ep;
	if ( stack->count == 1 )
	{
		stack->sp = NULL;
		stack->ep = NULL;
	}
	else
	{
		stack->ep = stack->ep->prev;
		stack->ep->next = NULL;
	}
	stack->count -= 1;
	//printf("return %c\n", n->id);
	return n;
}
void setValue(Stack *stack, Node *node, double value)
{
	node->value = value;
	push(stack, node);
}
double getValue(Stack *stack)
{
	Node *n = pop(stack);
	double v = n->value;
	free(n);
	return v;	
}
char peek(Stack *stack)
{
	return stack->ep->id;	
}
int backTracking(Stack *primary, Stack *secondary, char key, int mode)
{
	while ( primary->count > 0 )
	{
		if ( peek(primary) != key )
		{
			Node *cur = pop(primary);
			//printf("%c >> ", cur->id );
			push(secondary, cur);
		}
		else
		{
			if ( mode )	{
				//puts("inside");
				pop(primary);
				getchar();
			}
			return 1;
		}
	}
	return 0;	
}
Stack *destroyStack(Stack *stack)
{
	Node *c = stack->sp;
	while ( stack->count > 0 )
	{
		free(pop(stack));
	}
}
void printList(Stack *stack)
{
	Node *c = stack->sp;
	while ( c != NULL )
	{
		printf("%c ", c->id);
		c = c->next;
	} puts(">> || EOE");
}

void initialize(Stack *stack)
{
	stack->count = 0;
	stack->ep = NULL;
	stack->sp = NULL;
}
