#include <stdio.h>
#include <stdlib.h>
#include "RF.h"
#include "STACK.h"
#include "PARSER.h"
#define BUFLEN 256

char getOpposite(char key)
{
	switch(key)
	{
		case ')':
			return '(';
		case '(':
			return ')';
		case '}':
			return '{';
		case '{':
			return '}';
		case ']':
			return '[';
		case '[':
			return ']';
		default:
			return 'X';
	}
	return 0;
}
int hashing(char key)
{
	return key % 26;
}
int parenthesis(char buffer[], int size, Stack *working)
{
	int i = -1, outcome;
	char cur, prev = 0, open = INT_MAX, closing = 0;
	Stack /*working, */op;
	//initialize(&working); 
	initialize(&op);
	if ( buffer[0] == 42 || buffer[0] == 47 ) return 5; // operator at front
	while ( ++i < size )
	{
		cur = buffer[i];
		printf("\ncurrent : %c\n", cur);
		Node *node = createNode(cur);
		if ( cur >= 65 && cur <= 90 )
		{
			if ( i < size-1 && (buffer[i+1] == '[' || buffer[i+1] == '{' || buffer[i+1] == '(' ) ) return 99;
			if ( i < size-1 && (buffer[i+1] >= 65 && buffer[i+1] <= 90) ) return 88;
			else push(working, node);
		}	
		else
		{
			if ( op.count > 0 ) prev = peek(&op);
			else prev = 0;
			if ( cur == '(' || cur == '{' || cur == '[' )
			{
				if ( i < size-1 && ( buffer[i+1] == 42 || buffer[i+1] == 47 ) ) return 66;
				open = cur;
				//printf("LEFT : %c\n", cur);
				push(&op, node);
				continue;
			}
			if ( (prev == '(' && (prev < 42 || prev > 47) ) && (cur != getOpposite(prev)) && ( cur != ')' && cur != '}' && cur != ']' ) )
			{
				//puts("right here");
				//printf("%c %c %c\n", open, prev, cur);
				push(&op, node); // checked
				continue;
			}
			else if ( i < size-1 && cur != '}' && cur != ']' && cur != ')' )
			{
				//puts("here\n");
				if ( buffer[i+1] >= 42 && buffer[i+1] <= 47 ) return 77;
				if ( buffer[i+1] == ')' || buffer[i+1] == '}' || buffer[i+1] == ']' ) return 55;
			}
			if ( cur == 43 || cur == 45 )
			{
				if ( i == size-1 && buffer[i] >= 42 && buffer[i] <= 47 ) return 77;
				if ( prev == '(' || prev == '{' || prev == '[' || op.count == 0 )
					push(&op, node); 
				else if ( prev == 43 || prev == 45 )
				{
					//printf("11 %c %c", cur, prev);
					outcome = backTracking(&op, working, open, 0);
					push(&op, node); 
				}
				else if ( prev == 42 || prev == 47 )
				{
					//printf("12 %c %c", cur, prev);
					
					if ( op.count != 0 )
					{
						Node *p = pop(&op);
						//printf("13 %c\n", p->id);
						push(working, p);
					}
					//outcome = backTracking(&op, working, open, 0);	
					push(&op, node);
				}
			}
			else if ( cur == 42 || cur == 47 )
			{
				//puts("div/mul");
				if ( i == size-1 && buffer[i] >= 42 && buffer[i] <= 47 ) return 77;
				if ( prev == 43 || prev == 45 )
				{
					//printf("21 %c %c", cur, prev);
					push(&op, node);	
				}
				else
				{
					printf("22 %c %c", cur, prev);
					if ( op.count != 0 && ( prev != '(' && prev != '{' && prev != '[' ) )
					{
						Node *p = pop(&op);
						//printf("23 %c\n", p->id);
						push(working, p);
					}
					push(&op, node);
				}
			}
			else // right bracket
			{
				//printf("RIGTH %c", cur );
				if ( i < size-1 && ( buffer[i+1] >= 65 && buffer[i+1] <= 90 )) return 99;
				closing = getOpposite(cur);
				//printf("closing : %c\n", closing);
				outcome = backTracking(&op, working, closing, 1);
				if ( outcome == 0 )
				{
					puts("\nfailed");
					open = 0;
					return 8;
				}
			}
		}
	}
	printf("Open : %c\n", open);
	int flag = 0;
	while ( op.count != 0 )
	{
		Node *n = pop(&op);
		//printf("cur : %c\n", n->id);
		if ( n->id == open )
		{
			puts("kill");
			return 7;	
		} 
		push(working, n);
	}
	//printf("working size : %d\n", working->count);
	return 1;
}
double compute(Stack *stack, int algebra[26])
{
	double A, B;
	Stack working;
	initialize(&working);
	Node *current = stack->sp;
	
	while ( current != NULL )
	{
		if ( current->id >= 65 && current->id <= 90 )
		{
			setValue(&working,current,algebra[hashing(current->id)]);
		}
		else
		{
			if ( working.count >= 2 )
			{
				B = getValue(&working);
				A = getValue(&working);
				printf("%lf %lf\n", A, B);
				if ( current->id == 43 ) A += B;
				if ( current->id == 45 ) A -= B;
				if ( current->id == 42 ) A *= B;
				if ( current->id == 47 ) 
				{
					if ( B == 0 ) 
					{
						printf("Exception : Devide by zero\n");
						return INT_MAX;
					}
					A /= B;
				}	
			}
			else
			{
				A = getValue(&working);
				if ( current->id == 45 ) A *= -1;
				if ( current->id == 43 ) A = A;
			}
			setValue(&working, current, A);
		}
		current = current->next;	
	}
	A = getValue(&working);
	destroyStack(&working);
	return A;
}
int checker(char buffer[BUFLEN], int size)
{
	int i;
	int br[3][2] = { 0 };
	for ( i = 0; i < size; i++ )
	{
		if ( buffer[i] == '(' )
		{
			if ( i < size-1 && buffer[i+1] == ')' ) return 11;
			//if ( br[0][0] >= br[0][1] )
				br[0][0] += 1;
		}
		else if ( buffer[i] == ')' )
		{
			if ( br[0][0] >= br[0][1]+1 )
				br[0][1] += 1;
			else return 22;
		}
		else if ( buffer[i] == '{' )
		{
			if ( i < size-1 && buffer[i+1] == '}' ) return 11;
			if ( br[1][0] == br[1][1] && br[0][0] == br[0][1] )
			{
				//if ( br[2][0] >= br[2][1] )
					br[2][0] += 1;
			}
			else return 33;
		}
		else if ( buffer[i] == '}' )
		{
			if ( br[1][0] == br[1][1] && br[0][0] == br[0][1] )
			{
				if ( br[2][0] >= br[2][1]+1 )
					br[2][1] += 1;
				else return 22; 
			}
			else return 33;
		}
		else if ( buffer[i] == '[' )
		{
			if ( i < size-1 && buffer[i+1] == ']' ) return 11;
			if ( br[0][0] == br[0][1] )
			{
				//if ( br[1][0] >= br[1][1] )
					br[1][0] += 1;
			}
			else return 33;
		}
		else if ( buffer[i] == ']' )
		{
			if ( br[0][0] == br[0][1] )
			{
				if ( br[1][0] >= br[1][1]+1 )
					br[1][1] += 1;
				else return 22; 
			}
			else return 33;
		}
	}
	return ((br[0][0] == br[0][1])&&(br[1][0] == br[1][1])&&(br[2][0] == br[2][1]))?1:44;
}
int main(void)
{
	char *path = "expression.txt";
	char buffer[BUFLEN] = {0};
	int algebra[26] = { 0 };
	Stack postfix, prefix;
	initialize(&postfix); initialize(&prefix);
	char key;
	int len, outcome, value;
	double V;
	readData(buffer, path, BUFLEN);
	len = strlen(buffer);
	outcome = checker(buffer, len);
	if ( outcome != 1 )
	{
		printf("\nException : %d\n", checker(buffer, len));
		if ( outcome == 11 ) puts("\nException : Stacked paranthesis\n");
		if ( outcome == 33 ) puts("\nException : Parenthesis priority error\n");
		if ( outcome == 22 ) puts("\nException : Inproperly closed parenthesis\n");
		if ( outcome == 44 ) puts("\nException : Unclosed parenthesis\n");
	}
	else
	{
		outcome = parenthesis(buffer, len, &postfix);
		if ( outcome == 1 )
		{
			printList(&postfix);
			while ( scanf(" %c", &key) != EOF &&  scanf("%d", &value) != EOF )
				algebra[hashing(key)] = value;
			V = compute(&postfix, algebra);
			if ( V != INT_MAX )
				printf("V : %lf\n", V);
		}
		else
		{
			switch(outcome)
			{
				case 5:  
					puts("\nException : Operator at the beginning\n");
					break;
				case 55:
					puts("\nException : Operator right before Right parethesis\n");
					break;
				case 66:
					puts("\nException : Operator after Left parenthesis\n");
					break;
				case 7:
					puts("\nException : Unclosed parenthesis - Left\n");
					break;
				case 77:
					puts("\nException : Stacked operators\n");
					break;
				case 8:
					puts("\nException : Unclosed parethensis - Right\n");
					break;
				case 88:
					puts("\nException : Stacked variables\n");
					break;
				case 99:
					puts("\nException : Algebra around parethesis\n");
					break;
			}
		}		
	} 
	
	getchar();
	return 0;
}
