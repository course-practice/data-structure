#include <stdio.h>
#include <stdlib.h>
#define SIZE 140
typedef struct link{
	int index;
	struct link *next;
} Link;

typedef struct station{
	char color[2];
	int length;
	int cost;
	struct link *start;
} Station;

typedef struct path {
	int length;
	Link *start;
	Link *end;
} Path;

// function prototype
void readFile(char *path, Station taipei[], int *lineStart, int *lastStation );
int updateLink(Station *s, int original, int update);
int addLink(Station *s, int d);
int BFS(Station taipei[], int departure, int destination);
void getColor(Station taipei[]);
void updateStation(Station *s, char c, int d);
void addNewStation( int colorArray[], int size, Station taipei[], int *lastStation );
int getByIndex(Station s, int index);
// utility
void initializeStation(Station *s);
void initializer(Station taipei[]);
void printMap(Station taipei[], int last);
void deleteMap(Station taipei[], int last);
// queue operation
Path* createQueue();
void releaseQueue(Path *p);
int contains( Path *p, int tag );
int dequeue(Path *p);
void enqueue(Path *p, int s);

int main(void)
{
	// section 1
	char *red = "red.txt";
	char *blue = "blue.txt";
	char *green = "green.txt";
	char *brown = "brown.txt";
	char *orange = "orange.txt";
	char *orange2 = "orange2.txt";
	int colorArray[5] = {0}; // start index of each line color
	int lastStation = 0; // index of last station
	Station taipei[SIZE]; // map array
	puts("initializing map...\n"); 
	initializer(taipei);
	// section 2
	puts("Reading map from file...\n");
	colorArray[0] = lastStation+1;
	readFile(red, taipei, &colorArray[0], &lastStation);
	colorArray[1] = lastStation+1;
	readFile(blue, taipei, &colorArray[1], &lastStation);
	colorArray[2] = lastStation+1; 
	readFile(green, taipei, &colorArray[2], &lastStation); 
	colorArray[3] = lastStation+1;
	readFile(brown, taipei, &colorArray[3], &lastStation); 
	colorArray[4] = 86;
	readFile(orange, taipei, &colorArray[4], &lastStation);
	readFile(orange2, taipei, &colorArray[4], &lastStation);  
	printMap(taipei, lastStation);
	// section 3
	getColor(taipei); // get color by station index
	// section 4
	//addNewStation(colorArray, 5, taipei, &lastStation); // add new station to the 5th station
	addAtHead(colorArray, 5, taipei, &lastStation);
	printMap(taipei, lastStation);
	// section 5
	int departure, destination;
	printf("Please select a departure station\n");
	scanf("%d", &departure);
	printf("Please select a destination station\n");
	scanf("%d", &destination);
	int pathCost = BFS(taipei, departure, destination);
	if ( pathCost > 0 )
		printf( "Path cost from %d station to %d station is %d\n", departure, destination, pathCost );
	getchar();
	puts("Releasing map...");
	//deleteMap(taipei, lastStation);
	getchar();
	return 0;
}
void initializeStation(Station *s)
{
	(*s).color[0] = 0;
	(*s).color[1] = 0;
	(*s).length = 0;
	(*s).cost = 0;
	(*s).start = NULL;
}
void initializer(Station taipei[])
{
	int i;
	for ( i = 0; i < SIZE; i++ )
		initializeStation(&taipei[i]);
}
int addLink(Station *s, int d)
{
	Link *newLink = (Link*)malloc(sizeof(Link));
	if ( newLink != NULL )
	{
		newLink->index = d;
		newLink->next = s->start;
		s->start = newLink;
		printf("New Link Created %d\n", d);
		return 1;
	}
	puts("Memory not available");
	return 0;
}
void updateStation(Station *s, char c, int d)
{
	printf("UPDATE STATION %c\n", c);
	if ( s->color[0] == 0 ) s->color[0] = c;
	else if ( s->color[0] != c ) s->color[1] = c;
	addLink(s, d);
	(*s).length++; 
}
int getByIndex(Station s, int index)
{
	Link *pPre = s.start;
	int i = 0;
	while ( pPre != NULL && i != index )
	{
		pPre = pPre->next; i++;
	}
	if ( i == index )
	{
		puts("FOUND");
		return pPre->index;
	}
	return 999;
}
void readFile(char *path, Station taipei[], int *lineStart, int *lastStation )
{
	FILE *fp;
	char color;
	int prev, current;
	
	if ( (fp = fopen(path, "r") ) != NULL )
	{
		printf("OPEN FILE : %s\n", path);
		fscanf(fp, " %c", &color);
		printf("READING color :%c\n", color);
		fscanf(fp, "%d", &prev);
		while ( fscanf(fp, "%d", &current ) != EOF )
		{
			printf("%d -> %d\n", current, prev );
			if ( taipei[current].color[0] == 0 || 1 == 1 ) // all condition
			{
				updateStation(&taipei[current], color, prev); 
				updateStation(&taipei[prev], color, current);
			}
			prev = current;
		}
		*lastStation = prev; // track index of last station
		fclose(fp);
	}
	else
	{
		puts("READ FILE FAILED");
	}
}
int DFS(Station taipei[], int departure, int destination, Path *p, Path *v)
{
	int i, sub;
	printf("enter dfs %d\n", departure);/*
	if ( contains(v, departure) == 1 )
	{
		return 0;
	}*/
	enqueue(p,departure);
	if ( departure == destination ) return 1;
	//enqueue(v, departure);
	for ( i = 0; i < taipei[departure].length; i++ )
	{
		sub = getByIndex(taipei[departure], i);
		printf("exploring %d\n", sub);
		if ( taipei[sub].cost+1 == taipei[departure].cost )
		{
			if ( DFS(taipei, sub, destination, p, v) == 1 ) return 1;
		}
	}
	printf("\ndequeue p: %d\n", dequeue(p));
	return 0;
}
void trackPath(Station taipei[], int departure, int destination)
{
	//Link *current = taipei[destination].start;
	Path p; p.end = NULL; p.start = NULL; p.length = 0;
	Path v; v.end = NULL; v.start = NULL; v.length = 0;
	int outcome = 0, i, j = 0, sub, flag = 0;
	enqueue(&p, departure);
	for ( i = 0; i < taipei[departure].length; i++ )
	{
		sub = getByIndex(taipei[departure], i);
		if ( DFS( taipei, sub, destination, &p, &v) == 1 )
		{
			outcome = 1;
			puts("found");
			break;
		}
		printf("\nqueue.size  %d\n", p.length);
		while ( p.length != 0 )
		{
			printf("dequeue : %d\n", dequeue(&p) );
		}
		enqueue(&p, departure);
	}
	if ( outcome == 1 && p.length > 0 )
	{
		Link *printStation = p.start;
		while ( printStation != NULL )
		{
			printf("%d >> ", printStation->index );
			printStation = printStation->next;
		}
	}
	else
		puts("aiya");	
} 

int BFS(Station taipei[], int departure, int destination)
{
	if ( taipei[departure].length == 0 || taipei[destination].length == 0 || departure < 1 || departure >= SIZE || destination < 1 || destination >= SIZE)
	{
		puts("invalid input");
		return -1;
	}
	Link *current = taipei[departure].start;
	Path *visited = createQueue(); // inactive list
	Path *queue = createQueue(); // active list
	int cost = 0, i = 0, key, sub;
	if ( visited != NULL && queue != NULL ) // safety check
	{
		enqueue(queue, departure); // add departure station to active queue
		while ( queue->length != 0 ) // loop through all possible action
		{
			key = dequeue(queue); // explored frontier
			if ( key == destination ) // found
			{
				puts("track path...");
				trackPath(taipei, destination, departure);
				return taipei[key].cost; // return path-cost
			}
			enqueue(visited, key);
			while ( i < taipei[key].length ) // loop through all possible action
			{
				sub = getByIndex(taipei[key], i); // sub is index of next possible action
				if ( !contains(visited, sub) )
				{
					if ( taipei[sub].cost == 0 ) 
						taipei[sub].cost = taipei[key].cost + 1; // update cost
					enqueue(queue, sub); // add all possible actions to active queue
				}
				i++; // go to next index
			}
			i = 0; // reset i index for the coming loop
		}
		releaseQueue(visited);
		releaseQueue(queue);
		return -1;	// not found
	}
	else
		puts("Memory not available");
	return -2; // 
}
void getColor(Station taipei[])
{
	int StationX;
	printf("Enter Station X\n");
	scanf( "%d", &StationX );
	Station X = taipei[StationX];
	if ( X.length > 0 )
	{
		int i = 0;
		printf("Station %d is on ", StationX );
		while ( 1 )
		{
			switch(X.color[i])
			{
				case 'r':
					printf("RED ");
					break;
				case 'p':
					printf("BROWN ");
					break;
				case 'g':
					printf("GREEN ");
					break;
				case 'b':
					printf("BLUE ");
					break;
				case 'o':
					printf("ORANGE ");
					break;
			}
			if ( X.color[1] != 0 && i == 0 )
			{
				printf(", ");
				i = 1;
			}
			else if ( i == 1 )
				break;
			else
				break;	
		}
		printf("line\n");	
	}
	else
		puts("Empty Station");
}

void addAtHead(int colorArray[], int size, Station taipei[], int *lastStation )
{
	char lineColor;
	int newTag, key, depth = 0;
	printf("please enter tag of new station follow by lineColor\n");
	scanf( "%d", &newTag );
	scanf( " %c", &lineColor );
	switch(lineColor)
	{
		case 'r':
			key = 0;
			printf("RED LINE");
			break;
		case 'p':
			key = 3;
			printf("BROWN LINE");
			break;
		case 'g':
			key = 2;
			printf("GREEN LINE");
			break;
		case 'b':
			key = 1;
			printf("BLUE LINE");
			break;
		case 'o':
			key = 4;
			printf("ORANGE LINE");
			break;
	}
	puts("");
	int prev, flag;
	*lastStation += 1;
	key = colorArray[key]; // get start index of desired line color
	printf("index : %d\n", key); // debugger
	Link *current = taipei[key].start; // get first possible station of this line
	printf("Head of this line %d : %d\n", key, current->index); // debugger
	Path *visited = createQueue(); // inactive list, just to ensure no event of revisit
	colorArray[key] = *lastStation;
	if ( taipei[key].length == 1 )
	{
		updateStation(&taipei[*lastStation], lineColor, key);
		//updateStation(&taipei[*lastStation], lineColor, taipei[key].start->index);		
		if ( updateLink(&taipei[key], taipei[key].start->index, *lastStation) == -1 )
		{
			puts("UPDATELINK Failed");
			return;
		}
	}
	else if ( taipei[key].length > 1 )
	{
		updateStation(&taipei[*lastStation], lineColor, key);
		updateStation(&taipei[*lastStation], lineColor, taipei[key].start->index);
		if ( updateLink(&taipei[key], taipei[key].start->index, *lastStation) == -1 )
		{
			puts("UPDATELINK Failed");
			return;
		}
		if ( updateLink(&taipei[taipei[key].start->index], key, *lastStation) == -1 )
		{
			puts("UPDATELINK Failed");
			return;
		}	
	}
}
void addNewStation( int colorArray[], int size, Station taipei[], int *lastStation )
{
	char lineColor;
	int newTag, key, depth = 0;
	printf("please enter tag of new station follow by lineColor\n");
	scanf( "%d", &newTag );
	scanf( " %c", &lineColor );
	switch(lineColor)
	{
		case 'r':
			key = 0;
			printf("RED LINE");
			break;
		case 'p':
			key = 3;
			printf("BROWN LINE");
			break;
		case 'g':
			key = 2;
			printf("GREEN LINE");
			break;
		case 'b':
			key = 1;
			printf("BLUE LINE");
			break;
		case 'o':
			key = 4;
			printf("ORANGE LINE");
			break;
	}
	puts("");
	int prev, flag;
	key = colorArray[key]; // get start index of desired line color
	printf("index : %d\n", key); // debugger
	Link *current = taipei[key].start; // get first possible station of this line
	printf("Head of this line %d : %d\n", key, current->index); // debugger
	Path *visited = createQueue(); // inactive list, just to ensure no event of revisit
	if ( visited != NULL ) // safety check
	{
		while ( depth < 5 && current != NULL ) // ensure a depth of 5 
		{ // current == NULL is typically not possible if only same line is expored
			key = current->index; // get new station
			if ( !contains(visited, key) && ( taipei[key].color[0] == lineColor || taipei[key].color[1] == lineColor ) )
			{ // no revisit and only station of same color is explored
				depth++;
				if ( depth == 4 )
				{
					flag = 1;
					break;
				}
				prev = key; // keep track the prev index
				current = taipei[key].start; // possible action of the new station
				enqueue(visited, key); // add station to visited list
			}
			else
				current = current->next; // not the same color
			puts("help me");
		}
		if ( flag == 1 )
		{
			*lastStation += 1;
			printf("new index : %d\n", *lastStation);
			if ( updateLink(&taipei[prev], key, *lastStation) == -1 )
			{
				puts("UPDATELINK Failed");
				return;
			}
			if ( updateLink(&taipei[key], prev, *lastStation) == -1 )
			{
				puts("UPDATELINK Failed");
				return;
			}
			updateStation(&taipei[*lastStation], lineColor, key);
			updateStation(&taipei[*lastStation], lineColor, prev);
			printf("inserted between %d : %d\n", prev, key);
			releaseQueue(visited);
			return;
		}
		puts("Unable to locate station");
		return;
	}
	printf("Memory not available\n");
}
int updateLink(Station *s, int original, int update)
{
	Link *current = s->start;
	while ( current != NULL )
	{
		if ( current->index == original )
		{
			current->index = update; // found and will update
			return 1; // done
		}
		current = current->next; // go to next station
	}
	return 0; // not done
}
void printMap(Station taipei[], int last)
{
	int i, j;
	for ( i = 0; i <= last; i++ )
	{
		Station s = taipei[i];
		Link *current = s.start;
		printf("STATION %d: %c\n", i, s.color[0] );
		for ( j = 0; j < s.length; j++ )
		{
			printf("%d -> ", current->index );
			current = current->next;
			if ( current == NULL ) break;
		}
		puts(" END OF LINE");
	}
}
Path* createQueue()
{
	Path *p = (Path*)malloc(sizeof(Path));
	if ( p != NULL )
	{
		p->end = NULL;
		p->start = NULL;
		p->length = 0;
	}
	return p;
}
int contains( Path *p, int tag )
{
	Link *current = (*p).start;
	while ( current != NULL )
	{
		if ( current->index == tag )
		{
			puts("TRUE");
			return 1;
		}
		current = current->next;
	}
	return 0;
}
int dequeue(Path *p)
{
	int index;
	if (p->length > 0 )
	{
		Link *dlt = p->start;
		p->start = p->start->next;
		if ( p->length == 2 )
		{
			p->end = p->start;
		}
		else if ( p->length == 1 )
		{
			p->start = NULL;
			p->end = NULL;
		}
		index = dlt->index;
		free(dlt);
		p->length -= 1;
		puts("dequeue");
		return index;
	}
	return -1;
}
void enqueue(Path *p, int s)
{
	Link *newLink = (Link*)malloc(sizeof(Link));
	if ( newLink != NULL )
	{
		newLink->index = s;
		newLink->next = NULL;
		if ( p->length == 0 )
		{
			p->start = newLink;
			p->end = newLink;
		}
		else if ( p->length >= 1 )
		{
			p->end->next = newLink;
			p->end = newLink;
		}
		p->length += 1;
		return;
	}
	puts("Memory not available during enqueue");
}
void releaseQueue(Path *p)
{
	Link *l = p->start;
	Link *dltLink;
	while ( l != NULL )
	{
		dltLink = l;
		l = l->next;
		free(dltLink);			
	} 
}
void deleteMap(Station taipei[], int last)
{
	int i, j;
	Link *dltLink;
	for ( i = 0; i <= last; i++ )
	{
		Station s = taipei[i];
		Link *current = s.start;
		printf("STATION %d: %c\n", i, s.color[0] );
		printf("deleting ...");
		for ( j = 0; j < s.length; j++ )
		{
			printf("%d -> ", current->index );
			dltLink = current;
			current = current->next;
			free(dltLink);
			if ( current == NULL ) break;
		}
		puts(" END OF LINE");
	}
}
