#include <stdio.h>
#include <stdlib.h>

// program definition
/*
	input : 
		- a string of mathematic expression(infix) with lowercase alphabets as algebra
		- integer value for each algebra ( pair-wise and terminate with alphabet : '0' )
	functions : 
		- translate input expression to postfix
		- translate input expression to prefix
		- map the input value to the created linked-list
		- compute the sum with postfix stack
	
	output : 
		- typed double value
*/

// structure definition
struct stackNode {
	int id;
	double value;
	struct stackNode *next;
	struct stackNode *prev;
};
typedef struct stackNode StackNode;
typedef StackNode *Node;

struct stackTag{
	int count;
	Node head;
	Node tail;
};
typedef struct stackTag StackTag;
typedef StackTag *Stack;

// function prototype
Stack createStack();
int push(Stack *stack, int id);
int pop(Stack *stack);
int peek(Stack *stack);
void print(Stack *stack);
int parser(Stack *stack, char expression[], int size);
void release(Stack *stack);
int hashing(int algebra);
void push_value(Stack *stack, int id, double value);
double pop_value(Stack *stack);
double compute(Stack *stack, int table[] );
int infix2prefix(Stack *stack, char expression[], int size);
int main(){
	Stack stack = createStack();
	char expression[256] = { 0 }; // for flexible length, can consider vector 
	int hashArray[26] = { 0 }; // size of 26 is good enough since there are only 26 alphabets
	
	int output = 0;
	int size = sizeof( expression ) / sizeof( char );
	if ( stack == NULL )
	{
		puts("memory not available");
	}
	else
	{
		puts("part 1 : read string expression");
		scanf("%256[^\n]s",expression);
		// printf("%s\n", expression);
		puts("part 2 : infix to postfix");
		output = parser(&stack,expression, size);
		if ( stack != NULL )
			print(&stack);
		else
			puts("nothing to print");
		
		puts("part 3 : enter algebra and value");
		char algebra = 0;
		int value;
		scanf(" %c", &algebra);
		
		do {
			scanf("%d", &value);
			// printf("%c, %d\n", algebra, value);
			hashArray[hashing(algebra)] = value;
			scanf(" %c", &algebra);
		} while ( algebra != '0');
		
		printf("SUM : %lf\n", compute(&stack, hashArray));
		
		puts("\n part 4 : infix to prefix");
		Stack infix = createStack();
		output = infix2prefix(&infix, expression, size);
		print(&infix);
		puts("==========");
		release(&stack);
		release(&infix);
		// printf("%s\n", expression);
	}
	
	
	getchar();
	return 0;
} 


Stack createStack(){
	Stack stack = (Stack)malloc(sizeof(StackTag));
	if ( stack != NULL ){
		stack->count = 0;
		stack->head = NULL;
		stack->tail = NULL;
	}
	return stack;
}

void release(Stack *stack){
	puts("releasing memory");
	Node node = NULL;
	Node temp = NULL;
	node = (*stack)->head;
	while ( node != NULL ){
		temp = node;
		node = node->next;
		free(temp->next);
		free(temp->prev);
		free(temp);
	}
	free((*stack)->head);
	free((*stack)->tail);
	free(stack);
}

int parser(Stack *stack, char expression[], int size){
	Stack OpStack = createStack();
	/*
	int size = sizeof( expression ) / sizeof( char ); // ??? size = 8 while the actual length of expression[] is 256!
	printf("size %d",size);
	*/
	int i;
	char id = 40, prevId;
	// prepare the OpStack with a left bracket
	push(&OpStack, id);
	for( i = 0; i < size && expression[i] != 0; i++ ){
		// printf("\nround : %d, current char : %c\n", i, expression[i]);
		id = expression[i];
		if ( expression[i] >= 97 && expression[i] <= 122 ) // a to z
		{
			push(stack, id);
			// printf("\npush : %d\n", push(stack, id));
		}
		else{
			prevId = peek(&OpStack);
			// printf("\n==prevId %d==\n", prevId);
			if ( prevId != 0 )
			{
				if ( expression[i] == 41 ) // right bracket
				{
					while ( OpStack->count > 1 ) // loop until the left bracket
					{
						// printf("inserting %c\n", peek(&OpStack));
						if ( peek(&OpStack) != 40 )
							push(stack, pop(&OpStack)); // clear the content within bracket
						else
						{
							pop(&OpStack); // remove the left bracket from list
							break;
						}
					}
					continue;
				}
				else if ( expression[i] == 40 ) // left bracket
				{
					push(&OpStack, id);
				}
				else if ( expression[i] == 43 || expression[i] == 45 )
				{
					if ( prevId == 40 )
					{
						push(&OpStack, id);
					}
					else if ( prevId == 43 || prevId == 45 )
					{
						while ( OpStack->count > 1 )
						{
							// printf("inserting %c\n", peek(&OpStack));
							if ( peek(&OpStack) != 40 )
								push(stack, pop(&OpStack));
							else
							{
								// pop(&OpStack);
								break;
							}
						}
						push(&OpStack, id);
					}
					else if ( prevId == 42 || prevId == 47 )
					{
						/*
						while ( OpStack->count > 1 )
						{
							if ( *(char*)peek(&OpStack) != 40 )
								push(stack, pop(&OpStack));
							else
								break;
						}
						*/
						push(stack, pop(&OpStack));
						push(&OpStack, id);
					}
				}
				else if ( expression[i] == 42 || expression[i] == 47 )
				{
					if ( prevId == 40 )
					{
						push(&OpStack, id);
					}
					else if ( prevId == 43 || prevId == 45 )
					{
						push(&OpStack, id);
					}
					else /*if ( prevId == 42 || prevId == 47 )*/
					{
						push(stack, pop(&OpStack));
						push(&OpStack, id);
					}
				}
			}	
		}
		// puts("######");
	}
	print(&OpStack);
	
	while ( OpStack->count > 1 )
	{
		if ( peek(&OpStack) != 40 ){
			push(stack, pop(&OpStack));
			// printf("OpStack count %d\n", OpStack->count);
		}
		else
			break;
	}
	puts("releasing memory");
	release(&OpStack);
	puts("escape");
	return 1;

} 

int push(Stack *stack, int id){
	// puts("push()");
	Node newNode = (Node)malloc(sizeof(StackNode));
	if ( newNode != NULL )
	{
		newNode->id = id;
		newNode->value = 0;
		newNode->next = NULL;
		newNode->prev = NULL;
		if ( (*stack)->count == 0 ){
			(*stack)->head = newNode;
			newNode->prev = NULL; 
			// puts("empty stack");
		}
		else{
			(*stack)->tail->next = newNode;
			newNode->prev = (*stack)->tail;
		}
		(*stack)->tail = newNode;
		
		((*stack)->count)++;
		// printf("qtt : %d\n", ((*stack)->count));
		return (*stack)->count;
	}
	// puts("push done");
	return -1;
}

int pop(Stack *stack)
{
	int id = 0;
	if ( (*stack)->count > 0 )
	{
		Node node = (*stack)->tail;
		if ( (*stack)->count > 1 )
			(*stack)->tail = node->prev;
		else
		{
			(*stack)->head = NULL;
			(*stack)->tail = NULL;
		}
		id = node->id;
		free(node);
		((*stack)->count)--;
	}
	return id;
}

int peek(Stack *stack){
	// printf("peek() ");
	int id = 0;
	if ( (*stack)->count > 0 ){
		id = (*stack)->tail->id;
		// printf("%c\n", id);
	}	
	return id;
}

void print(Stack *stack){
	// puts("print()");
	Node node = (*stack)->head;
	while( node != NULL ){
		printf("%c", node->id);
		node = node->next;
	}
} 

int hashing(int algebra)
{
	return algebra % 26;
}

double pop_value(Stack *stack){
	double value = 0;
	
	if ( (*stack)->count > 0 ){
		Node node = (*stack)->tail;
		(*stack)->tail = node->prev;
		value = node->value;
		((*stack)->count)--;
		free(node);
	}
	return value;
}
void push_value(Stack *stack, int id, double value){
	push(stack, id);
	(*stack)->tail->value = value;
}
double compute(Stack *stack, int table[] )
{
	Stack working = createStack();
	
	Node currentNode = NULL;
	double A, B;
	int id;
	// assign value to the list
	currentNode = (*stack)->head;
	while ( currentNode != NULL )
	{
		id = currentNode->id;
		if ( id >= 97 )
		{
			currentNode->value = table[hashing(id)];
			// printf("assignment : %lf\n", currentNode->value);
		}
		currentNode = currentNode->next;
	}
	
	currentNode = (*stack)->head;
	while ( currentNode != NULL )
	{
		if ( currentNode->id >= 97 )
			push_value(&working, currentNode->id, currentNode->value);
		else{
			// total = (int*)malloc(sizeof(int));
			B = pop_value(&working);
			A = pop_value(&working);
			// printf("%lf %lf\n", A, B);
			switch(currentNode->id){
				case 43:
					puts("add");
					A += B;
					break;
				case 45:
					puts("min");
					A -= B;
					break;
				case 42:
					puts("mul");
					A *= B;
					break;
				case 47:
					puts("div");
					if ( B == 0 ) 
					{
						printf("\n\nexception : divide by zero\n\n");
						return 0;	
					}
					A /= B;
					break;
			}
			push_value(&working, 123, A);
			// printf("latest updates %lf\n", A);
		}
		currentNode = currentNode->next;
	}
	A = pop_value(&working);
	release(&working);
	return A;
}
int push_head(Stack *stack, int id)
{
	Node node = (Node)malloc(sizeof(StackNode));
	if ( node != NULL )
	{
		node->id = id;
		node->value = 0;
		node->prev = NULL;
		if ( (*stack)->count != 0 )
		{
			node->next = (*stack)->head;
			// (*stack)->head->prev = node;
			(*stack)->head = node;
		}
		else
		{
			(*stack)->head = node;
			(*stack)->tail = node;
			node->next = NULL;
		}
		return ++((*stack)->count);
	}
	return 0;
}
int infix2prefix(Stack *stack, char expression[], int size)
{
	Stack OpStack = createStack();
	int i, index;
	char id = 41, prevId;
	push(&OpStack, id);
	for(i = 0; i < size;i++ )
	{
		if ( expression[i] == 0 )
		{
			index = i-1;
			break;
		}
	}
	// printf("\nlen  %d \n", index);
	for(i = index; i >= 0; i-- )
	{
		// printf("%c\n", expression[i]);
		if ( expression[i] >= 97 && expression[i] <= 122 )
		{
			push_head(stack, expression[i]);
			continue;
		}
		prevId = peek(&OpStack);
		if ( prevId == 41 || expression[i] == 41 )
		{
			push(&OpStack, expression[i]);				
			continue;
		}
		if ( expression[i] == 43 || expression[i] == 45 )
		{
			if ( prevId != 43 && prevId != 45 )
				push_head(stack, pop(&OpStack));
			push(&OpStack, expression[i]);
			continue;
		}
		if ( expression[i] == 42 || expression[i] == 47 )
		{
			if ( prevId == 42 || prevId == 47 )
				push_head(stack, pop(&OpStack));
			push(&OpStack, expression[i]);
			continue;
		}
		else /*if( expression[i] == 41 )*/
		{
			while ( OpStack->count > 1 ) // loop until the right bracket
			{
				// printf("inserting %c\n", peek(&OpStack));
				if ( peek(&OpStack) != 41 )
					push_head(stack, pop(&OpStack)); // clear the content within bracket
				else
				{							
					pop(&OpStack); // remove the right bracket from list
					break;
				}
			}
			continue;
		}	
	}
	while ( OpStack->count > 1 ) // loop until the left bracket
	{
		// printf("inserting %c\n", peek(&OpStack));
		if ( peek(&OpStack) != 41 )
			push_head(stack, pop(&OpStack)); // clear the content within bracket
		else
		{							
			pop(&OpStack); // remove the right bracket from list
			break;
		}
	}
	release(&OpStack);
	return 1;
}
