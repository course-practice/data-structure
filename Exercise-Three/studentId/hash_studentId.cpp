#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 68
#define BUCKET 2

int generatetxt(char *path);
int readtext(int buff[], int size, char *path);
int hashFunction(int key, int bound);
int hash_friend(int key, int bound);
int hashing(int key, int bound);
int insert(int index, int value, int hashArray[], int size, int *collision );
int insert_collision(int index, int value, int hashArray[], int size, int *collision );
int main(void)
{
	// char *path = "student.txt";
	srand(time(NULL));
	char *path = "student.txt";
	int buff[50] = { 0 };
	int hashArray[N] = {0};
	
	int n, i, j, index, total = 0, collision = 0, max = 0, loop = 1000;
	double lf = 0, cumulative = 0;
	
	scanf("%d", &n);
	// readtext(buff, n, path);
	for ( j = 0; j < loop; j++ )
	{
		printf("$ %d\n", generatetxt(path));
		if ( readtext(buff, n, path) )
		{
			for ( i = 0; i < n; i++ )
			{
				//printf("%2d %10d\n", i, buff[i]);
				index = hashFunction(buff[i], N);
				//index = hashing(buff[i], N);
				//index = hash_friend(buff[i], N);
				//lf += insert(index, buff[i], hashArray, BUCKET, &collision);
				lf += insert_collision(index, buff[i], hashArray, BUCKET, &collision);
				total += collision;
				collision = 0;
			}
			puts("\noutput");
			
			for ( i = 0; i < N; i++ )
			{
				if( hashArray[i] >= 2 )
				{
					printf("#2 %d %d\n", i, hashArray[i]);
					if ( hashArray[i] > max )
						max = hashArray[i];
				}
				else
					printf("#1 %d %d\n", i, hashArray[i]);
				hashArray[i] = 0;
			}
			 
			total += max;
			max = 0;
			
			/*
			printf("total collision : %d\n", total );
			// printf("successed : %lf\n", lf);	
			lf /= 34;
			printf("load factor : %lf\n", lf);
			cumulative += lf;
			lf = 0;
			// total = 0;
			for ( i = 0; i < N; i++ )
			{
				hashArray[i] = 0;
			}
			*/
		}		
	} 
	printf("avg : %lf\n", total/(double)loop);
	puts("");
	/*
	for ( i = 0; i < N; i++ )
		printf("%2d %10d\n", i, hashArray[i]);
	*/
	puts("== ENED ==");
	getchar();
	return 0;
} 

int readtext(int buff[], int size, char *path)
{
	FILE *fptr;
	int k = 0, counter = 0;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	
   	while ( !feof(fptr) && k < size )
   	{
   		fscanf(fptr, "%d", &buff[k]);
   		k++;
   	}
   	fclose(fptr); 
   	
   	return 1;
}

int generatetxt(char *path)
{
	FILE *fptr;
	char buff[8] = { 0, 0, 0, 1, 0 };
	int i, j, mul,value;
	if ( ( fptr = fopen(path, "w") ) == NULL )
		printf("\nfile io exception : file not found\n");
	else
	{
		
		for( i = 0; i < 31; i++ )
		{
			buff[0] = rand() % 10 + 10;
			buff[1] = rand() % 10;
			buff[2] = rand() % 10;
			buff[4] = rand() % 10;
			buff[5] = (rand()%2==1)?1:2;
			buff[6] = rand() % 10;
			buff[7] = rand() % 10;
			value = 0;
			mul = 10000000;	
			for ( j = 0; j < 8; j++ )
			{
				value += buff[j] * mul;
				mul /= 10; 
			}
			// printf("value : %d\n", value);
			fprintf(fptr, "%d\n", value);
		}
		fclose(fptr);
		return 1;
	}
	return 0;
}

int hashFunction(int key, int bound)
{
	// 1 0 73 1 61 54
	int b1 = key / 10000000 % 10;
	int b5 = key / 10000 % 10;
	int a = key / 100000 % 100; // 2 digits
	int b = key % 10000 / 100; // 2 digits
	int c = key % 100 / 10; // 1 digits
	int cc = key % 100;
	int d = key % 10; // 1 digits
	d = d * 10 + c;
	// % 24 = 3.4
	int e = c * 10000 + b * 47 + a; // 3.39
	//e = c * 23 + b * 47 + a; // 3.47
	// e = a * b * cc; // 4.51
	//e = a + b + cc; // 3.38
	e = cc * 47 + b * 23 + a * 11 + b1 + b5;
	//e = a + b * c; // 3.95
	//e = a * 7 + b * c; // 3.91
	int bit8 = key / 10000000 % 10;
		int bit7 = key / 1000000 % 10;
		int bit6 = key / 100000 % 10;
		int bit5 = key / 10000 % 10;
		int bit4 = key / 1000 % 10;
		int bit3 = key / 100 % 10;
		int bit2 = key / 10 % 10;
		int bit1 = key % 10;
		
		//e = (long long)((long long)bit1*433494437 + bit7*16769023 + bit6*126001 + bit3*84631 + bit5*3571 + bit8*613 + bit2*911 + bit4*3) % 34;
	
	// printf("# %d %d %d %d %d %d\n", a, b, c, d, d, e%34);
	// int d = key % 100;
	// printf("# %d %d\n", key, e%34);
	return e%34;
}

int hashing(int key, int bound)
{
	int a = key / 1000000 % 10;
	int b = key % 10000;
	int c = a * 10000 + b;
	int d = c % 100 * 1000 + c / 100;
	d = c%47 * 27 * 27 + c%47 * 27 + c%47;
	printf("# %d %d %d %d %d %d\n", a, b, c, d, d, d%34);
	return d%34;
}

int insert(int index, int value, int hashArray[], int size, int *collision )
{
	int i;
	for ( i = index*size; i < index*size + size; i++ )
	{
		if ( hashArray[i] == 0 )
		{
			hashArray[i] = value;
			break;
		}
		else
			(*collision)++;
	}
	printf(" collision : %d\n", *collision);
	return ((*collision) < size)?1:0;
}

int insert_collision(int index, int value, int hashArray[], int size, int *collision )
{
	hashArray[index]++;
	return 1;
}

int hash_friend(int key, int bound)
{
	 int bit7 = key / 100000 % 10;
  int bit3 = key / 100 % 10;
  int bit2 = key / 10 % 10;
  int bit1 = key % 10;
  
  return (bit7*11 + bit3*1 + bit2*15 + bit1*7) % 34;
}
