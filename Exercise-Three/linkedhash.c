#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define DEPTH 3 

struct node {
	int value;
	int start;
	int next;
};
typedef struct node Node;

// hashing solution
int hashing(int key, int bound);
int offset(int key, int old, int bound, int depth);
// insert
int insert(Node list[], int size, int index, int value);
int insert(Node list[], int size, int index, int value);
int linearProbing(Node list[], int size, int start, int value);
// remove and search
int rem(Node list[], int size, int value);
int search(Node list[], int size, int value );
// utitlity
int distinctKey(int randNumber[], int l);
void printList(Node list[], int size);

int main(void)
{
	srand(time(NULL));
	int i, r, index, total = 0, comparison;
	Node list[30];
	int randNumber[30];
	int size = sizeof(list) / sizeof(list[0]);
	
	puts("\n========== INITIALIZE ==========\n");
	for ( i = 0; i < size; i++ )
	{
		list[i].value = 0;
		list[i].next = -2;
		list[i].start = -1;
		randNumber[i] = distinctKey(randNumber, i);
		// printf("distinct value %6d\n", randNumber[i]);
	}
	puts("\n========== INSERT ==========\n");
	for ( i = 0; i < size; i++ )
	{
		index = hashing(randNumber[i], size);
		comparison = insert(list, size, index, randNumber[i]);
		printf("%d >> %d mode : %2d\n", i, randNumber[i], comparison);
	}
	//printList(list, size); comparison = 0;
	/*
	puts("\n========== SEARCH ==========\n");
	for ( i = 0; i < size; i++ )
	{
		comparison = search(list, size, randNumber[i]);
		//comparison = search(list, size, 777);
		printf("comparison : %d\n", comparison);
		total += comparison;
		comparison = 0;
	}
	printf("total : %d\n", total);
	// printf("AVG Comparison : %lf\n", (double)total/(double)size);
	printf("AVG Comparison : %lf\n", (double)total/30);
	*/
	/*
	puts("\n========== REMOVE ==========\n");
	for ( i = size-1; i >= 0; i-- )
	{
		comparison = rem(list, size, randNumber[i]);
		printList(list, size);
		printf("Insert %6d\n", randNumber[i]);
		index = hashing(randNumber[i], size);
		comparison = insert(list, size, index, randNumber[i]);
		printList(list, size);
	}
	*/
	puts("");
	printList(list, size);
	getchar();
	return 0;	
} 

void printList(Node list[], int size)
{
	int i;
	for (i = 0; i<size; i++ )
		printf("i : %d >> %3d %3d %3d\n", i, list[i].start, list[i].next, list[i].value);
}

int hashing(int key,int bound)
{
	return abs( 7 * key ) % bound;
}

int offset(int key, int old, int bound, int depth)
{
	int new_address = key + old;
	new_address = depth * depth + key; // 
	//int os = (key/bound);
	//new_address = abs(os+old);
	return abs(new_address) % bound;
}

int distinctKey(int randNumber[], int l)
{
	int i, r, flag = 1, sign;
	while ( flag == 1 )
	{
		r = rand() % 32769;
		sign = rand()%2; // 1 0
		if ( sign )
			r *= -1;// 32768
		else
			r -= 1;
		flag = 0;
		for ( i = 0; i <= l; i++ )
		{
			if ( randNumber[i] == r )
			{
				flag = 1;
				break;
			}
		}
	}
	return r;
}

int linearProbing(Node list[], int size, int start, int value)
{
	puts("linear probing");
	int i = (start+1==size)?0:start+1;
	while (i != start )
	{
		if ( list[i].next == -2 )
		{
			list[i].next = -1;
			list[i].value = value;
			return i;
		}
		if ( ++i == size )
			i = 0;
	}
}

int recursive_insert(Node list[], int size, int index, int value, int d, int mode)
{
	int new_index;
	if ( list[index].next == -2 )
	{
		if ( list[index].start == index )
		{
			puts("self bypass");		
		}
		else if ( d == 0 )
			list[index].start = index;
		list[index].next = -1;
		list[index].value = value;
		return index;	
	}
	if ( ++d >= DEPTH )
	{
		return linearProbing(list, size, index, value);
	}
	if ( mode == 1 )
	{
		new_index = offset(value, index, size, d);
		return recursive_insert(list, size, new_index, value, d, 1);
	}
	// non-recursive 
	if ( list[index].start == -1 )
	{
		new_index = offset(value, index, size, d);
		list[index].start = recursive_insert(list, size, new_index, value, d, 1);
		return -1;
	}
	if ( list[index].start != index ) // != 
	{
		index = list[index].start;
		new_index = list[index].next;
		if ( new_index != -1 )
		{
			while ( new_index != -1 )
			{
				index = new_index;
				new_index = list[index].next;
				if ( ++d >= 9 )
				{
					list[index].next = linearProbing(list, size, new_index, value);
					return -33;
				}
			}
		}
		new_index = offset(value, index, size, d);
		list[index].next = recursive_insert(list, size, new_index, value, d, 1);
		return -333;
	}
	if (list[index].start == index) // ==
	{
		new_index = list[index].next;
		if ( new_index != -1 )
		{
			while ( new_index != -1 )
			{
				index = new_index;
				new_index = list[index].next;
				if ( ++d >= 9 )
				{
					list[index].next = linearProbing(list, size, new_index, value);
					return -44;
				}
			}
			new_index = offset(value, new_index, size, d);
		}
		else
		{
			new_index = offset(value, index, size, d);
		}
		list[index].next = recursive_insert(list, size, new_index, value, d, 1);
		
		return -4;
	}
}

int insert(Node list[], int size, int index, int value)
{
	return recursive_insert(list, size, index, value, 0, 0);
}

int rem(Node list[], int size, int value)
{
	int key = hashing(value, size), prev = -1, origin, base;
	printf("removing %d\n", value);
	if ( list[key].start == key )
	{
		//puts("Best case");
		if ( list[key].value == value )
		{
			list[key].start = list[key].next; // 
			list[key].next = -2;
			list[key].value = 0;
			return -1;
		}
		while ( list[key].value != value )
		{
			prev = key;
			key = list[key].next;
			if ( key == -1 )
				return 0; 
		}
	}
	else
	{
		//puts("Update Start");
		origin = key;
		key = list[key].start;
		if ( list[key].value == value )
		{
			list[origin].start = list[key].next;// 
			list[key].next = -2;
			list[key].value = 0;
			return -3;
		} 
		while ( list[key].value != value )
		{
			prev = key;
			key = list[key].next;
			if ( key == -1 )
				return 0; 
		}
	}
	printf("Found %d at %d, prev %d\n", value, key, prev );
	list[prev].next = list[key].next;//  
	list[key].next = -2;
	list[key].value = 0;
	return -4;	
}

int search(Node list[], int size, int value )
{
	int key = hashing(value, size);
	int counter = 0;
	counter++;
	printf("Key : %d\n", key);
	if ( list[key].start == -1 )
	{
		//puts("list[key].start == -1");
		printf("value : %d was not found at key : %d\n", value, key);
		return counter;
	}
	if ( list[key].start != key )
	{
		key = list[key].start;
		printf("Get actual start address : key : %d\n", key);
	}
	if ( list[key].next == -2  ) // additional condition || list[key].start == -1
	{
		//puts("list[key].next == -2");
		printf("value : %d was not found at key : %d\n", value, key);
		return counter;
	}
	while ( list[key].value != value )
	{
		//puts("Looping to search()");
		key = list[key].next;
		if ( key == -1 )
		{
			printf("value : %d was not found at key : %d\n", value, key);
			return counter;
		}
		else
			printf("value : %d || key : %d\n", value, key);
		counter++;
	}
	printf("Found %d at %d\n", value, key );
	return counter;
}

int loop_insert(Node list[], int size, int index, int value, int d, int mode)
{
	int index = hashing(value, size);
	int i, target, base = index, origin;
	// direct insert
	if (list[index].next == -2)
	{
		
		return 0;
	}
	// find empty space
	for (i = 0; i < 10; i++)
	{
		index = offset(value, index, size, i);
		if ( list[index].next == -2 )
		{
			target = index;
			return 0;
		}
	}
	// check start
	if ( list[base].start == -1 )
	{
		list[base].start = target;
		// new node
	}
	
	if ( list[base].start == base )
	{
		temp = base;
		while ( list[temp].next != -1 )
		{
			temp = list[temp].next;		
		}
		list[temp].next = target;	
	}
	
	if ( list[base].start != base ) 
	{
		// origin = base;
		temp = list[origin].start;
		while ( list[temp].next != -1 )
		{
			temp = list[temp].next;
		}
		list[temp].next = target;
	}
}



