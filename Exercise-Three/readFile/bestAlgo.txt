/* 

int hashing(char key[], int bound)
{
	return ( 128 * key[0] + 12800 * key[1] + 1000 * key[2] ) % bound;
}
*/ 

/*

int hashing(char key[], int bound)
{
	return ( 128 * key[0] + 127 * key[1] + 999 * key[2] ) % bound;
}

*/

/*

int hashing(char key[], int bound)
{
	return ( 128 * (key[0]%26) + 127 * (key[1]%26) + 999 * (key[2]%26) ) % bound;
}

*/

/*

int hashing(char key[], int bound)
{
	int a = key[0] % 27;
	int b = key[1] % 27;
	int c = key[2] % 27;
	return (127 * a * a + 277 * b + 2 * c * c * c) % bound;
	// return ( 7 * (key[0]) + 17 * (key[1]) + 128 * (key[2]) ) % bound;
}

*/
