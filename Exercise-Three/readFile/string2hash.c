#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
// global variables
char hashArray[27000][4] = { 0 };
char data_set[100][4] = { 0 };
int table[27000] = { 0 };
int tab_hundred[100] = { 0 };
// function prototype
int generatetxt(char *path);
int readtext(char set[100][4], char *path);
// int readtext_hundred(char raw[100][4], int tab[100], char *path);
int hashing(char key[], int bound);
int hashing_hundred(char key[], int bound);
int insert(char data[100][4], char list[27000][4], int tab[], int bound);
int insert_hundred(char data[100][4], int tab[], int bound);
int search(char raw[27000][4], char key[], int raw_bound, int key_size );
void printTable(char list[][4], int size);
void clearTable(char array[27000][4], int width, int height);
// main program
int main()
{
	srand(time(NULL));
	char *path = "raw.txt";
	char buff[4] = { 0 };
	generatetxt(path);
	readtext(data_set, path);
	printTable(data_set, 100);
	int collision = insert(data_set, hashArray, table, 27000);
	printf("\ncollision : %d\n", collision);
	
	scanf("%3[^\n]s", buff);
	int loc = search(hashArray, buff, 27000, 3);
	if ( loc >= 0 )
		printf("\n%s found at %d\n", buff, loc);
	else
		printf("\n%s was not found", buff);
	clearTable(hashArray, 27000, 4);
	// readtext_hundred(hashArray, tab_hundred, path);
	puts("hundred space");
	collision = insert_hundred(data_set, tab_hundred, 100 );
	printf("\ncollision : %d\n", collision);
   	return 0;
}

int insert(char data[100][4], char list[27000][4], int tab[], int bound)
{
	int i, j, collision = 0, index;
	for ( i = 0; i < 100; i++ )
	{
		index = hashing(data[i], bound);
		if ( tab[index] == 1 )
			collision++;
		else
		{
			tab[index]++;
			for ( j = 0; j < 3; j++ )
				list[index][j] = data[i][j];
		}
	}
	return collision;
}
int insert_hundred(char data[100][4], int tab[], int bound)
{
	int i, j, collision = 0, index;
	for ( i = 0; i < 100; i++ )
	{
		index = hashing_hundred(data[i], bound);
		if ( tab[index] == 1 )
			collision++;
		else
		{
			tab[index]++;
		}
	}
	return collision;
}
int generatetxt(char *path)
{
	FILE *fptr;
	char buff[3] = { 0 };
	int i, j;
	if ( ( fptr = fopen(path, "w") ) == NULL )
		printf("\nfile io exception : file not found\n");
	else
	{
		for( i = 0; i < 100; i++ )
		{
			for( j = 0; j < 3; j++ )
				buff[j] = ( rand() % 25 ) + 97;
			fprintf(fptr, "%s", buff);
		}
		fclose(fptr);
		return 1;
	}
	return 0;
}

int hashing(char key[], int bound)
{
	/*
	int a = key[0] % 100 + 1;
	int b = key[1] % 'a' + 1;
	int c = key[2] % 'a' + 1;
	int d = key[0] % 10 + 1;
	return abs( 128 * a + 447 * (a+b) - 3 * (c-a) + d) % bound;
	// return (127 * a * a + 277 * b + 2 * c * c * c) % bound;
	// return ( 7 * (key[0]) + 17 * (key[1]) + 128 * (key[2]) ) % bound;
	*/
	int a = key[0] % 25;
	int b = key[1] % 27 + 1;
	int c = key[2] % 27 + 1;
	int d = (a * 1000 + (b + c)%1000);
	printf("d : %d\n", d);
	return d;
}

int hashing_hundred(char key[], int bound)
{
	int a = key[0] % 25;
	int b = key[1] % 27;
	int c = key[2] % 27;
	int d = (a * 4 + (b + c) % 4 ) % 100;
	printf("d : %d\n", d);
	return d;
}

int readtext(char raw[100][4], char *path)
{
	
	FILE *fptr;
	char buff[4] = { 0 };
	int i = 1, j, k = 0, counter = 0, index, r;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	r = ( rand() % 3 ) + 1;
   	for ( i = 0; i < r; i++ )
   		fscanf(fptr,"%1s", &buff[i]);
	// fscanf(fptr,"%3s", buff);
   	while ( !feof(fptr) && k < 100 )
   	{
   		// printf("%s\n", raw[i]);
   		r = ( rand() % 3 ) + 1;
	   	for ( i = 0; i < r; i++ )
	   		fscanf(fptr,"%1s", &buff[i]);
   		for ( j = 0; j < 3; j++ )
   			raw[k][j] = buff[j];
   		k++;
   	}
   	fclose(fptr); 
   	return 1;
}

int search(char raw[27000][4], char key[], int raw_bound, int key_size )
{
	int index = hashing(key, raw_bound);
	if ( index > raw_bound )
	{
		printf("exception index out of bound %d\n", index);
		return -1;
	}
	if ( raw[index][0] == key[0] && raw[index][1] == key[1] && raw[index][2] == key[2] )
		return index;
	return -1;
}

void clearTable(char array[27000][4], int width, int height)
{
	int i, j; puts("\nclear array");
	for( i = 0; i < width; i++ )
	{
		for( j = 0; j < height; j++ )
			array[i][j] = 0;
	}
}

int readtext_hundred(char raw[100][4], int tab[100], char *path)
{
	
	FILE *fptr;
	char buff[4] = { 0 };
	int i = 1, j, counter = 0, index, r;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
	r = ( rand() % 3 ) + 1;
   	for ( i = 0; i < r; i++ )
   		fscanf(fptr,"%1s", &buff[i]);
   	while ( !feof(fptr) )
   	{
   		r = ( rand() % 3 ) + 1;
	   	for ( i = 0; i < r; i++ )
	   		fscanf(fptr,"%1s", &buff[i]);
   		index = hashing_hundred(buff,100);
   		printf("%s %d\n", buff, index);
   		if ( tab[index] == 1 )
   			counter++;
   		else
   		{
   			for ( j = 0; j < 3; j++ )
   				raw[index][j] = buff[j];
   			tab[index] = 1;
		}
   		i++;
   		printf("collision : %d\n", counter);
   	}
   	printf("collision : %d\n", counter);
   	fclose(fptr); 
   	return 1;
}

void printTable(char list[][4], int size)
{
	puts("\nentire table");
	int i, j = 0;
  	for ( i = 0; i < size; i++ )
  	{
  		if ( list[i][0] != 0 )
  		{
  			printf("%d : %s ", i, list[i]);
  			if ( (j+1) % 10 == 0 )
  				puts("");
  			j++;
		}
  		
	}
}
