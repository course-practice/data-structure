#include <iostream>
#include <string>
using namespace std;

class Car{
	public :
		Car(string name, int speed, double price)
		{
			this->brand = name;
			this->speed = speed;
			this->price = price;
		}
		int getSpeed()
		{
			return this->speed;
		}
		int getPrice()
		{
			return this->price;
		}
		string getName()
		{
			return this->brand;
		}
	private :
		string brand;
		int speed;
		double price;
};
