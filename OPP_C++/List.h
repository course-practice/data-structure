#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

typedef struct node {
			int value;
			struct node *link;
			node(){
				value = 0;
				link = NULL;
			}
} Node;

class List{
	public :
		List(string str)
		{
			cout << str << endl;
			count = 0;
		}
		
		Node * createNode(int value)
		{
			Node *n = (Node*)malloc(sizeof(Node));
			if ( n != NULL )
				n->value = value;
			n->link = NULL;
			return n;
		}
		
		int push(int value)
		{
			Node *n = createNode(value);
			if ( n != NULL )
			{
				n->link = queue.link;
				queue.link = n;
				count++;
				return 1;
			}
			return 0;
		}	
		int pop()
		{
			int value;
			Node *n = queue.link;
			queue.link = queue.link->link;
			value = n->value;
			count--;
			free(n);
			return value;
		}
		int insert(int value)
		{
			Node *n = createNode(value);
			if ( n != NULL )
			{
				Node *current = queue.link, *prev = NULL;
				while ( current != NULL && current->value < value )
				{
					prev = current;
					current = current->link;
				}
				count++;
				if ( prev == NULL )
				{
					n->link = queue.link;
					queue.link = n;
					return 1;
				}
				prev->link = n;
				n->link = current;
				return 1;
			}
			return 0;
		}
		int search(int value)
		{
			Node *current = queue.link;
			while ( current != NULL )
			{
				if ( current->value == value )
				{
					return 1;
				}
				current = current->link;	
			}
			return 0;
		}
		int remove(int value)
		{
			Node *current = queue.link, *temp = NULL, *prev = NULL;
			int flag = 0, v;
			while ( current != NULL )
			{
				if ( current->value == value )
				{
					temp = current;
					flag = 1;
					break;
				}
				prev = current;
				current = current->link;
			}
			if ( flag == 1 )
			{
				if ( prev == NULL )
					queue.link = (*(current->link)).link;
				else if ( temp != NULL )
					prev->link = current->link;
				v = temp->value;
				free(temp);
				count--;
				return v;
			}
			return -1;
		}
		int isEmpty(){
			return count > 0;
		}
	private :
		Node queue;
		int count;
};






