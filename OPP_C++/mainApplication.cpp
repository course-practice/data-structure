#include <iostream>
#include "Car.h"
#include "List.h"
#include <map>
#include <string>

#include <stdio.h>
using namespace std;

int main(void)
{
	/* 
	Car car("VOLK", 99, 127);
	cout << "BRAND : " << car.getName() << " SPEED : " << car.getSpeed() << " PRICE : " << car.getPrice() << endl;
	List list("fasd");
	list.push(90);
	list.insert(9);
	list.insert(23);
	cout << "remove : " << list.remove(9) << endl;
	cout << "remove : " << list.remove(88) << endl;
	list.push(100);
	list.push(13);
	cout << list.pop() << endl;
	
	map<string, int> my_map;
	my_map["japan"] = 91;
	cout << my_map["japan"] << endl;
	*/
	
	int n, i, j, len, input, prev, flag = 0;
	scanf("%d", &n);
	for ( i = 0; i < n; i++ )
	{
		scanf("%d", &len);
		int array[50000] = { 0 };
		for ( j = 0; j < len; j++ )
		{
			scanf("%d", &input);
			if ( array[input] >= 1 )
			{
				if ( prev != input || array[input] >= 2 )
				{
					flag = 1;
				}
			}
			array[input] += 1;
			prev = input;
		}
		if ( flag == 1 )
		{
			printf("YES\n");
			flag = 0;
		}
		else{
			printf("NO\n");
		}
	}
	getchar();
	return 0;
}
