#include <stdio.h>
#include <stdlib.h>

struct stackNodeTag{
	void *dataPtr;
	struct stackNodeTag *link;
};

struct stackTag{
	int count;
	struct stackNodeTag *top;
};

struct stackTag* stackCreate( void )
{
	struct stackTag *stack;
	puts("A");
	stack = malloc( sizeof( struct stackTag ) );
	puts("B");
	if ( stack != NULL )
	{
		puts("C");
		stack->count = 0;
		stack->top = NULL;
	}
	puts("D");
	printf("\n stack pointer : %p", *stack);
	return stack;
}

int stackPush( struct stackTag *stack, void *dataPtr )
{
	struct stackNodeTag *newNode;
	puts("A");
	newNode = malloc(sizeof(struct stackNodeTag));
	puts("B");
	
	if ( newNode != NULL )
	{
		puts("C");
		newNode->dataPtr = dataPtr;
		newNode->link = stack->top;
		stack->top = newNode;
		(stack->count)++;
		puts("D");
		return 1;
	}
	
	puts("E");
	return 0;
}

void *stackPop( struct stackTag *stack )
{
	void *dataPtr;
	if ( stack->count == 0 )
	{
		puts("NULL pointer");
		dataPtr = NULL;
	}
	else
	{
		dataPtr = stack->top->dataPtr;
		stack->top = stack->top->link;
		(stack->count)--;
	}
	return dataPtr;
}

void *stackGet( struct stackTag *stack )
{
	if ( stack->count == 0 )
		return NULL;
	else
		return stack->top->dataPtr;
}


void *stackSearch(struct stackTag *stack, void *dataPtr )
{
	printf("Key : %d\n", *(int *)dataPtr );
	if( stack->count == 0 )
		return NULL;
	else
	{
		struct stackNodeTag *currentNode = NULL;
		currentNode = stack->top;
		while ( currentNode != NULL )
		{
			// printf("%d => ", *((int *)(currentNode->dataPtr)));
			if ( *((int *)(currentNode->dataPtr)) == *(int *)dataPtr )
				return ((int *)(currentNode->dataPtr));
			currentNode = currentNode->link;
		}
		return NULL;
	}
	return NULL;
}

void traverse( struct stackTag *stack )
{
	struct stackNodeTag *currentNode = NULL;
	currentNode = stack->top;
	while ( currentNode != NULL )
	{
		printf("%d => ", *((int *)(currentNode->dataPtr)));
		currentNode = currentNode->link;
	}
	puts("EOL");
}
int stackEmpty( struct stackTag *stack )
{
	return stack->count == 0;
}

int stackFull( struct stackTag *stack )
{
	struct stackNodeTag *newNode;
	newNode = malloc( sizeof( struct stackNodeTag ) );
	if( newNode != NULL )
	{
		free(newNode);
		return 0;
	}
	return 1;
}

int stackCount( struct stackTag *stack )
{
	return stack->count;
}

struct stackTag *stackRelease( struct stackTag * stack )
{
	struct stackNodeTag *nodePtr;
	if( stack )
	{
		while( stack->top != NULL )
		{
			free( stack->top->dataPtr );
			nodePtr = stack->top;
			stack->top = stack->top->link;
			free( nodePtr );
		}
		free( stack );
	}
	else
		puts("empty list");
	return NULL;
}

int main( void )
{
	int num;
	int *digit;
	
	struct stackTag *stack;
	int choice;
	scanf("%d", &choice);
	while( choice != 0 )
	{
		switch(choice)
		{
			case 1:
				puts("stackCreate");
				stack = stackCreate();
				break;
			case 2:
				puts("stackPush");
				digit = (int*)malloc(sizeof( int ));
				scanf("%d", digit );
				stackPush( stack, digit );
				traverse(stack);
				break;
			case 3:
				puts("stackPop");
				digit = ( int*)stackPop( stack );
				if ( digit != NULL )
					printf("%d\n", *digit);
				break;
			case 4:
				puts("stackRelease");
				stack = stackRelease( stack );
				puts("released");
				break;
			case 5:
				puts("stackCount");
				num = stackCount( stack );
				printf( "stack count : %d\n", num );
				break;
			case 6:
				puts("stackFull");
				num = stackFull(stack);
				printf("FULL : %s\n", (num==1)?"TRUE":"FALSE");
				break;
			case 7:
				puts("stackEmpty");
				num = stackEmpty(stack);
				printf("EMPTY : %s\n", (num==1)?"TRUE":"FALSE");
				break;
			case 8:
				puts("stackSearch");
				digit = (int*)malloc(sizeof( int ));
				scanf("%d", digit );
				num = *digit;
				digit = ( int*)stackSearch( stack, digit );
				if ( digit != NULL )
					printf("%d was found\n", *digit);
				else
					printf("%d not found\n", num );
				break;
			default:
				break;
				
		}
		scanf("%d", &choice);
	}
	puts("termination");
	if( stack != NULL )
	{
		stackRelease( stack);
		puts("released");
	}
	getchar();
	return 0;
}
