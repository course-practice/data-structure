#include <stdio.h>
#include <stdlib.h>

struct listNode {
	int data;
	struct listNode *NextPtr;
};

typedef struct listNode ListNode;
typedef ListNode *ListNodePtr;

void push(ListNodePtr *sPtr, int value);
void insert(ListNodePtr *sPtr, int value);
int pop(ListNodePtr *sPtr);
int deleteKey(ListNodePtr *sPtr, int key);
int searchIndex(ListNodePtr *sPtr, int index);
int searchKey(ListNodePtr *sPtr, int key);
int isEmpty(ListNodePtr *sPtr);
int isFull(ListNodePtr *sPtr);
void traverse(ListNodePtr currentPtr);
void destroy(ListNodePtr *sPtr);
int hasNext(ListNodePtr currentPtr);
ListNodePtr reverse(ListNodePtr *sPtr);

int main( void )
{
	ListNodePtr startPtr = NULL;
	int choice = 0, value, index, key;
	while( choice != -1 )
	{
		switch( choice ){
			case 1:
				puts("push");
				scanf("%d", &value);
				push(&startPtr, value);
				traverse(startPtr);
				break;
			case 2:
				puts("insert");
				scanf("%d", &value);
				insert(&startPtr, value);
				traverse(startPtr);
				break;
			case 3:
				puts("pop");
				value = pop(&startPtr);
				if ( value != -1 )
				{
					printf("%d was extracted\n", value);
					traverse(startPtr);
				}
				break;
			case 4:
				puts("deleteKey");
				scanf("%d", &key);
				value = deleteKey( &startPtr, key);
				if ( value != -1 )
				{
					printf("%d was extracted\n", key);
					traverse(startPtr);
				}
				break;
			case 5:
				puts("searchIndex"); // query to remove node
				scanf("%d", &index);
				value = searchIndex( &startPtr, index );
				if ( value != -1 )
					printf("%d was extracted\n", value);
				break;
			case 6:
				puts("searchKey"); // query to remove node
				scanf("%d", &key);
				value = searchKey(&startPtr, key);
				if ( value != -1 )
					printf("%d was extracted\n", value);
				break;
			case 7:
				puts("isEmpty");
				if( isEmpty(&startPtr) )
					puts("TRUE");
				else
					puts("FALSE");
				break;
			case 8:
				puts("isFull");
				// have no idea how to know whether a linked list is full
				break;
			case 9:
				puts("traverse");
				traverse(startPtr);
				break;
			case 10:
				puts("destroy");
				destroy(&startPtr);
				traverse(startPtr);
				break;
			case 11:
				puts("reverse");
				startPtr = reverse(&startPtr);
				traverse( startPtr );
				break;
			default:
				break;
		}
		scanf( "%d", &choice );
	}
		
	getchar();
	return 0;
}

void push(ListNodePtr *sPtr, int value)
{
	ListNodePtr currentPtr = *sPtr;
	ListNodePtr newPtr = malloc(sizeof( ListNode ));
	if( newPtr != NULL )
	{
		newPtr->data = value;
		newPtr->NextPtr = *sPtr;
		*sPtr = newPtr;
	}
	else
	{
		puts("memory not available");
	}
}

void insert(ListNodePtr *sPtr, int value)
{
	ListNodePtr previousPtr, currentPtr;
	ListNodePtr newPtr = malloc(sizeof(ListNode));
	puts("inserting...");
	if( newPtr != NULL )
	{
		previousPtr = NULL;
		currentPtr = *sPtr;
		newPtr->data = value;
		newPtr->NextPtr = NULL;
		puts("X");
		while( currentPtr != NULL && value > currentPtr->data )
		{
			puts("A");
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
		}
		if( previousPtr == NULL )
		{
			puts("B");
			newPtr->NextPtr = *sPtr;
			*sPtr = newPtr;
		}
		else
		{
			previousPtr->NextPtr = newPtr;
			newPtr->NextPtr = currentPtr;
			puts("C");
		}
	}
	else
	{
		puts("memory not available");
	}
}

void traverse(ListNodePtr currentPtr)
{
	if( currentPtr != NULL )
	{
		while( currentPtr != NULL )
		{
			printf("%d => ", currentPtr->data );
			currentPtr = currentPtr->NextPtr;
		}
		printf("EOL\n");
	}
	else
		puts("Traverse failed : empty list");
}

int pop(ListNodePtr *sPtr){
	if( *sPtr != NULL )
	{
		int value = 0;
		ListNodePtr currentPtr = *sPtr;
		ListNodePtr tempPtr = currentPtr;
		value = tempPtr->data;
		currentPtr = currentPtr->NextPtr;
		*sPtr = currentPtr;
		free( tempPtr );
		return value;
	}
	else
	{
		puts("POP failed : empty list");
		return -1;	
	}	
}

int deleteKey( ListNodePtr *sPtr, int key )
{
	if( *sPtr != NULL )
	{
		ListNodePtr previousPtr = *sPtr;
		ListNodePtr currentPtr = previousPtr->NextPtr;
		ListNodePtr tempPtr = NULL;
		
		while( currentPtr != NULL && key != currentPtr->data )
		{
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
		}
		if( currentPtr != NULL )
		{
			if ( key == currentPtr->data )
			{
				puts("accident");
				tempPtr = currentPtr;
				if( previousPtr == NULL )
				{
					previousPtr = currentPtr->NextPtr;
				}
				else
					previousPtr->NextPtr = currentPtr->NextPtr;
				free( tempPtr );
				return key;
			}
		}
		else
		{
			printf("%d not found\n", key);
		}
	}
	else
		puts("delted failed : emtpy list");
	return -1;	
}

int searchIndex(ListNodePtr *sPtr, int index){
	if( *sPtr != NULL )
	{
		ListNodePtr previousPtr = NULL;
		ListNodePtr currentPtr = *sPtr;
		int currentIndex = 0;
		while( currentPtr != NULL && index != currentIndex )
		{
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
			currentIndex++;
		}
		if( currentPtr == NULL )
		{
			printf("%d is beyond list length\n", index);
		}
		else
		{
			return currentPtr->data;
		}
	}
	else
		puts("searchIndex failed : empty list");
	return -1;
}

int searchKey(ListNodePtr *sPtr, int key)
{
	if( *sPtr != NULL )
	{
		ListNodePtr previousPtr = NULL;
		ListNodePtr currentPtr = *sPtr;
		while( currentPtr != NULL && key != currentPtr->data )
		{
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
		}
		if( currentPtr == NULL )
			printf("%d not found\n", key);
		else
			return currentPtr->data;
	}
	else
		puts("searchKey failed : empty list");
	return -1;
}

int isEmpty( ListNodePtr *sPtr )
{
	return *sPtr == NULL;
}

void destroy(ListNodePtr *sPtr)
{
	if( *sPtr != NULL )
	{
		ListNodePtr previousPtr = *sPtr;
		ListNodePtr currentPtr = previousPtr->NextPtr;
		ListNodePtr tempPtr = NULL;
		while ( currentPtr != NULL ){
			tempPtr = currentPtr;
			previousPtr->NextPtr = currentPtr->NextPtr;
			currentPtr= currentPtr->NextPtr;
			free(tempPtr);
		}
		free( previousPtr );
		*sPtr = NULL;
	}
	else
		puts("destroy operation failed : empty list");
}

int hasNext(ListNodePtr currentPtr)
{
	return currentPtr->NextPtr != NULL;
}
ListNodePtr reverse(ListNodePtr *sPtr)
{
	ListNodePtr previousPtr = *sPtr;
	ListNodePtr currentPtr = NULL;
	ListNodePtr tempPtr = NULL;
	
	int FLAG = 0;
	if ( previousPtr != NULL )
	{
		if ( previousPtr->NextPtr != NULL )
		{
			currentPtr = previousPtr->NextPtr; // safe timing to asign from previousPtr->NextPtr
			while ( currentPtr != NULL )
			{
				if( FLAG == 0 )
				{
					// this block only get executed once
					if ( hasNext(currentPtr) ) // whether there's third node
					{
						// more than 2 nodes
						tempPtr = currentPtr->NextPtr;
						currentPtr->NextPtr = previousPtr;
						previousPtr->NextPtr = NULL;
						previousPtr = currentPtr;
						currentPtr = tempPtr;
						FLAG = 1;
					}
					else
					{
						// tail
						// only two nodes
						currentPtr->NextPtr = previousPtr;
						previousPtr->NextPtr = NULL; 
						previousPtr = currentPtr;
						// currentPtr = tempPtr; // query the existence of this code
						FLAG = 1;
						break;
					}
				}
				else
				{
					if ( hasNext( currentPtr ) )
					{
						tempPtr = currentPtr->NextPtr; // hold the address of the next node
						currentPtr->NextPtr = previousPtr; // reverse direction of current node
						previousPtr = currentPtr; // get ready for the next loop
						currentPtr = tempPtr; // get ready for the next loop
					}
					else
					{
						// tail
						currentPtr->NextPtr = previousPtr; 
						previousPtr = currentPtr; 
						break;
					}
				}
			}
		}
		else
		{
			puts("reverse failed : single node");
		}
		// *sPtr = previousPtr;
		return previousPtr;
	}
	else
	{
		puts("reverse failed : empty list");
	}
	return NULL;
}
