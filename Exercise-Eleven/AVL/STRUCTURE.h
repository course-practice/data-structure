typedef struct LEAVE{
	int bf, v, ht;
	struct LEAVE *left, *right;
}Leave;

typedef struct TREE{
	int count;
	Leave *root;
}Tree;

Leave *createLeave(int value)
{
	Leave *leave = (Leave*)malloc(sizeof(Leave));
	if ( leave != NULL )
	{
		leave->v = value;
		leave->ht = leave->bf = 0;
		leave->left = leave->right = NULL;
	}
	return leave;
}

void initT(Tree *tree)
{
	tree->count = 0;
	tree->root = NULL;
}

int getTreeHeight(Tree *tree)
{
	if ( tree->root == NULL ) return -1;
	return tree->root->ht;
}

int contains(Leave *root, int value)
{
	if ( root == NULL ) return 0;
	if ( root->v > value ) {
		return contains(root->left, value);
	}
	else if ( root->v < value ) return contains(root->right, value);
	return 1;
}

void inOrder(Leave *root)
{
	if ( root == NULL ) return;
	inOrder(root->left);
	printf("%d >>%s", root->v, (((root->v+1)%10==0)?"\n":"\t"));
	inOrder(root->right);
}

void preOrder(Leave *root)
{
	if ( root == NULL ) return;
	printf("%d >> ", root->v );
	preOrder(root->left);
	preOrder(root->right);
}

void postOrder(Leave *root)
{
	if ( root == NULL ) return;
	postOrder(root->left);
	postOrder(root->right);
	printf("%d >> ", root->v);
}
