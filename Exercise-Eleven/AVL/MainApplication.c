#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// SELF DEFINE LIBRARY
#include "STRUCTURE.h"
// function prototype
void update(Leave *node);
Leave *balance(Leave *node);
Leave *recursiveInsert( Leave *root, int value );
int insert(Tree *tree, int value);
Leave *RRotation(Leave *leave);
Leave *LRotation(Leave *leave);
Leave *RLRotation(Leave *leave);
Leave *LRRotation(Leave *leave);
// main program
int main(void)
{
	int i; Tree tree; initT(&tree);
	for ( i = 0; i < 100; i++ )
	{
		printf(">> Stat : %d\n", insert(&tree,i));
	}
	printf(">> Tree Size : %d\n", tree.count);
	puts("\nIn Order");
	inOrder(tree.root);
	puts("\nPre Order");
	preOrder(tree.root);
	puts("\nPost Order");
	postOrder(tree.root);
	printf("\n>> Tree Height : %d\n", tree.root->ht);
	for ( i = 0; i < 20; i++ )
	{
		printf(">> Stat : %d\n", removeB(&tree,i));
	}
	puts("\nIn Order");
	inOrder(tree.root);
	return 0;
}


void update(Leave *node)
{
	int left = (node->left==NULL) ? -1 : node->left->ht;
	int right = (node->right==NULL) ? -1 : node->right->ht;
	
	node->ht = 1 + ( ( left > right ) ? left : right );
	
	node->bf = right - left;
}

Leave *balance(Leave *node)
{
	if ( node->bf == -2 )
	{
		if ( node->left->bf <= 0 )
		{
			return RRotation(node);
		}
		else
		{
			return LRRotation(node);
		}
	}
	else if ( node->bf == 2 )
	{
		if ( node->right->bf >= 0 )
		{
			return LRotation(node);
		}
		else
		{
			return RLRotation(node);
		}
	}
	return node;
}

Leave *recursiveInsert( Leave *root, int value )
{
	if ( root == NULL )
	{
		return createLeave(value);
	}
	if ( value > root->v ) root->right = recursiveInsert( root->right, value );
	// if ( newNode->v < (*root)->v ) 
	else root->left = recursiveInsert(root->left, value ); 
	
	update(root);
	
	return  balance(root);
}

int insert(Tree *tree, int value)
{
	if ( contains(tree->root, value) == 0 )
	{
		tree->root = recursiveInsert(tree->root,value);
		tree->count += 1;
		return 1;
	}
	return 0;
}

int findMax(Leave *leave)
{
	Leave *current = leave;
	while ( current->right != NULL ) current = current->right;
	return current->v;
}

int findMin(Leave *leave)
{
	Leave *current = leave;
	while ( current->left != NULL ) current = current->left;
	return current->v;
}

Leave *recursiveRemove( Leave *root, int value )
{
	if ( root == NULL ) return;
	if ( value > root->v ) root->right = recursiveRemove( root->right, value );
	else if ( value < root->v ) root->left = recursiveRemove( root->left, value );
	else
	{
		// found
		if ( root->left == NULL ) return root->right;
		else if ( root->right == NULL ) return root->left;
		else
		{
			// both branch has subtree
			int toor;
			if ( root->left->ht > root->right->ht )
			{
				toor = findMax(root->left);
				root->v = toor;
				root->left = recursiveRemove(root->left, toor);
			}
			else
			{
				toor = findMin(root->right);
				root->v = toor;
				root->right = recursiveRemove(root->right, toor);
			}
		}
	}
	update(root);
	return balance(root);
}

int removeB(Tree *tree, int value)
{
	if ( contains(tree->root, value) )
	{
		tree->root = recursiveRemove(tree->root, value);
		tree->count -= 1;
		return 1;
	}
	return 0;
}
Leave *LRotation(Leave *leave)
{
	Leave *parent = leave->right;
	leave->right = parent->left;
	parent->left = leave;
	
	update(leave);
	update(parent);
	return parent;
}

Leave *RRotation(Leave *leave)
{
	Leave *parent = leave->left;
	leave->left = parent->right;
	parent->right = leave;
	
	update(leave);
	update(parent);
	return parent;
}

Leave *LRRotation(Leave *leave)
{
	leave->left = LRotation(leave->left);
	return RRotation(leave);
}

Leave *RLRotation(Leave *leave)
{
	leave->right = RRotation(leave->right);
	return LRotation(leave);
}
