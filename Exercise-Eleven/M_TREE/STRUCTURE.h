#define M 3

typedef struct B_NODE{
	int value;
	struct B_NODE *left, *right;
}B_Node;

typedef struct M_NODE{
	int values[M-1], i;
	struct M_NODE *branches[M];
}M_Node;

typedef struct BTREE{
	B_Node *root;
}BTree;

typedef struct MTREE{
	M_Node *root;
}MTree;

void initBT(BTree *tree)
{
	tree->root = NULL;
}
void initMT(MTree *tree)
{
	tree->root = NULL;
}
B_Node *createB(int value)
{
	B_Node *b = (B_Node*)malloc(sizeof(B_Node));
	if ( b != NULL )
	{
		b->left = b->right = NULL;
		b->value = value;
	}
	return b;
}

M_Node *createM(int value)
{
	M_Node *m = (M_Node*)malloc(sizeof(M_Node));
	if ( m != NULL )
	{
		int seq;
		m->i = 1;
		for ( seq = 0; seq < M; seq++ )
			m->branches[seq] = NULL;
		for ( seq = 0; seq < M-1; seq++ )
			m->values[seq] = 0;
		m->values[0] = value;
	}
	return m;
}


