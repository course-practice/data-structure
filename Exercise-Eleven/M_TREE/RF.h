#define COUNT 100
int generate_TestData(char *path, const int count, const int bound)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "w") ) == NULL )
	{
		printf("IO EXCEPTION\n");
		return 0;
	}
	int seq, k, value = 77, data[COUNT] = { 0 };
	
	for ( seq = 0; seq < count; seq++ )
	{
		int r = rand() % 10000;
		for ( k = 0; k < seq; k++ )
		{
			if ( data[k] == r )
			{
				seq--;
				break;
			}
		}
		data[seq] = r;
	}
	for ( seq = 0; seq < count; seq++ )
		fprintf(fptr, "%d%s", data[seq], (seq+1!=count)?"\n":"");
	fclose(fptr);
	return 1;
}
