#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// self define library
#include "RF.h"
#include "STRUCTURE.h"
// constant variables
#define PATH "data.txt"
#define COUNT 100
#define BOUND 200000
#define M 3
#define KEYS 20
// global variables
int CMP = 0;
int MOV = 0;
// function prototype
int insert_array(int array[COUNT], int value);
int buildArray(char *path, int bin[], const int count);
int build_Tree(char *path, BTree *tree, const int count);
int build_MTree(char *path, MTree *tree, const int count);
void recursive_insertB(B_Node **root, int value);
void recursive_insertM(M_Node **root, int value);
int Search_Array(int array[], int count, int target);
int recursive_BSA(int array[], int begin, int last, int target );
int Search_Tree(B_Node *root, int target);
int Search_M(M_Node *root, int target);
M_Node* Remove_M(M_Node **root, int target);
M_Node* testing(M_Node **root, int target);
void printArray(int bin[], const int count);
void inOrder(B_Node *root);
void inOrderM(M_Node *root);
// main function
int main(void)
{
	srand(time(NULL));
	int F[COUNT] = { 0 };
	int seq;
	if ( generate_TestData(PATH, COUNT, BOUND) == 0 )
	{
		puts("FILE CANNOT OPEN\n");
		return 0;
	}puts("");
	for ( seq = 0; seq < 100; seq++ ) printf("=");
	puts("");
	printf(">> Build Binary Array\n");
	if ( buildArray(PATH, F, COUNT) == 0 )
	{
		puts("FILE CANNOT OPEN\n");
		return 0;
	} printArray(F, COUNT);puts("");
	for ( seq = 0; seq < 100; seq++ ) printf("=");
	puts("");
	getchar();
	printf(">> Build Binary Tree\n");
	BTree B_T; initBT(&B_T);
	if ( build_Tree(PATH, &B_T, COUNT) == 0 )
	{
		puts("FILE CANNOT OPEN\n");
		return 0;
	} inOrder(B_T.root); puts("");
	for ( seq = 0; seq < 100; seq++ ) printf("=");
	puts("");
	getchar();
	printf(">> Build M-Way Tree\n");
	MTree M_T; initMT(&M_T);
	if ( build_MTree(PATH, &M_T, COUNT) == 0 )
	{
		puts("FILE CANNOT OPEN\n");
		return 0;
	} inOrderM(M_T.root); puts("");
	getchar();
	printf(">> Search On Array\n");
	int keys[KEYS] = { 0 }, cumulative_cmp = 0;
	for ( seq = 0; seq < KEYS; seq++ ) keys[seq] = rand() % BOUND + 20;
	keys[0] = 50; keys[KEYS-1] = 19917;
	for ( seq = 0 ; seq < KEYS; seq++ )
	{
		int found = Search_Array(F, COUNT, keys[seq]);
		printf("Searching... %d ", keys[seq]);
		if ( found ) printf("FOUND. ");
		else printf("NOT FOUND. ");
		printf("%d comparison\n", CMP);
		cumulative_cmp += CMP; CMP = 0;
	}
	printf("AVG Comparison : %4.6lf\n", (double)cumulative_cmp/(double)KEYS);
	puts("");
	for ( seq = 0; seq < 100; seq++ ) printf("=");
	puts(""); 
	getchar();
	printf(">> Search On Binary Tree\n");
	cumulative_cmp =0;
	for ( seq = 0; seq < KEYS; seq++ )
	{
		printf("Searching... %d ", keys[seq]);
		int found = Search_Tree(B_T.root, keys[seq]);
		if ( found ) printf("FOUND. ");
		else printf("NOT FOUND. ");
		printf("%d comparison\n", CMP);
		cumulative_cmp += CMP; CMP = 0;
	}	
	printf("AVG Comparison : %4.6lf\n", (double)cumulative_cmp/(double)KEYS);
	puts("");
	for ( seq = 0; seq < 100; seq++ ) printf("=");
	puts("");
	getchar(); 
	printf(">> Search On M-Way Tree\n");
	cumulative_cmp =0;
	for ( seq = 0; seq < KEYS; seq++ )
	{
		printf("Searching... %d ", keys[seq]);
		int found = Search_M(M_T.root, keys[seq]);
		if ( found ) printf("FOUND. ");
		else printf("NOT FOUND. ");
		printf("%d comparison\n", CMP);
		cumulative_cmp += CMP; CMP = 0;
	}	
	printf("AVG Comparison : %4.6lf\n", (double)cumulative_cmp/(double)KEYS);
	puts("");
	for ( seq = 0; seq < 100; seq++ ) printf("=");
	puts("");
	getchar();
	cumulative_cmp =0;
	inOrderM(M_T.root);
	printf(">> Removing M-Node\n");
	for ( seq = 0; seq < COUNT; seq++ )
	{
		printf("\nSearching... %d ", F[seq]);
		M_T.root = testing(&(M_T.root), F[seq]);
		puts("\\\\");
		inOrderM(M_T.root);
		getchar();
	}	
	getchar();
	inOrderM(M_T.root);
	return 0;
}

int insert_array(int array[COUNT], int value)
{
	//printf("\n%d\n", value);
	int seq = 0, temp;
	CMP++;
	if ( array[seq] == 0 )
	{
		array[seq] = value;
		return seq;
	}
	CMP++;
	if ( value > array[seq] )
	{
		do{
			seq++;
			CMP++;
			if ( array[seq] == 0 )
			{
				array[seq] = value;
				return seq;
			}
			CMP++;
		} while ( value >= array[seq] );
	}
	int index = seq;
	temp = array[seq];
	array[seq] = value; seq++;
	do {
		// printf("%d %d\n", temp, value);
		value = array[seq];
		array[seq] = temp;
		temp = value;
		seq++;
		MOV++; CMP++;
	} while ( array[seq] != 0 );
	CMP++;
	if ( seq < COUNT ) {
		MOV++; array[seq] = temp;	
	}
	return index;
}

int buildArray(char *path, int bin[], const int count)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "r") ) == NULL )
	{
		printf("IO EXCEPTION\n");
		return 0;
	}
	int seq, value, index, cumulative_cmp = 0, cumulative_mov = 0;
	for ( seq = 0; seq < count; seq++ )
	{
		fscanf(fptr, "%d", &value);	// printf("%d\n", value);
		index = insert_array(bin, value); // printf("Index : %d\n", index ); printf("CMP : %d\n", CMP); printf("MOV : %d\n", MOV);
		cumulative_cmp += CMP;
		cumulative_mov += MOV;
		CMP = MOV = 0;
	}
	printf("Cumulative CMP : %d, avg : %4.6lf\n", cumulative_cmp, (double)cumulative_cmp/(double)count);
	printf("Cumulative MOV : %d, avg : %4.6lf\n", cumulative_mov, (double)cumulative_mov/(double)count);
	fclose(fptr);
	return 1;
}

void recursive_insertB(B_Node **root, int value)
{
	CMP++;
	if ( *root == NULL )
	{	MOV++;
		*root = createB(value); // printf("inserted\n");
		return;
	}	CMP++;
	if ( (*root)->value > value )
	{	// printf("left ");	
		recursive_insertB(&((*root)->left), value);
	}
	else
	{	// printf("right ");
		recursive_insertB(&((*root)->right), value);
	}
}

int build_Tree(char *path, BTree *tree, const int count)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "r") ) == NULL )
	{
		printf("IO EXCEPTION\n");
		return 0;
	}
	int seq, value, cumulative_cmp = 0, cumulative_mov = 0;
	for ( seq = 0; seq < count; seq++ )
	{
		fscanf(fptr, "%d", &value); printf("%d\n", value); //printf("inserting... %d\n", value);
		recursive_insertB(&(tree->root), value);
		cumulative_cmp += CMP;
		cumulative_mov += MOV;
		CMP = MOV = 0;
	}
	printf("Cumulative CMP : %d, avg : %4.6lf\n", cumulative_cmp, (double)cumulative_cmp/(double)count);
	printf("Cumulative MOV : %d, avg : %4.6lf\n", cumulative_mov, (double)cumulative_mov/(double)count);
	fclose(fptr);
	return 1;
}

void recursive_insertM(M_Node **root, int value)
{
	CMP++;
	if ( *root == NULL )
	{	// printf("created\n");
		*root = createM(value);
		MOV++; 
		return;
	}
	CMP++;
	if ( (*root)->i == 1 )
	{	CMP++;
		if ( (*root)->values[0] > value )
		{
			(*root)->values[1] = (*root)->values[0];
			(*root)->values[0] = value; // printf("swaped\n");
			MOV += 2;
		}
		else
		{	// printf("inserted\n");
			(*root)->values[(*root)->i] = value;
			MOV++; 
		} 
		(*root)->i = 2;
		return;
	}	CMP++;
	if ( (*root)->values[0] > value )
	{	// printf("left ");
		recursive_insertM(&((*root)->branches[0]), value);
		return;
	} CMP++;
	if ( (*root)->values[1] <= value )
	{	// printf("right ");
		recursive_insertM(&((*root)->branches[2]), value);
		return;
	}
		// printf("mid ");
	recursive_insertM(&((*root)->branches[1]), value);
	return;
}

int build_MTree(char *path, MTree *tree, const int count)
{
	FILE *fptr;
	if ( ( fptr = fopen(path, "r") ) == NULL )
	{
		printf("IO EXCEPTION\n");
		return 0;
	}
	int seq, value, cumulative_cmp = 0, cumulative_mov = 0;
	for ( seq = 0; seq < count; seq++ )
	{
		fscanf(fptr, "%d", &value); printf("%d\n", value);
		recursive_insertM(&(tree->root), value);
		cumulative_cmp += CMP;
		cumulative_mov += MOV;
		CMP = MOV = 0;
	}
	printf("Cumulative CMP : %d, avg : %4.6lf\n", cumulative_cmp, (double)cumulative_cmp/(double)count);
	printf("Cumulative MOV : %d, avg : %4.6lf\n", cumulative_mov, (double)cumulative_mov/(double)count);
	fclose(fptr);
	return 1;
}

int recursive_BSA(int array[], int begin, int last, int target )
{
	CMP++;
	if ( begin >= last ) return 0;
	int mid = begin + ( last  - begin ) / 2;
	CMP++;
	if ( array[mid] > target )
		return recursive_BSA(array, begin, mid-1, target);
	CMP++;
	if ( array[mid] < target )
		return recursive_BSA(array, mid+1, last, target);
	return 1;
}

int Search_Array(int array[], int count, int target)
{
	return recursive_BSA(array, 0, count, target);
}

int Search_Tree(B_Node *root, int target)
{
	CMP++;
	if ( root == NULL ) return 0;
	CMP++;
	if ( root->value > target ) return Search_Tree(root->left, target);
	CMP++;
	if ( root->value < target ) return Search_Tree(root->right, target);
	return 1;
}

void inOrder(B_Node *root)
{
	if ( root == NULL ) return;
	inOrder(root->left);
	printf("%d ", root->value);
	inOrder(root->right);
}

void printArray(int bin[], const int count)
{
	int seq;
	for ( seq = 0; seq < count; seq++ )
	{
		printf("%5d ", bin[seq]);
		if ( ( seq + 1 ) % 10 == 0 )	puts("");
	}
}

void inOrderM(M_Node *root)
{
	if ( root == NULL ) return;
	if ( root->values[0] != 0 )
	{
		inOrderM(root->branches[0]);
		printf("%5d ", root->values[0]);
		if ( root->values[1] != 0 )
		{
			inOrderM(root->branches[1]);
			printf("%5d ", root->values[1]);
			inOrderM(root->branches[2]);
		}
	}
}

int Search_M(M_Node *root, int target)
{
	CMP++;
	if ( root == NULL )
	{
		return 0;
	}
	CMP += 2;
	if ( root->values[0] == target || root->values[1] == target )
	{
		return 1;
	}
	CMP++;
	if ( root->values[0] > target ) // left
	{	// printf("left ");
		return Search_M(root->branches[0], target);
	}
	CMP++;
	if ( root->values[1] > target ) // mid
	{	// printf("mid ");
		return Search_M(root->branches[1], target);
	}
	else // if ( root->values[1] < target ) // right
	{	// printf("right ");
		return Search_M(root->branches[2], target);
	}
	return 0;
}

int minValue(M_Node *root)
{
	M_Node *current = root;
	while ( current->branches[0] != NULL )
		current = current->branches[0]; 
	return current->values[0] ;
}

int maxValue(M_Node *root)
{
	M_Node *current = root;
	while ( current->branches[2] != NULL )
		current = current->branches[2];
	return (current->values[1] == 0 ) ? current->values[0] : current->values[1];
}

M_Node* testing(M_Node **root, int target)
{
	if ( *root == NULL )
	{	// printf("failed\n");
		return NULL;
	}
	if ( (*root)->values[0] == target )
	{
		// printf("found at a\n");
		// printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		if ( (*root)->branches[0] == NULL && (*root)->branches[1] == NULL && (*root)->branches[2] == NULL )
		{
			if ( (*root)->values[1] != 0 )
			{
				(*root)->values[0] = (*root)->values[1];
				(*root)->values[1] = 0;
				(*root)->i = 1;
				printf("Changed Node : %5d\n", (*root)->values[0]);
				printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
			}
			else
			{
				free(*root);
				return NULL;
			}
		}
		else if ( (*root)->branches[1] != NULL )
		{
			(*root)->values[0] = minValue((*root)->branches[1]);
			printf("Changed Node : %5d\n", (*root)->values[0]);
			(*root)->branches[1] = testing(&((*root)->branches[1]), (*root)->values[0]);
			printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		}
		else if ( (*root)->branches[0] != NULL )
		{
			(*root)->values[0] = maxValue((*root)->branches[0]);
			printf("Changed Node : %5d\n", (*root)->values[0]);
			(*root)->branches[0] = testing(&((*root)->branches[0]), (*root)->values[0]);
			printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		}
		else if ( (*root)->values[1] != 0 )
		{
			(*root)->values[0] = (*root)->values[1];
			printf("Changed Node : %5d\n", (*root)->values[0]);
			printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
			if ( (*root)->branches[2] != NULL )
			{
				(*root)->values[1] = minValue((*root)->branches[2]);
				printf("Another Changed Node : %5d\n", (*root)->values[1]);
				(*root)->branches[2] = testing(&((*root)->branches[2]), (*root)->values[1]);
				printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
				if ( (*root)->values[1] < (*root)->values[0] )
				{	// printf("swap\n");
					int temp = (*root)->values[0];
					(*root)->values[0] = (*root)->values[1];
					(*root)->values[1] = temp;
				}
			}
			else
			{
				(*root)->values[1] = 0;
				(*root)->i = 1;
				printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
			}
		} 
		return *root;
	}
	else if ( (*root)->values[1] == target )
	{
		printf("found at b\n");
		printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		if ( (*root)->branches[0] == NULL && (*root)->branches[1] == NULL && (*root)->branches[2] == NULL )
		{
			printf("Leave node\n");
			if ( (*root)->values[0] != 0 )
			{
				(*root)->values[1] = 0;
				(*root)->i = 1;
				printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
			}
			else
			{
				free(*root);
				return NULL;
			}
		}
		else if ( (*root)->branches[1] != NULL )
		{
			(*root)->values[1] = maxValue((*root)->branches[1]);
			printf("Changed Node : %5d\n", (*root)->values[1]);
			(*root)->branches[1] = testing(&((*root)->branches[1]), (*root)->values[1]);
			printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		}
		else if ( (*root)->branches[2] != NULL )
		{
			(*root)->values[1] = minValue((*root)->branches[2]);
			printf("Changed Node : %5d\n", (*root)->values[1]);
			(*root)->branches[2] = testing(&((*root)->branches[2]), (*root)->values[1]);
			printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		}
		else if ( (*root)->values[0] != 0 )
		{
			if ( (*root)->branches[0] != NULL )
			{
				(*root)->values[1] = (*root)->values[0];
				printf("Changed Node : %5d\n", (*root)->values[1]);
				(*root)->values[0] = maxValue((*root)->branches[0]);
				(*root)->branches[0] = testing(&((*root)->branches[0]), (*root)->values[0]);
				printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
				if ( (*root)->values[1] < (*root)->values[0] )
				{
					printf("swap\n");
					int temp = (*root)->values[0];
					(*root)->values[0] = (*root)->values[1];
					(*root)->values[1] = temp;
				}
			}
			else
			{
				(*root)->values[1] = 0;
				(*root)->i = 1;
				printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
			}
		}
		else
		{
			(*root)->values[1] = 0;
			(*root)->i = 1;
			printf("a = %5d, b = %5d\n", (*root)->values[0], (*root)->values[1]);
		}
		return *root;
	}
	else if ( (*root)->values[0] > target ) // left
	{	// printf("left ");
		(*root)->branches[0] = testing(&((*root)->branches[0]), target);
	}
	else if ( (*root)->values[1] > target ) // mid
	{
		// printf("mid ");
		(*root)->branches[1] = testing(&((*root)->branches[1]), target);
	}
	else if ( (*root)->values[1] < target )
	{
		// printf("right ");
		(*root)->branches[2] = testing(&((*root)->branches[2]), target);
	}
	return *root;
}
