typedef struct BRANCH{
	double v; // consider to change to void *
	char type;
	struct BRANCH *parent, *lt, *rt;
}Branch;

typedef struct TREE{
	Branch *root;
}Tree;

typedef struct NODE{
	Branch *branch;
	struct NODE *link;
}Node;

typedef struct STACK{
	Node *sp;
	int count;
}Stack;

void initT(Tree *tree)
{
	tree->root = NULL;
}

void initS(Stack *stack)
{
	stack->count = 0;
	stack->sp = NULL;
}

Branch *createBranch(double v, char type)
{
	Branch *branch = (Branch*)malloc(sizeof(Branch));
	if ( branch != NULL )
	{
		branch->lt = NULL;
		branch->rt = NULL;
		branch->v = v;
		branch->type = type;
	}
	return branch;
}

Node *createNode(Branch *branch)
{
	Node *node = (Node*)malloc(sizeof(Node));
	if ( node != NULL )
	{
		node->branch = branch;
	}
	return node;
}

void push(Stack *stack, Node *node)
{
	node->link = stack->sp;
	stack->sp = node;
	stack->count += 1;
}

Branch *pop(Stack *stack)
{
	Node *node = stack->sp;
	Branch *branch = node->branch;
	stack->sp = stack->sp->link;
	stack->count -= 1;
	free(node);
	return branch;
}


