#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
// self define library
#include "STRUCTLIB.h"

void getExpressionTree(Tree *expressionTree, char expression[], int size)
{
	Stack stack; initS(&stack);
	int i; Branch *A, *B, *current; 
	for ( i = 0; i < size; i++ )
	{
		// printf(">> Current : %d\n", expression[i]);
		if ( expression[i] >= 48 && expression[i] <= 57 )
		{
			// puts("Operand");
			current = createBranch(expression[i]-'0','N');
			push(&stack, createNode(current));
		}
		else
		{
			// puts("Operator");
			current = createBranch(expression[i],'O');
			current->rt = pop(&stack); // B
			current->lt = pop(&stack); // A
			push(&stack, createNode(current));
		}
		// printf(">> Stack Count : %d\n", stack.count);
	}
	expressionTree->root = pop(&stack);
}

void diver(Branch **branch)
{
	printf(">> %\lfn", (*branch)->v);
	if ( (*branch)->type == 'N' ) return;
	printf("\n>> Captured : %\lf\n", (*branch)->v);
	diver(&(*branch)->lt);
	Branch *A = (*branch)->lt;
	Branch *B = (*branch)->rt;
	printf("\nA %lf B %lf\n", A->v, B->v);
	if ( (*branch)->v == 43 ) A->v += B->v;
	if ( (*branch)->v == 45 ) A->v -= B->v;
	if ( (*branch)->v == 42 ) A->v *= B->v;
	if ( (*branch)->v == 47 )
	{
		if ( B->v == 0 )
		{
			printf(">> Exception : Attemp to divide by zero!!!\n");
			return;
		}
		A->v /= B->v;
	}
	(*branch)->type = 'N';
	(*branch)->v = A->v;
	printf("Outcome %lf\n", A->v);
	(*branch)->lt = NULL;
	(*branch)->rt = NULL;
	free(A);
	free(B);
}
double solveExpressionTree(Tree *expressionTree)
{
	printf("\n>> Solving\n");
	diver(&expressionTree->root);
	return expressionTree->root->v;
}
void inOrder(Branch *root)
{
	if ( root == NULL ) return;
	inOrder(root->lt);
	if ( root->type == 'O')
		printf("%c ", (char)root->v );
	else
		printf("%lf ", root->v );
	inOrder(root->rt);
}

void postfix(Branch *root)
{
	if ( root == NULL ) return;
	postfix(root->lt);
	postfix(root->rt);
	if ( root->type == 'O')
		printf("%c ", (char)root->v );
	else
		printf("%lf ", root->v );
}

void prefix(Branch *root)
{
	if ( root == NULL ) return;
	if ( root->type == 'O')
		printf("%c ", (char)root->v );
	else
		printf("%lf ", root->v );
	prefix(root->lt);
	prefix(root->rt);
}
int main(void)
{
	char *expression = "12+3*4-";
	int len = strlen(expression);
	Tree tree; initT(&tree);
	getExpressionTree(&tree, expression, len);
	printf("\nInfix\n");
	inOrder(tree.root);
	printf("\nPostfix\n");
	postfix(tree.root);
	printf("\nPrefix\n");
	prefix(tree.root);
	
	double outcome = solveExpressionTree(&tree);
	printf(">>>> Outcome : %lf\n", outcome);
	return 0;
}
