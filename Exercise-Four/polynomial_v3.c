#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct listNode {
	void *coef;
	void *exp;
	struct listNode *link;
};
typedef struct listNode ListNode;
typedef ListNode *NodePtr;
struct listHead {
	int count;
	NodePtr top;
};
typedef struct listHead ListHead;
typedef ListHead *HeadPtr;

struct resultHead {
	int count;
	NodePtr top;
	NodePtr tail;
};
typedef struct resultHead ResultHead;
typedef ResultHead *ResultPtr;

// function prototype

// linked list operation
int insertASCByCoef( HeadPtr *stack, void *coef, void *exp );
int insertDSCByExp( HeadPtr *stack, void *coef, void *exp );
void directPush(ResultPtr *result, void *coef, void *exp );
void clearList( NodePtr *list, ResultPtr *result, int mode );
// utility
HeadPtr stackCreate( void );
ResultPtr resultCreate( void );
HeadPtr release( HeadPtr *stack );
ResultPtr releaseResult( ResultPtr *stack );
void traverse( HeadPtr *stack );
void traverseResult( ResultPtr *stack );
// polynomial operation 
void sum( HeadPtr *s1, HeadPtr *s2, ResultPtr *result );
void sub( HeadPtr *s1, HeadPtr *s2, ResultPtr *result );
void mul( HeadPtr *s1, HeadPtr *s2, ResultPtr *result );
void quo( HeadPtr *s1, HeadPtr *s2, ResultPtr *result );



int main( void )
{
	int choice;
	HeadPtr p1;
	HeadPtr p2;
	ResultPtr result;
	
	char list[50];
	int *coef, *exp;
	int output;
	scanf( "%d", &choice );
	while ( choice != 0 )
	{
		switch( choice )
		{
			case 1:
				p1 = stackCreate();
				break;
			case 2:
				p2 = stackCreate();
				break;
			case 3:
				puts("insertASCByCoef");
				coef = (int *)malloc( sizeof( int ) );
				exp = ( int *)malloc( sizeof( int ) );
				scanf("%d", coef);
				scanf("%d", exp);
				// output = insertASCByCoef( &p1, coef, exp );
				output = insertDSCByExp(&p1, coef, exp );
				printf("output : %d\n", output);
				traverse(&p1);
				break;
			case 4:
				puts("insertDSC");
				coef = ( int *)malloc( sizeof( int ) );
				exp = ( int *)malloc( sizeof( int ) );
				scanf( "%d", coef );
				scanf( "%d", exp );
				output = insertDSCByExp(&p2, coef, exp );
				printf("output : %d\n", output);
				traverse( &p2 );
				break;
			case 5:
				puts("add");
				result = resultCreate();
				sum(&p1, &p2,&result);
				traverseResult(&result);
				releaseResult(&result);
				break;
			case 6:
				puts("sub");
				result = resultCreate();
				sub( &p1, &p2, &result);
				traverseResult( &result );
				releaseResult(&result);
				break;
			case 7:
				puts("mul");
				result = resultCreate();
				mul(&p1,&p2,&result);
				traverseResult( &result );
				releaseResult(&result);
				break;
			case 8:
				puts("div");
				break;
			default:
				break;
		}
		scanf( "%d", &choice );
	}
	if ( p1 != NULL )
	{
		puts("releasing p1");
		release(&p1);
	}
	if ( p2 != NULL )
	{
		puts("releasing p2");
		release(&p2);
	}
	getchar();
	return 0;
}

HeadPtr stackCreate( void )
{
	HeadPtr stack; // this is already a pointer type, do not add * in between type and variable
	stack = (HeadPtr)malloc( sizeof( ListHead ) );
	if ( stack != NULL )
	{
		stack->count = 0;
		stack->top = NULL;
	}
	return stack;
}

ResultPtr resultCreate( void )
{
	ResultPtr stack; // this is already a pointer type, do not add * in between type and variable
	stack = (ResultPtr)malloc( sizeof( ResultHead ) );
	if ( stack != NULL )
	{
		stack->count = 0;
		stack->top = NULL;
		stack->tail = NULL;
	}
	return stack;
}

int insertASCByCoef( HeadPtr *stack, void *coef, void *exp )
{
	NodePtr newNode;
	NodePtr previousNode = NULL;
	NodePtr currentNode;
	currentNode = (*stack)->top;
	int v;
	
	newNode = ( NodePtr )malloc( sizeof( ListNode)  );
	if ( newNode )
	{
		newNode->coef = coef;
		newNode->exp = exp;
		newNode->link = NULL;
		int c = *(int *)newNode->coef;
		while ( currentNode != NULL )
		{
			v = *(int *)currentNode->coef;
			// merge
			if ( v == c )
			{
				*(int *)currentNode->exp += *(int *)newNode->exp;
				free( newNode );
				(*stack)->count += 1;
				return (*stack)->count;
			}
			else if ( v > c)
			{
				break;
			}
			previousNode = currentNode;
			currentNode = currentNode->link;
		}
		if ( previousNode == NULL )
		{
			newNode->link = (*stack)->top;
			(*stack)->top = newNode;
		}
		else
		{
			previousNode->link = newNode;
			newNode->link = currentNode;
		}
		(*stack)->count += 1;
		return (*stack)->count;
	}
	else
		puts("insert : failed due to lack of memory access");
	return 0;
}


int insertDSCByExp( HeadPtr *stack, void *coef, void *exp )
{
	NodePtr newNode;
	NodePtr previousNode = NULL;
	NodePtr currentNode;
	currentNode = (*stack)->top;
	int v;
	
	newNode = ( NodePtr )malloc( sizeof( ListNode)  );
	if ( newNode )
	{
		newNode->coef = coef;
		newNode->exp = exp;
		newNode->link = NULL;
		int e = *(int *)newNode->exp;
		while ( currentNode != NULL )
		{
			v = *(int *)currentNode->exp; 
			// merge
			if ( v == e )
			{
				*(int *)currentNode->coef += *(int *)newNode->coef;
				free(newNode);
				(*stack)->count += 1;
				return (*stack)->count;
			}
			else if ( v < e)
			{
				break;
			}
			previousNode = currentNode;
			currentNode = currentNode->link;
		}
		if ( previousNode == NULL )
		{
			newNode->link = (*stack)->top;
			(*stack)->top = newNode;
		}
		else
		{
			previousNode->link = newNode;
			newNode->link = currentNode;
		}
		(*stack)->count += 1;
		return (*stack)->count;
	}
	else
		puts("insert : failed due to lack of memory access");
	return 0;
}

int orderedPush( ResultPtr *stack, void *coef, void *exp )
{
	// puts("OrderedPush");
	NodePtr newNode;
	NodePtr previousNode = NULL;
	NodePtr currentNode;
	currentNode = (*stack)->top;
	int v;
	
	newNode = ( NodePtr )malloc( sizeof( ListNode)  );
	if ( newNode )
	{
		// puts("A");
		newNode->coef = coef;
		newNode->exp = exp;
		newNode->link = NULL;
		// puts("AA");
		if ( (*stack)->tail != NULL )
		{
			if ( *(int *)exp > *(int *)((*stack)->tail->exp ))
			{
				(*stack)->tail->link = newNode;
				(*stack)->tail = newNode;
				(*stack)->count += 1;
				return (*stack)->count;
			}
		}
		int e = *(int *)newNode->exp;
		while ( currentNode != NULL )
		{
			v = *(int *)currentNode->exp; 
			// merge
			if ( v == e )
			{
				*(int *)currentNode->coef += *(int *)newNode->coef;
				free(newNode);
				(*stack)->count += 1;
				return (*stack)->count;
			}
			else if ( v < e)
			{
				break;
			}
			previousNode = currentNode;
			currentNode = currentNode->link;
		}
		if ( previousNode == NULL )
		{
			newNode->link = (*stack)->top;
			(*stack)->top = newNode;
			(*stack)->tail = newNode;
		}
		else
		{
			previousNode->link = newNode;
			newNode->link = currentNode;
			if ( currentNode == NULL )
				(*stack)->tail = newNode;
		}
		(*stack)->count += 1;
		return (*stack)->count;
	}
	else
		puts("insert : failed due to lack of memory access");
	return 0;
}


HeadPtr release( HeadPtr *stack )
{
	NodePtr currentNode = (*stack)->top;
	while ( (*stack)->top != NULL )
	{
		free( (*stack)->top->coef );
		free( (*stack)->top->exp );
		currentNode = (*stack)->top;
		(*stack)->top = (*stack)->top->link;
		free( currentNode );
	}
	free( *stack );
	return NULL;
}

ResultPtr releaseResult( ResultPtr *stack )
{
	NodePtr currentNode = (*stack)->top;
	while ( (*stack)->top != NULL )
	{
		free( (*stack)->top->coef );
		free( (*stack)->top->exp );
		currentNode = (*stack)->top;
		(*stack)->top = (*stack)->top->link;
		free( currentNode );
	}
	free( *stack );
	return NULL;
}

void traverse( HeadPtr *stack )
{
	NodePtr currentNode = (*stack)->top;
	while ( currentNode != NULL )
	{
		printf("%d ^ %d ", *(int *)currentNode->coef, *(int *)currentNode->exp);
		currentNode = currentNode->link;
	}
	puts("EOQ");
}

void traverseResult( ResultPtr *stack )
{
	NodePtr currentNode = (*stack)->top;
	while ( currentNode != NULL )
	{
		printf("%d ^ %d ", *(int *)currentNode->coef, *(int *)currentNode->exp);
		currentNode = currentNode->link;
	}
	puts("EOQ");
}

void directPush(ResultPtr *result, void *coef, void *exp )
{
	// puts("push");
	NodePtr newNode = (NodePtr)malloc( sizeof( ListNode ) );
	// puts("A");
	newNode->coef = coef;
	newNode->exp = exp;
	newNode->link = NULL;
	// printf("count : %d\n", (*result)->count);
	if ( (*result)->count == 0 )
	{
		// puts("AB");
		(*result)->top = newNode;
		(*result)->tail = newNode;
	}
	else
	{
		// puts("AC");
		(*result)->tail->link = newNode;
		(*result)->tail = newNode;
	}
	// puts("AD");
	((*result)->count)++;
	// traverseResult(result);
}

void clearList( NodePtr *list, ResultPtr *result, int mode )
{
	NodePtr currentNode = *list;
	int *coef, *exp;
	while ( currentNode != NULL )
	{
		coef = (int *)malloc(sizeof(int));
		exp = (int *)malloc(sizeof(int));
		*coef = *(int *)currentNode->coef;
		if ( mode )
			*coef *= -1;
		*exp = *(int *)currentNode->exp;
		directPush(result,coef,exp);
		currentNode = currentNode->link;		
	}
}

void sum( HeadPtr *s1, HeadPtr *s2, ResultPtr *result )
{
	NodePtr q1, q2;
	int *coef, *exp;
	q1 = (*s1)->top;
	q2 = (*s2)->top;
	while ( q1 != NULL || q2 != NULL )
	{
		coef = (int *)malloc(sizeof(int));
		exp = (int *)malloc(sizeof(int));
		if ( *(int *)q1->exp == *(int *)q2->exp )
		{
			*coef = *(int *)q1->coef + *(int *)q2->coef;
			*exp = *(int *)q1->exp;
			directPush(result,coef,exp);
			q1 = q1->link;
			q2 = q2->link;
		}
		else if ( *(int *)q1->exp > *(int *)q2->exp )
		{
			*coef = *(int *)q1->coef;
			*exp = *(int *)q1->exp;
			directPush(result,coef,exp);
			q1 = q1->link;
		}
		else if ( *(int *)q2->exp > *(int *)q1->exp )
		{
			*coef = *(int *)q2->coef;
			*exp = *(int *)q2->exp;
			directPush(result,coef,exp);
			q2 = q2->link;
		}
		
		if ( q1 != NULL && q2 == NULL )
		{
			clearList(&q1,result, 0);
			break;
		}
		else if ( q1 == NULL && q2 != NULL )
		{
			clearList(&q2,result, 0);
			break;
		}
	}
}

void sub( HeadPtr *s1, HeadPtr *s2, ResultPtr *result )
{
	NodePtr q1, q2;
	int *coef, *exp;
	q1 = (*s1)->top;
	q2 = (*s2)->top;
	
	while ( q1 != NULL || q2 != NULL )
	{
		coef = (int *)malloc(sizeof(int));
		exp = (int *)malloc(sizeof(int));
		if ( *(int *)q1->exp == *(int *)q2->exp )
		{
			*coef = *(int *)q1->coef - *(int *)q2->coef;
			*exp = *(int *)q1->exp;
			directPush(result,coef,exp);
			q1 = q1->link;
			q2 = q2->link;
		}
		else if ( *(int *)q1->exp > *(int *)q2->exp )
		{
			*coef = *(int *)q1->coef;
			*exp = *(int *)q1->exp;
			directPush(result,coef,exp);
			q1 = q1->link;
		}
		else if ( *(int *)q2->exp > *(int *)q1->exp )
		{
			*coef = *(int *)q2->coef * -1;
			*exp = *(int *)q2->exp;
			directPush(result,coef,exp);
			q2 = q2->link;
		}
		
		if ( q1 != NULL && q2 == NULL )
		{
			clearList(&q1,result, 0); // set to Mode zero because no neg sign required 
			break;
		}
		else if ( q1 == NULL && q2 != NULL )
		{
			clearList(&q2,result, 1);
			break;
		}
	}
}

void mul( HeadPtr *s1, HeadPtr *s2, ResultPtr *result )
{
	NodePtr q1, q2;
	int *coef, *exp;
	q1 = (*s1)->top;
	q2 = (*s2)->top;
	while ( q1 != NULL || q2 != NULL )
	{
		coef = (int *)malloc(sizeof(int));
		exp = (int *)malloc(sizeof(int));
		*coef = *(int *)q1->coef * *(int *)q2->coef;
		*exp = *(int *)q1->exp + *(int *)q2->exp;
		orderedPush(result,coef, exp);
		q2 = q2->link;
		if( q1 != NULL && q2 == NULL )
		{
			q1 = q1->link;
			q2 = (*s2)->top;
		}
		if ( q1 == NULL )
		{
			puts("end of cycle");
			break;
		}
	}
}
