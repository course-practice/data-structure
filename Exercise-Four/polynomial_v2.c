#include <stdio.h>
#include <stdlib.h>

struct listNode {
	int coef;
	int ex;
	struct listNode *NextPtr;
};

typedef struct listNode ListNode;
typedef ListNode *ListNodePtr;

// function prototype 

void insert( ListNodePtr *sPtr, int c, int e )
{
	ListNodePtr currentPtr = NULL, previousPtr = NULL;
	ListNodePtr newPtr = malloc(sizeof(ListNode));
	if( newPtr != NULL )
	{
		newPtr->coef = c;
		newPtr->ex = e;
		newPtr->NextPtr = NULL;
		currentPtr = *sPtr;
		while( currentPtr != NULL && e <= currentPtr->ex ){
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
		}
		
		if( previousPtr == NULL ){
			newPtr->NextPtr = *sPtr;
			*sPtr = newPtr;
		}
		else if( e == previousPtr->ex )
		{
			previousPtr->coef += c;
			free(newPtr);
		}
		else{
			previousPtr->NextPtr = newPtr;
			newPtr->NextPtr = currentPtr;
		}
	}
	else{
		printf("%s\n", "No memory available");
	}	
}

void show( ListNodePtr currentPtr, int op ){
	if( currentPtr == NULL ) {
		puts("Empty list");
	}
	else {
		puts("Printing list\n");
		while( currentPtr != NULL ){
			printf("%d^", currentPtr->coef);
			printf("%d ", currentPtr->ex);
			currentPtr = currentPtr->NextPtr;
			if( currentPtr != NULL )
			{
				if( currentPtr->coef > 0 )
				{
					switch(op)
					{
						case 1:
						case 2:
							printf("+");
							break;
						case 3:
							printf("*");
							break;
						case 4:
							printf("/");
						default:
							break;
					}
				}
					
			}
		}
		puts("");
	}
}

void basicOP(ListNodePtr *eq1, ListNodePtr *eq2, ListNodePtr *result, int mode )
{
	ListNodePtr currentPtr1 = NULL;
	ListNodePtr currentPtr2 = NULL;
	currentPtr1 = *eq1;
	currentPtr2 = *eq2;
	int coef, exp;
	int loop = 0;
	int flag = 0; 
	while( 1 )
	{
		/*
		if( currentPtr1->ex == currentPtr2->ex )
		{
			// add
			currentPtr2->NextPtr = currentPtr2->NextPtr;
		}
		else if( currentPtr1->ex > currentPtr->ex )
		{
			// insert
			currentPtr2->NextPtr = currentPtr2->NextPtr;
		}
		if( currentPtr2 == NULL )
		{
			break;
		}
		else if( currentPtr2 != NULL && currentPtr1 == NULL )
		{
			// load the rest to currentPtr1
			while( currentPtr2 != NULL )
			{
				// insert
			}
		}
		*/
		if( currentPtr1->ex == currentPtr2->ex )
		{
			exp = currentPtr1->ex;
			if( mode )
				coef = currentPtr1->coef + currentPtr2->coef;
			else
				coef = currentPtr1->coef - currentPtr2->coef;
			insert( result,coef, exp );
			currentPtr2 = currentPtr2->NextPtr;
			flag = 1;
			currentPtr1 = currentPtr1->NextPtr;
		}
		else if( currentPtr1->ex < currentPtr2->ex )
		{
			exp = currentPtr2->ex;
			coef = currentPtr2->coef;
			if ( !mode )
				coef *= -1;
			insert( result, coef, exp );
			currentPtr2 = currentPtr2->NextPtr;
			flag = 2;
		}
		if( flag == 2 || flag == 0 )
		{
			exp = currentPtr1->ex;
			coef = currentPtr1->coef;
			insert( result, coef, exp );
			currentPtr1 = currentPtr1->NextPtr;
		}
		flag = 0;
		if( currentPtr2 == NULL )
			break;
		else if( currentPtr2 != NULL && currentPtr1 == NULL )
		{
			while( currentPtr2 != NULL )
			{
				exp = currentPtr2->ex;
				coef = currentPtr2->coef;
				if ( !mode )
					coef *= -1;
				insert( result, coef, exp );
				currentPtr2 = currentPtr2->NextPtr;
			}
			break;
		}
	}	
}

void advanceOp(ListNodePtr *eq1, ListNodePtr *eq2, ListNodePtr *result, int mode )
{
	ListNodePtr currentPtr1 = NULL;
	ListNodePtr currentPtr2 = NULL;
	currentPtr1 = *eq1;
	currentPtr2 = *eq2;
	int coef, exp;
	int loop = 0;
	int flag = 0;
	while( 1 )
	{
		coef = currentPtr1->coef * currentPtr2->coef;
		exp = currentPtr1->ex + currentPtr2->ex;
		insert( result, coef, exp );
		currentPtr2 = currentPtr2->NextPtr;
		if( currentPtr1 != NULL && currentPtr2 == NULL )
		{
			currentPtr1 = currentPtr1->NextPtr;
			currentPtr2 = *eq2;
		}
		if ( currentPtr1 == NULL )
		{
			puts("end of cycle");
			break;
		}
	} 
}

void deleteList( ListNodePtr *start ) {
	ListNodePtr previousPtr;
	ListNodePtr currentPtr;
	ListNodePtr tempPtr;
	
	previousPtr = *start;
	currentPtr = previousPtr->NextPtr;
	while( currentPtr != NULL ){
		tempPtr = currentPtr;
		previousPtr->NextPtr = currentPtr->NextPtr;
		currentPtr = currentPtr->NextPtr;
		free( tempPtr );
	}
	if( previousPtr != NULL )
	{
		tempPtr = previousPtr;
		*start = NULL;
		free(tempPtr);
	}
}	

void insertByCoef(ListNodePtr *list, ListNodePtr *newPtr )
{
	printf(" sorting coef %d exp %d\n", (*newPtr)->coef, (*newPtr)->ex);
	ListNodePtr previousPtr = NULL;
	ListNodePtr currentPtr = *list;
	while( currentPtr != NULL && (*newPtr)->coef > currentPtr->coef ){
		previousPtr = currentPtr;
		currentPtr = currentPtr->NextPtr;
	}
	if( previousPtr == NULL ){
		(*newPtr)->NextPtr = *list;
		*list = *newPtr;
	}
	else{
		previousPtr->NextPtr = *newPtr;
		(*newPtr)->NextPtr = currentPtr;
	}
}

void insertByExp(ListNodePtr *list, ListNodePtr *newPtr )
{
	printf(" sorting coef %d exp %d\n", (*newPtr)->coef, (*newPtr)->ex);
	ListNodePtr previousPtr = NULL;
	ListNodePtr currentPtr = *list;
	while( currentPtr != NULL && (*newPtr)->ex < currentPtr->ex ){
		previousPtr = currentPtr;
		currentPtr = currentPtr->NextPtr;
	}
	if( previousPtr == NULL ){
		(*newPtr)->NextPtr = *list;
		*list = *newPtr;
	}
	else{
		previousPtr->NextPtr = *newPtr;
		(*newPtr)->NextPtr = currentPtr;
	}
}

void sort( ListNodePtr *eq1, int mode ){
	ListNodePtr previousPtr = *eq1;
	ListNodePtr currentPtr = *eq1;
	ListNodePtr newList = NULL;
	ListNodePtr tempPtr = NULL;
	while( currentPtr != NULL ){
		tempPtr = currentPtr;
		currentPtr = currentPtr->NextPtr;
		tempPtr->NextPtr = NULL;
		if ( mode == 1 )
			insertByCoef(&newList, &tempPtr);
		else
			insertByExp(&newList, &tempPtr);
	}
	*eq1 = newList;
}
int main( void )
{
	ListNodePtr eq1 = NULL; 
	ListNodePtr eq2 = NULL;
	ListNodePtr result = NULL;
	int choice, c, e;
	choice = c = e = 0;
	while( choice != -1 )
	{
		scanf("%d", &choice);
		switch(choice){
			case 1:
				puts("insert 1");
				scanf("%d%d", &c, &e);
				insert(&eq1, c, e);
				show(eq1, 1);
				break;
			case 2:
				puts("insert 2");
				scanf("%d%d", &c, &e);
				insert(&eq2, c, e);	
				show(eq2, 1);
				break;
			case 3:
				puts("add");
				if( eq1 != NULL && eq2 != NULL )
				{
					if( result != NULL )
						deleteList(&result);
					sort(&eq1,0);
					show(eq1, 1);
					sort(&eq2,0);
					show(eq2, 1);
					basicOP(&eq1, &eq2, &result, 1);
					show(result, 1);	
				}
				else
					puts("either list is empty");
				break;
			case 4:
				puts("sub");
				if( eq1 != NULL && eq2 != NULL )
				{
					if( result != NULL )
						deleteList(&result);
					sort(&eq1,0);
					sort(&eq2,0);
					basicOP(&eq1, &eq2, &result, 0);
					show(result, 2);	
				}
				else
					puts("either list is empty");
				break;
			case 5:
				puts("mul");
				if( eq1 != NULL && eq2 != NULL )
				{
					if( result != NULL )
						deleteList(&result);
					sort(&eq1,1);
					puts("sorted 1");
					show( eq1, 1 );
					sort(&eq2,1);
					puts("sorted 2");
					show( eq2, 1 );
					advanceOp(&eq1, &eq2, &result, 1);
					show(result, 1);
				}
				else
					puts("either list is empty");
				break;
			default:
				break;
		}
	}
	puts("ended");
	getchar();
	return 0;
}
