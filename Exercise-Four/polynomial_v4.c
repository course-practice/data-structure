#include <stdio.h>
#include <stdlib.h>

struct listNode{
	int coef, exp;
	struct listNode *link;
};
typedef struct listNode ListNode;
typedef ListNode *Node;
struct stackNode{
	int count;
	Node head, tail;
};
typedef struct stackNode StackNode;
typedef StackNode *Stack;

// function prototype
// high level function
void readInput(Stack *stack);
int insert_coef_asc(Stack *stack, int coef, int exp);
int insert_exp_dsc(Stack *stack, int coef, int exp);
// arithemic operation
void add(Stack *Eq1, Stack *Eq2, Stack *Result);
void sub(Stack *Eq1, Stack *Eq2, Stack *Result);
void mul(Stack *Eq1, Stack *Eq2, Stack *Result);
// 
Stack createStack();
void killStack(Stack *stack);
void traverse(Stack *stack);

int main(void)
{
	int i, choice = 0, outcome, coef, exp;
	Stack eq1, eq2, result;
	scanf("%d", &choice);
	while ( choice != -1 )
	{
		switch(choice)
		{
			case 1:
				eq1 = createStack();
				eq2 = createStack();
				if ( eq1 == NULL || eq2 == NULL )
				{
					printf("System out of memory\n\n");
					return 0;
				}
				break;
			case 2:
			case 3:
				readInput((choice==2)?&eq1:&eq2);
				// traverse((choice==2)?eq1:eq2);
				break;
			case 4:
				puts("====== ADD ======");
				result = createStack();
				add(&eq1, &eq2, &result);
				traverse(&result); // need to traverse from the tail
				killStack(&result);
				break;
			case 5:
				puts("====== SUB ======");
				result = createStack();
				sub(&eq1, &eq2, &result);
				traverse(&result); // need to traverse from the tail
				killStack(&result);
				break;
			case 6:
				puts("====== MUL ======");
				result = createStack();
				mul(&eq1, &eq2, &result);
				traverse(&result); // need to traverse from the tail
				killStack(&result);
				break;
			case 7:
				puts("====== Release Stack ======");
				killStack(&eq1);
				killStack(&eq2);
				printf("Program terminating\n\n");
				return 0;
				break;
			default:
				break;
		}
		scanf("%d", &choice);
	}
	killStack(&eq1);
	killStack(&eq2);
	getchar();
	return 0;
}
void readInput(Stack *stack)
{
	int terminate = 0;
	int coef, exp, outcome;
	while ( terminate != -1 )
	{
		scanf("%d%d", &coef, &exp);
		outcome = insert_exp_dsc(stack, coef, exp);
		printf("Outcome : %d\n", outcome);
		scanf("%d", &terminate);
	}
	traverse(stack);
}
Stack createStack()
{
	Stack stack = (Stack)malloc(sizeof(StackNode));
	if ( stack != NULL )
	{
		stack->count = 0;
		stack->head = NULL;
		stack->tail = NULL;
	}
	printf("Stack : %p\n", stack);
	return stack;
}
void killStack(Stack *stack)
{
	Node currentNode;
	while ( (*stack)->head != NULL )
	{
		currentNode = (*stack)->head;
		(*stack)->head = (*stack)->head->link;
		free( currentNode );
	}
	free( *stack );
	return NULL;
}

void traverse(Stack *stack)
{	puts("");
	Node current = (*stack)->head;
	while ( current != NULL )
	{
		printf("%2d ^ %2d", current->coef, current->exp);
		current = current->link;
	}
	printf(" || End of Equation\n\n");
}

int insert_exp_dsc(Stack *stack, int coef, int exp)
{
	Node newNode = (Node)malloc(sizeof(ListNode));
	if (newNode != NULL)
	{
		newNode->coef = coef;
		newNode->exp = exp;
		Node prev = NULL, current = (*stack)->head;
		while ( current != NULL && current->exp >= exp )
		{
			if ( current->exp == exp )
			{
				current->coef += coef;
				free(newNode);
				return 1;
			}
			prev = current;
			current = current->link;
		}
		if ( prev == NULL )
		{
			newNode->link = (*stack)->head;
			(*stack)->head = newNode;
			return 2;
		}
		prev->link = newNode;
		newNode->link = current;
		return 3;
	}
	return 0;
}
void push(Stack *stack, int coef, int exp)
{ 	puts("push");
	if ( coef != 0 )
	{
		Node newNode = (Node)malloc(sizeof(ListNode));
		if (newNode != NULL)
		{
			newNode->coef = coef;
			newNode->exp = exp;
			newNode->link = NULL;
			if ( (*stack)->head == NULL )
				(*stack)->head = newNode;
			if ( (*stack)->tail != NULL )
				(*stack)->tail->link = newNode;
			(*stack)->tail = newNode;
			return;
		}
		puts("Memory not available");
	}
}
void add(Stack *Eq1, Stack *Eq2, Stack *Result)
{
	int coef, exp;
	Node headOne = (*Eq1)->head, headTwo = (*Eq2)->head;
	while ( headOne != NULL && headTwo != NULL )
	{
		printf("%d %d\n", headOne->exp, headTwo->exp);
		if ( headOne->exp == headTwo->exp )
		{
			push(Result, headOne->coef + headTwo->coef, headOne->exp);
			headOne = headOne->link;
			headTwo = headTwo->link;
			continue;
		}
		if ( headOne->exp > headTwo->exp )
		{
			push(Result, headOne->coef, headOne->exp);
			headOne = headOne->link;
			continue;
		}
		
		push(Result, headTwo->coef, headTwo->exp);
		headTwo = headTwo->link;
	}
	while ( headOne != NULL )
	{
		push(Result, headOne->coef, headOne->exp);
		headOne = headOne->link;
	}
	while ( headTwo != NULL )
	{
		push(Result, headTwo->coef, headTwo->exp);
		headTwo = headTwo->link;
	}
}
void sub(Stack *Eq1, Stack *Eq2, Stack *Result)
{
	int coef, exp;
	Node headOne = (*Eq1)->head, headTwo = (*Eq2)->head;
	
	while ( headOne != NULL && headTwo != NULL )
	{
		printf("%d %d\n", headOne->exp, headTwo->exp);
		if ( headOne->exp == headTwo->exp )
		{
			push(Result, headOne->coef - headTwo->coef, headOne->exp);
			headOne = headOne->link;
			headTwo = headTwo->link;
			continue;
		}
		if ( headOne->exp > headTwo->exp )
		{
			push(Result, headOne->coeff, headOne->exp);
			headOne = headOne->link;
			continue;
		}
		if ( headTwo->exp > headOne->exp )
		{
			push(Result, -1 * (headTwo->coef)f, headTwo->exp);
			headTwo = headTwo->link;
			continue;
		}
	}
	while ( headOne != NULL )
	{
		push(Result, headOne->coef, headOne->exp);
		headOne = headOne->link;
	}
	while ( headTwo != NULL )
	{
		push(Result, -1 * (headTwo->coef), headTwo->exp);
		headTwo = headTwo->link;
	}	
}
void mul(Stack *Eq1, Stack *Eq2, Stack *Result)
{
	Node headOne = (*Eq1)->head, headTwo = (*Eq2)->head;
	int coef, exp;
	while ( headOne != NULL || headTwo != NULL )
	{
		insert_exp_dsc(Result, headOne->coef * headTwo->coef, headOne->exp + headTwo->exp);
		headTwo = headTwo->link;
		if ( headTwo == NULL )
		{
			headOne = headOne->link;
			if ( headOne == NULL )
				break;
			headTwo = (*Eq2)->head;
		}
	}
}
