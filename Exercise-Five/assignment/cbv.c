#include <stdio.h>
#include <stdlib.h>

struct listNode 
{
	char value[7];
	struct listNode *link;
};
typedef struct listNode Node;

struct listTag
{
	int count;
	Node *head;
	Node *tail;
};
typedef struct listTag Tag;

// global variable
Tag container[50];

// function prototype
int generatetxt(char *path);
int readData(char set[][7], char *path, int size);
void printSimpleArray(char arr[], int size);
void printStructure(Tag tag);
int hashing(char key[7], int bound);
int simple_search(Tag *tag, char value[7]);
int insert_asc( Tag *tag, char value[7] );
int rangeControl_search(Tag *tag, char value[7]);
void removeNode(Tag *tag, char value[7]);

int main(void)
{
	srand(time(NULL));
	//char *path = "data.txt";
	char *left = "left.txt";
	char *right = "right.txt";
	//char *test = "test.txt";
	char data[70][7] = {0}, searchKey[46][7]={0};
	int i, j, size = sizeof(container)/sizeof(container[0]), key, searchOutcome, total = 0;
	int left_size = 46, right_size = 70;
	int counter = 0;
	int frequency[50] = {0};
	
	// read data from file
	readData(data,right, right_size);
	readData(searchKey, left, left_size);
	// initialize container
	for ( i = 0; i < size; i++ )
	{
		container[i].count = 0;
		container[i].head = NULL;
		container[i].tail = NULL;
	}
	// push
	puts("\n\n====== STUPID PUSH ======\n\n");
	for ( i = 0; i < right_size; i++ )
	{
		key = hashing( data[i], size );
		//push(&container[key], data[i]);
		insert_asc(&container[key], data[i]);
		frequency[key]++;
		printf("after push : %d\n", container[key].count);
	}
	puts("\n\n====== COMPUTE LOAD FACTOR ======\n\n");
	for ( i = 0; i < size; i++ )
	{
		if ( frequency[i] > 0 )
			counter++;
	}
	printf("\n\nLoad Factor : %lf\n", (double)counter/50.0);
	puts("\n\n====== PRINT STRUCTURE[] ======\n\n");
	for ( i = 0; i < size; i++ )
	{
		printStructure(container[i]);
	}
		puts("\n\n====== SIMPLE SEARCH ======\n\n");
	for ( i = 0; i < left_size; i++ )
	{
		key = hashing(searchKey[i], size);
		//searchOutcome = simple_search(&container[key], searchKey[i]);
		searchOutcome = rangeControl_search(&container[key], searchKey[i]);
		total += searchOutcome; searchOutcome = 0;
	}
	printf("Total effort to search : %d\n", total);
	printf("Average in %d round : %lf\n", left_size, (double)total/left_size);
	puts("Press ENTER to continue");
	getchar();
	puts("\n\n====== END ======\n\n");
	for ( i = 0; i < right_size; i++ )
	{
		key = hashing(data[i], 50);
		printf("removing : %s at %d\n\n", data[i], key);
		removeNode(&container[key], data[i]);
		for ( j = 0; j < size; j++ )
		{
			printStructure(container[j]);
		}
		push(&container[key], data[i]);
		getchar();
	}
	getchar();
	return 0;
}

void push(Tag *tag, char value[7])
{
	Node *node = (Node*)malloc(sizeof(Node));
	int i;
	if ( node != NULL )
	{
		for ( i = 0; i < 6; i++ )
			node->value[i] = value[i]; // a[0] - '0'
		node->link = tag->head; //
		tag->head = node; //
		tag->count += 1;
		printf("SIZE : %d || ", tag->count);
		printf("%d was pushed\n", atoi(value));
		return;
	}
	puts("Out Of Memory");
}

int hashing(char key[7], int bound)
{
	int integerKey = atoi(key);
	printf("HASHING : %d\n", integerKey);
	int a[6], i, power = 100000, hashkey = -1;
	int prime[6] = { 19, 31, 47, 59, 117, 271 };
	for ( i = 0; i < 6; i++ )
	{
		a[i] = integerKey / power;
		integerKey -= a[i] * power;
		power /= 10;
		hashkey += a[i] * prime[i];
	}
	return abs(hashkey)%bound;	
}

void printSimpleArray(char arr[], int size)
{
	int i;
	for ( i = 0; i < size; i++ )
	{
		printf( "%s %s", arr[i], ((i+1)%10==0)?"\n":" >> " );
	}
}
void printStructure(Tag tag)
{
	Node *node = tag.head;
	if (1) //  tag.count != 0 
	{
		printf("%d >> SIZE : %d || ", hashing((tag.head)->value, 50), tag.count);
		while ( node != NULL )
		{
			printf("%s >> ", (*node).value);
			node = (*node).link;
		}
		printf("||| EOL\n");
	}
}
int generatetxt(char *path)
{
	FILE *fptr;
	char buff[7] = { 0 };
	int i, j;
	if ( ( fptr = fopen(path, "w") ) == NULL )
		printf("\nfile io exception : file not found\n");
	else
	{
		for( i = 0; i < 70; i++ )
		{
			buff[0] = rand()%9 + '1';
			for ( j = 1; j < 6; j++ )
				buff[j] = rand()%10 + '0';
			if ( i == 69 )
				fprintf(fptr, "%s", buff);
			else
				fprintf(fptr, "%s\n", buff);
			printf("%s\n", buff);
		}
		fclose(fptr);
		return 1;
	}
	return 0;
}

int readData(char set[][7], char *path, int size)
{	
	FILE *fptr;
	char buff[7] = {0};
	int i = 0, j = 0, n = 0, k;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	puts("readData\n");
   	while ( !feof(fptr) && j < size )
   	{
   		// fscanf(fptr, "%s", buff);
   		for ( k = 0; k < 6; k++ )
	   		fscanf(fptr," %c", &buff[k]);
   		printf("%s\n", buff);
   		for ( i = 0; i < 6; i++ )
   			set[j][i] = buff[i];
   		j++; n++;
	}
   	fclose(fptr); 
   	return 1;
}

int simple_search(Tag *tag, char value[7])
{
	int counter = 1;
	int actual = atoi(value), cur;
	Node *current = (*tag).head;
	printf("Searching : %d ", actual);
	while ( current != NULL )
	{
		cur = atoi(current->value);
		if ( cur == actual )
		{
			printf("FOUND\n");
			return counter;
		}
		counter++;
		current = current->link;
	}
	printf("NOT FOUND\n");
	return counter;
}

int insert_asc( Tag *tag, char value[7] )
{
	Node *node = (Node*)malloc(sizeof(Node));
	int i;
	if ( node != NULL )
	{
		for ( i = 0; i < 6; i++ )
			node->value[i] = value[i];
		Node *prev = NULL, *current = tag->head;
		int curV = 0, actV = atoi(value);
		if ( current != NULL )
			curV = atoi(current->value);
		while ( current != NULL && curV < actV )
		{
			prev = current;
			current = current->link;
			if ( current == NULL )
				break;
			curV = atoi(current->value);
		}
		tag->count += 1;
		if ( prev == NULL )
		{
			node->link = tag->head;
			tag->head = node;
			if ( tag->tail == NULL )
				tag->tail = node;
			return 1;
		}
		prev->link = node;
		node->link = current;
		if ( current == NULL )
			tag->tail = node;
		return 9;
	}
	puts("Out of memory");
	return -1;
}

int rangeControl_search(Tag *tag, char value[7])
{
	int counter = 1;
	Node *current = tag->head;
	int min = atoi(tag->head->value), max = atoi(tag->head->value), curV, actV = atoi(value);
	printf("Searching : %d ", actV);
	if ( actV > max )
	{
		printf("NOT FOUND\n");
		return counter;
	}
	counter++;
	if ( actV < min )
	{
		printf("NOT FOUND\n");
		return counter;
	}
	counter++;
	while ( current != NULL && atoi(current->value) <= actV )
	{
		curV = atoi(current->value);
		if ( curV == actV )
		{
			printf("FOUND\n");
			return counter;
		}
		current = current->link;
		counter++;
	}
	printf("NOT FOUND\n");
}

void removeNode(Tag *tag, char value[7])
{
	int curV, actV = atoi(value), flag = 0;
	printf("Removing : %d ", actV);
	Node *current = tag->head, *temp, *prev = NULL;
	while ( current != NULL )
	{
		curV = atoi(current->value);
		if ( curV == actV )
		{
			temp = current;
			tag->count -= 1;
			puts("FOUND");
			flag = 1;
			break;
		}
		prev = current;
		current = current->link;
	}
	if ( flag == 1 )
	{
		if ( prev == NULL )
		{
			current = current->link;
			tag->head = current;
			free(temp);
		}
		else if ( temp != NULL )
		{
			prev->link = current->link;
			free(temp);
		}
	}
	else
		puts("NOT FOUND");
}
