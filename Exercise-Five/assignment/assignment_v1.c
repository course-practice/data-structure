#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct listNode 
{
	char value[7];
	struct listNode *link;
};
typedef struct listNode ListNode;
typedef ListNode *Node;

struct listTag
{
	int count;
	Node head;
	Node tail;
};
typedef struct listTag ListTag;
typedef ListTag *Tag;

// global variable
Tag container[50];

// function prototype
int generatetxt(char *path);
int readData(char set[][7], char *path, int size);
void printSimpleArray(char arr[], int size);
void printStructure(Tag container[], int size);
// 
int hashing(char key[7], int bound);
void push(Tag *tag, char value[7]);
int simple_search(Tag *tag, char value[7]);
int insert_asc(Tag *tag, char value[7]);
int rangeControl_search(Tag *tag, char value[7]);
Tag stackCreate();
void release( Tag *tag );
void removeNode(Tag *tag, char value[7]);
int main(void)
{
	srand(time(NULL));
	char *path = "data.txt";
	char *left = "left.txt";
	char *right = "right.txt";
	char *test = "test.txt";
	char data[70][7] = {0}, searchKey[46][7]={0};
	int i, size = sizeof(container)/sizeof(container[0]), key, searchOutcome, total = 0;
	int left_size = 46, right_size = 70;
	int counter = 0;
	// debug
	int frequency[50] = {0};
	
	generatetxt(path);
	generatetxt(test);
	readData(data,path, 70);
	readData(searchKey, test, 46);

	/*
	readData(data,right, right_size);
	readData(searchKey, left, left_size);
	*/
	puts("\n\n====== INITIALIZE ======\n\n");
	for ( i = 0; i < 50; i++ )
	{
		container[i] = stackCreate();
	}
	//printSimpleArray(data, size);
	puts("\n\n====== STUPID PUSH ======\n\n");
	for ( i = 0; i < right_size; i++ )
	{
		key = hashing(data[i], 50);
		//printf("key >> %d\n", key);
		//printf("%s\n", data[i]);
		push(&container[key], data[i]);
		//int code = insert_asc(&container[key], data[i]);
		//printf("\nCODE : %d\n", code);
		frequency[key]++;
	}
	puts("\n\n====== COMPUTE LOAD FACTOR ======\n\n");
	for ( i = 0; i < size; i++ )
	{
		if ( frequency[i] > 0 )
			counter++;
	}
	printf("\n\nLoad Factor : %lf\n", (double)counter/50.0);
	// for ( i = 0; i < 50; i++ ) printf(" %d : %d\n", i, frequency[i]);
	puts("\n\n====== PRINT STRUCTURE[] ======\n\n");
	printStructure(container, 50);
	puts("Press ENTER to continue");
	getchar();
	puts("\n\n====== SIMPLE SEARCH ======\n\n");
	for ( i = 0; i < left_size; i++ )
	{
		key = hashing(searchKey[i], size);
		searchOutcome = simple_search(&container[key], searchKey[i]);
		//searchOutcome = rangeControl_search(&container[key], searchKey[i]);
		total += searchOutcome; searchOutcome = 0;
	}
	printf("Total effort to search : %d\n", total);
	printf("Average in %d round : %lf\n", left_size, (double)total/left_size);
	puts("Press ENTER to continue");
	getchar();
	puts("\n\n====== END ======\n\n");
	/*
	for ( i = 0; i < 50; i++ )
	{
		release(&container[i]);
		printStructure(container, 50);
		getchar();
	}
	*/
	for ( i = 0; i < right_size; i++ )
	{
		key = hashing(data[i], 50);
		printf("removing : %s at %d\n\n", data[i], key);
		removeNode(&container[key], data[i]);
		printStructure(container, 50);
		getchar();
	}
	// havent release array tag
	getchar();
	return 0;	
} 

void printSimpleArray(char arr[], int size)
{
	int i;
	for ( i = 0; i < size; i++ )
	{
		printf( "%s >> %s", arr[i], ((i+1)%10==0)?"\n":"" );
	}
}
void printStructure(Tag container[], int size)
{
	int i; Node current;
	for ( i = 0; i < size; i++ )
	{
		current = (*container[i]).head;
		printf("\nNew List : %d }} ", i);
		while ( current != NULL )
		{
			printf("%s >> ", current->value);
			current = current->link;
		}
	}
}
int generatetxt(char *path)
{
	FILE *fptr;
	char buff[7] = { 0 };
	int i, j;
	if ( ( fptr = fopen(path, "w") ) == NULL )
		printf("\nfile io exception : file not found\n");
	else
	{
		for( i = 0; i < 70; i++ )
		{
			buff[0] = rand()%9 + '1';
			for ( j = 1; j < 6; j++ )
				buff[j] = rand()%10 + '0';
			if ( i == 69 )
				fprintf(fptr, "%s", buff);
			else
				fprintf(fptr, "%s\n", buff);
			printf("%s\n", buff);
		}
		fclose(fptr);
		return 1;
	}
	return 0;
}

int readData(char set[][7], char *path, int size)
{	
	FILE *fptr;
	char buff[7];
	int i = 0, j = 0, n = 0, k;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	puts("readData\n");
   	while ( !feof(fptr) && n < size )
   	{
   		// fscanf(fptr, "%s", buff);
   		for ( k = 0; k < 6; k++ )
	   		fscanf(fptr," %c", &buff[k]);
   		printf("%s\n", buff);
   		for ( i = 0; i < 6; i++ )
   			set[j][i] = buff[i];
   		j++; n++;
	}
   	fclose(fptr); 
   	return 1;
}

int hashing(char key[7], int bound)
{
	int integerKey = atoi(key);
	printf("HASHING : %d\n", integerKey);
	int a[6], i, power = 100000, hashkey = -1;
	int prime[6] = { 23, 31, 47, 97, 113, 117 };
	for ( i = 0; i < 6; i++ )
	{
		a[i] = integerKey / power;
		integerKey -= a[i] * power;
		power /= 10;
		hashkey += a[i] * prime[i];
	}
	return abs(hashkey)%bound;	
}

Tag stackCreate()
{
	Tag tag = (Tag)malloc(sizeof(ListTag));
	if ( tag != NULL )
	{
		tag->count = 0;
		tag->head = NULL;
		tag->tail = NULL;
		puts("tag created");
	}
	return tag;
}

void release( Tag *tag )
{
	Node current = (*tag)->head, temp;
	while ( current != NULL )
	{
		temp = current;
		current = current->link;
		free(temp);
	}
	(*tag)->head = NULL;
	(*tag)->tail = NULL;
	(*tag)->count = 0;
	puts("fdf");
	//free( *tag );
	//return NULL;
}


void push(Tag *tag, char value[7])
{
	printf("Attemp to push %d\n", value);
	Node newNode = (Node)malloc(sizeof(ListNode));
	if ( newNode != NULL )
	{
		puts("newNode created");
		int i;
		for ( i = 0; i < 6; i++ )
			newNode->value[i] = value[i];
		puts("AA");
		newNode->link = NULL;
		if ( (*tag)->head == NULL )
			(*tag)->head = newNode;
		if ( (*tag)->tail != NULL )
			(*tag)->tail->link = newNode;
		(*tag)->tail = newNode;
		(*tag)->count += 1;
		printf("%d >> inserted\n", value);
		return;
	}
	puts("Memory not available");
}

int insert_asc(Tag *tag, char value[7])
{
	Node newNode = (Node)malloc(sizeof(ListNode));
	int ret, cur;
	ret = atoi(value);
	if ( newNode != NULL )
	{
		int i;
		for ( i = 0; i < 6; i++ )
			newNode->value[i] = value[i];
		newNode->link = NULL;
		Node current = (*tag)->head, previous = NULL;
		(*tag)->count += 1;
		if ( current != NULL )
			cur = atoi(current->value);
		while ( current != NULL && cur < ret )
		{
			previous = current;
			current = current->link;
			if ( current == NULL )
				break;
			cur = atoi(current->value);
		}
		if ( previous == NULL )
		{
			newNode->link = (*tag)->head;
			(*tag)->head = newNode;
			if ( (*tag)->tail == NULL )
				(*tag)->tail = newNode;
			return 1;
		}
		else{
			previous->link = newNode;
			newNode->link = current;
			if ( current == NULL )
				(*tag)->tail = newNode;
			return 2;
		}
	}
	return 3;
}

int simple_search(Tag *tag, char value[7])
{
	int counter = 1, i;
	Node current = (*tag)->head;
	int cur = atoi(current->value), actual = atoi(value);
	printf("Searching : %d\n", actual);
	while ( current != NULL )
	{
		
		cur = atoi(current->value);
		if ( cur == actual ){
			printf("Found\n");
			return counter;
		}
		current = current->link;
		counter++;
	}
	printf("\n%d was not found\n", value);
	return counter;
}

int rangeControl_search(Tag *tag, char value[7])
{
	int counter = 0;
	//printf("Searching : %d\n", value);
	Node current = (*tag)->head, prev = NULL;
	printf("List length : %d\n\n", (*tag)->count);
	int min = atoi((*tag)->head->value);
	int max = atoi((*tag)->tail->value); 
	int actual = atoi(value);
	printf("Searching for %d ", actual);
	int cur;
	counter += 1;
	if ( actual > max )
	{
		return counter;
	}
	counter += 1;
	if ( actual < min )
		return counter;
	while ( current != NULL && atoi(current->value) <= actual )
	{
		cur = atoi(current->value);
		if ( cur == actual ){
			printf("Found\n");
			return counter;
		}
		current = current->link;
		counter++;
	}
	printf("\n%d was not found %d\n", value, counter);
	return counter;
}

void removeNode(Tag *tag, char value[7])
{
	int actual = atoi(value), cur;
	Node current = (*tag)->head, temp, prev = NULL;
	while ( current != NULL )
	{
		cur = atoi(current->value);
		if ( cur == actual )
		{
			temp = current;
			(*tag)->count -= 1;
			puts("Found");
			break;
		}
		prev = current;
		current = current->link;
	}
	if ( prev == NULL )
	{
		current = current->link;
		(*tag)->head = current;
		free(temp);
	}
	else if ( temp != NULL ){
		prev->link = current->link;
		free(temp);
	}
}
