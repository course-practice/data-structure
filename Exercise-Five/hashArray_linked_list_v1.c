#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// struct definition
struct listNode 
{
	void *value;
	struct listNode *link;
};
typedef struct listNode ListNode;
typedef ListNode *Node;

struct listTag
{
	int count;
	Node head;
	Node tail;
};
typedef struct listTag ListTag;
typedef ListTag *Tag;

// global variable
Tag container[3];


// function prototype
int hashing(int value, int size);
int add( Tag *tag, void *value );
int addAsc( Tag *tag, void *value );
void delNode(Tag container[], int index, void *value);
int searchKey(Tag container[], int size, void *value);
void showlist(Tag container[], int size)
{
	int i;
	Node currentNode;
	for ( i = 0; i < size; i++ )
	{
		printf("%d : ", i);
		if ( container[i] != NULL )
		{
			currentNode = container[i]->head;
			while ( currentNode != NULL )
			{
				printf("%d => ", *(int*)currentNode->value);
				currentNode = currentNode->link;
			}
			printf("EOL\n");
		}
		else
			puts("Empty List\n");
	}
}

int main( void )
{
	srand( time( NULL ) );
	int random, choice;
	int *digit;
	
	int *output;
	int index;
	int N = sizeof( container ) / sizeof( Tag );
	printf("size : %d\n", N);
	int i;
	for( i = 0; i < N*2; i++ )
	{
		digit = (int*)malloc(sizeof(int));
		*digit = ( rand() % N ) + 1;
		// printf("%d ", *digit );
		output = (int *)malloc( sizeof( int ) );
		*output = hashing(*digit, N);
		// printf("hashed : %d\n", *output );
		index = addAsc(&container[*output], digit);
	}
	// free(digit);
	
	showlist(container, N);
	index = -1;
	digit = (int*)malloc(sizeof(int));
	*digit = ( rand() % N ) + 1;
	// *digit = 10; 
	printf("searching : %d ", *digit );
	index = searchKey(container, N, digit);
	printf(", outcome : %d\n", index);
	
	if ( index != -1 )
	{
		puts("A");
		delNode(container,  index, digit);
		showlist(container, N);
	}
	// showlist(container, N);
	/*
	scanf("%d", &choice);
	while ( choice != 0 )
	{
		switch(choice)
		{
			case 1:
				puts("search");
				digit = (int*)malloc(sizeof(int));
				scanf("%d", digit);
				puts("waiting...");
				printf("searching... %d\n", *digit);
				index = searchKey(container, N, digit );
				printf("index : %d", index);
				free(digit);
				break;
			default:
				break;
		}
		scanf("%d", &choice);
	}
	*/
	puts("termination");
	// remember to release nodes
	getchar();
	return 0;
}

int hashing( int value, int size )
{
	int key = value % size;
	return key;
}

Tag stackCreate( void )
{
	Tag tag;
	tag = (Tag)malloc(sizeof(ListTag));
	if ( tag != NULL )
	{
		tag->count = 0;
		tag->head = NULL;
		tag->tail = NULL;
	}
	return tag;
}

Tag release( Tag *tag )
{
	(*tag)->count = 0;
	free( (*tag)->head );
	free( (*tag)->tail );
	free( tag );
	return NULL;
}

int add( Tag *tag, void *value )
{
	Node currentNode = NULL;
	Node newNode = (Node)malloc(sizeof(ListNode));
	// puts("A");
	if ( newNode != NULL )
	{
		newNode->value = value;
		newNode->link = NULL;
		if ( *tag != NULL )
		{
			// puts("B");
			// push node
			newNode->link = (*tag)->head;
			(*tag)->head = newNode;
			// puts("BBB");
		}
		else
		{
			// puts("C");
			*tag = stackCreate();
			(*tag)->head = newNode;
			(*tag)->tail = newNode;
		}
		(*tag)->count++;
		// puts("BB");
		return 1;
	}
	else
	{
		// puts("D");
		return 0;
	}
}

int addAsc( Tag *tag, void *value )
{
	Node previousNode = NULL;
	Node currentNode = NULL;
	Node newNode = (Node)malloc(sizeof(ListNode));
	puts("A");
	if ( newNode != NULL )
	{
		newNode->link = NULL;
		newNode->value = value;
		if ( *tag == NULL )
		{
			puts("stack create");
			*tag = stackCreate();
			(*tag)->head = newNode;
			(*tag)->tail = newNode;
		}
		else
		{
			puts("B");
			currentNode = (*tag)->head;
			while ( currentNode != NULL && *(int*)currentNode->value < *(int*)value )
			{
				puts("C");
				previousNode = currentNode;
				currentNode = currentNode->link;
			}
			if ( previousNode == NULL )
			{
				puts("D");
				(*tag)->head = newNode;
				(*tag)->head->link = currentNode;
			}
			else
			{
				puts("E");
				previousNode->link = newNode;
				newNode->link = currentNode;
				if ( currentNode == NULL )
					(*tag)->tail = newNode;
			}
		}
		puts("G");
		(*tag)->count++;
		return 1;
	}
	else
	{
		puts("out of memory");
		return 0;	
	}
} 

int searchKey(Tag container[], int size, void *value)
{
	int hashKey = hashing(*(int*)value, size);
	printf("%d ", hashKey);
	Node currentNode = NULL;
	int index = -1;
	int i;
	if ( container[hashKey]->count != 0 )
	{
		currentNode = container[hashKey]->head;
		while( currentNode != NULL  )
		{
			if (*(int*)currentNode->value == *(int*)value)
			{
				index = hashKey;
				break;
			}
			else if (*(int*)currentNode->value > *(int*)value )
			{
				break;
			}
			currentNode = currentNode->link;
		}
		if ( index == -1 )
		{
			for( i = 0; i < size; i++ )
			{
				if ( i == hashKey )
				{
					puts("skip : index same as hashKey");
					continue;
				}
				if ( container[i] == NULL )
				{
					puts("skip : empty tag");
					continue;
				}
				currentNode = container[i]->head;
				while ( currentNode != NULL )
				{
					if (*(int*)currentNode->value == *(int*)value)
					{
						index = i;
						break;
					}
					currentNode = currentNode->link;
				}
				if ( index != -1 )
					break;
			}
		}
	}
	else
		puts("not found");
	return index;
}

// current goal : search first and do later
void delNode(Tag container[], int index, void *value)
{
	Node currentNode = container[index]->head;
	Node previousNode = NULL; 
	Node tempNode = NULL;
	if ( container[index]->count == 1 )
	{
		tempNode = currentNode;
		free( tempNode );
		container[index] = release( &container[index] );
	}
	else{
		while ( currentNode != NULL )
		{
			if ( *(int*)currentNode->value == *(int*)value)
			{
				tempNode = currentNode;
				break;
			}
			previousNode = currentNode;
			currentNode = currentNode->link;
		}
		if ( previousNode == NULL )
		{
			puts("previous is null");
			getchar();
			// tempNode = currentNode;
			currentNode = currentNode->link;
			container[index]->head = currentNode;
			free(tempNode->value);
			free(tempNode);
		}
		else if ( tempNode != NULL )
		{
			puts("temp not null");
			getchar();
			previousNode->link = currentNode->link;
			free(tempNode->value);
			free(tempNode);
		}
	}
	(container[index]->count)--;
}

