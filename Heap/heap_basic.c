#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void max_heapify(int arr[], int i, int N);
void build_max_heap(int array[], int n);
void heapsort(int array[], int n);

int main(void)
{
	srand(time(NULL));
	int array[1000] = { 0 };
	int N = 10, i;
	for ( i = 1; i <= 10; i++ )
		array[i] = rand()%10;
	for ( i = 0; i <= N; i++ )
		printf("%d >> ", array[i]);
		
	heapsort(array,N);
	puts("");
	for ( i = 0; i < N; i++ )
		printf("%d >> ", array[i]);
	getchar();
	return 0;
} 

void heapsort(int array[], int n)
{
	build_max_heap(array, n);
	printf("max : %d\n", array[0]);
	int i;
	puts("max_heapify()");
	for ( i = 0; i < n; i++ )
		printf("%d >> ", array[i]);
	puts("");
	for (i = n; i>= 1; i-- )
	{
		swap(&array[i], &array[1]);
		max_heapify(array, 1, i-1);	
	}
}

void build_max_heap(int array[], int n)
{
	int k;
	for ( k = n/2; k >= 1; k-- )
	{
		printf("k %d >> ", k);
		max_heapify(array, k, n); // 
	}
}

void swap(int *i, int *j)
{
	int temp = *i;
	*i = *j;
	*j = temp;
}
void max_heapify(int arr[], int i, int N)
{
	int left = 2 * i; 
	int right = 2 * i + 1;
	int largest;
	printf("\nLeft %d Right %d\n", left, right);
	if ( left <= N && arr[left] > arr[i] )
		largest = left;
	else
		largest = i;
	if ( right <= N && arr[right] > arr[largest] )
		largest = right;
	printf("[%d] : %d, [largest-%d] %d\n", i, arr[i], largest, arr[largest]);
	if ( largest != i )
	{
		swap( &arr[i], &arr[largest] );
		max_heapify( arr, largest, N );
	}
}
