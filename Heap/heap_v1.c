#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void addItem(int array[], int *last, int item)
{
	printf("addItem\n\n");
	array[*last] = item;
	
	printf("last++");
	heapify_up(array, *last-1);
	(*last)++;
}
void swap(int *e1, int *e2)
{
	int temp = *e1;
	*e1 = *e2;
	*e2 = temp;
}
void heapify_up(int array[], int index)
{
	printf("heapify_up\n\n");
	int parent = (index-1)/2;
	printf("parent %d array[parent] %d array[index] %d\n", parent, array[parent], array[index]);
	while ( parent >= 0 && array[parent] > array[index] )
	{
		swap(&array[parent], &array[index] );
		index = parent;
		parent = (index-1) / 2;
		printf("parent %d array[parent] %d array[index] %d\n", parent, array[parent], array[index]);
	}
	puts("End heapify_up");
}

void heapify_down(int array[], int size)
{
	int index = 0;
	while ( index * 2 + 1 < size )
	{
		int left = index * 2 + 1;
		int smallerIndex = left;
		int right = left + 1;
		if ( right < size && array[right] < array[left] )
			smallerIndex = right;
		if ( array[index] < array[smallerIndex] )
			break;
		else
			swap( &array[index], &array[smallerIndex] );
		index = smallerIndex;
	}
}

int Parent(int i){return floor(i/2);}
int Left(int i){return 2*i;}
int Right(int i){return 2*i+1;}

void max_heapify(int array[], int i)
{
	int left = Left(i);
	int right = Right(i);
	int largest;
	if ( left <= 12 && array[left] > array[i] )
		largest = left;
	else
		largest = i;
	if ( right <= 12 && array[right] > array[largest] )
		largest = right;
	if ( largest != i )
	{
		array[i] = array[largest] + array[i];
		array[largest] = array[i] - array[largest];
		array[i] = array[i] - array[largest];
		max_heapify(array, largest);
	}	
}

int main(void)
{
	srand(time(NULL));
	int i, array[12], heapArray[12] = {0};
	for ( i = 0; i < 12; i++ )
		array[i] = rand()%30;
	for (i = 0; i < 12; i++ )
	{
		printf("%d >> ", array[i] );
	}
	puts("");
	// for ( i = 0; i < 12; i++ )
		max_heapify(array, 1);
	for (i = 0; i < 12; i++ )
	{
		printf("%d >> ", array[i] );
	}
	getchar();
	return 0;	
} 
