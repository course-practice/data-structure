#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>

// function prototype
void max_heapify(int arr[], int i, int N);
void build_max_heap(int array[], int n);
void heapsort(int array[], int n);

int main(void)
{
	srand(time(NULL));
	int arr[10] = {0};
	int i, j;
	for (i = 0; i < 10; i++ )
	{
		arr[i] = rand() % 37;
		build_max_heap(arr, i); 
		// keep the max-heap property of this array, i is the actual size
		puts("");
		for ( j = 0; j < 10; j++ )
			printf("%d >> ", arr[j] );	
	}
	puts("xxx");
	// in order to sort the array back to ascending order
	// swap every i node with 0 node
	// then call max-heapify set i to 0 and reduce map from size-1 to zero
	for (i = 9; i>= 1; i-- )
	{
		swap(&arr[i], &arr[0]);
		max_heapify(arr, 0, i-1); 
	}
	for ( j = 0; j < 10; j++ )
		printf("%d >> ", arr[j] );	
	getchar();
	return 0;
}

void heapsort(int array[], int n)
{
	build_max_heap(array, n);
	int i;
	for (i = n-1; i>= 0; i-- )
	{
		swap(&array[i], &array[1]);
		max_heapify(array, 1, i);	
	}
}

void build_max_heap(int array[], int n)
{
	int k;
	// n/2-1 because n/2 imply left = 10 and right = 11
	// which doesn't need any operation in the max_heapify function
	for ( k = n/2 -1; k >= 0; k-- ) 
	{
		max_heapify(array, k, n); // this is the bottom to up approach
	}
}

void swap(int *i, int *j)
{
	int temp = *i;
	*i = *j;
	*j = temp;
}
void max_heapify(int arr[], int i, int N)
{
	int left = 2 * i; 
	int right = 2 * i + 1;
	int largest;
	// always ensure left and right index stay within the safe boundry
	// below operation is to identify the index of the largest element
	if ( left <= N && arr[left] > arr[i] )
		largest = left;
	else
		largest = i;
	if ( right <= N && arr[right] > arr[largest] )
		largest = right;
	// in max-heap, parent node are expected to be the larger than child nodes
	// therefore further action are required if element i is not largest
	if ( largest != i )
	{
		swap( &arr[i], &arr[largest] );
		max_heapify( arr, largest, N );
	}
}
