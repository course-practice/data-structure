#include <stdio.h>
#include <stdlib.h>
int binarySearch(int data[], int begin, int last, int t, int tracker)
{
	printf("target : %d\n", t);
	if ( begin >= last ) return 0;
	int mid = begin + ( last  - begin ) / 2;
	printf("mid : %d\n", mid);
	printf("data[mid] : %d\n", data[mid]);
	getchar();
	
	if ( data[mid] > t )
		return binarySearch(data, begin, mid-1, t, tracker+1 );
	if ( data[mid] < t )
		return binarySearch(data, mid+1, last, t, tracker+2 );
	printf("tracker : %d\n", tracker+2);
	return 1;
}
int main(void)
{
	int data[10] = {0};
	int i;
	for ( i = 0; i < 10; i++ )
		data[i] = i * i;
	int outcome = binarySearch(data, 0, 10, 81, 0);
	printf("%d %s\n", 4, (outcome == 1)?"F":"N");
	getchar();
	return 1;
}
