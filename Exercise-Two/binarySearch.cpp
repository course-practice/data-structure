#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int ds[10000] = { 0 };
int binarySearch(int ds[], int size, int t, long *tracker)
{
	printf("Searching for %d\n", t);
	int left, right, mid;
	int currentIndex = size;
	
	left = 0;
	right = size;
	currentIndex = (right - left ) / 2;
	while( right >= left )
	{
		
		//printf("Round %ld\n", *tracker);
		//printf("currentIndex : %d\n", currentIndex);
		//printf("current value : %d\n", ds[currentIndex]);
		if( ds[currentIndex] == t )
		{
			//*tracker = *tracker + 1;
			return 1;
		}
		if( ds[currentIndex] > t )
		{
			*tracker = *tracker + 1;
			//puts("too big");
			//getchar();
			right = currentIndex - 1;
			//currentIndex = ( right - left ) / 2 + left;
			//continue;
		}
		else
		{
			//*tracker = *tracker + 1;
			//puts("too small");
			//getchar();
			left = currentIndex + 1;
			
			//continue;
		}
		/*
		if( currentIndex == 0 || currentIndex == size-1 || left == right - 1)
		{
			*tracker = *tracker + 1;
			eos = 1;
			//puts("xx");
			return 0;
		}
		*/
		currentIndex = ( right - left ) / 2 + left;
	}
	return 0;
}
int main( void )
{
	srand(time(NULL));
	int t, output;
	// unsigned long start_t, end_t;
	// unsigned long comparison, movement;
	long tracker = 0;
	int loopSize = 100;
	int size = sizeof( ds ) / 4;
	ds[size-1] = 32767;
	for(int i = size-2; i >= 0; i-- )
		ds[i] = ds[i+1] - ( rand() % 3 );
	for( int k = 0; k < loopSize; k++ )
	{
		t = ( rand() % 32767 );
		output = binarySearch( ds, size, t, &tracker );
		printf("%d was %s\n", t, (output==1)?"found":"not found");
	}
	printf("Called binarySearch function %d times\n", loopSize);
	printf("%ld comparision made and average about %lf made on each search\n", tracker, (double)tracker/loopSize);
	getchar();
	return 0;
}
