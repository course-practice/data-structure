// This program is written by Yeoh Chi Ee
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 100 
int ternarySearch(int list[], int head, int tail, int target, int *tracker);

int list[10000] = { 0 };
int main( void ){
	srand(time(NULL));
	int size = sizeof ( list ) / 4;
	int output = 0; 
	int tracker = 0;
	int i, t, total = 0;
	double avg;
	list[size-1] = 32767;
	// list[size-1] = -5;
	for( i = size-2; i >= 0; i-- ){
		// list[i] = -5;
		list[i] = list[i+1] - ( rand() % 3 );
	}
		
	// list[4] = -5;
	
	for ( i = 0; i < N; i++ ){
		t = rand() % (32767);
		// t = -5;
		// t = 32777;
		output = ternarySearch(list, 0,size-1, t, &tracker);
		printf("%d was %s\n", t, (output==1)?"found":"not found");
		printf("loop size : %d\n", tracker);
		total += tracker;
		tracker = 0;
	}
	
	/*
	output = ternarySearch(list, 0,size-1, -5, &tracker);
	printf("%d was %s\n", -5, (output==1)?"found":"not found");
	printf("loop size : %d\n", tracker);
	*/
	printf("avg : %lf\n", (double)total/(double)N);
	
	/*	
	for ( i = 0; i < size; i++ )
		printf("%d ", list[i]);
	*/
	getchar();
	return 0;
} 

int ternarySearch(int list[], int head, int tail, int target, int *tracker){
	int mid1, mid2;
	while ( tail >= head ){
		mid1 = head + ( tail - head ) / 3;
		mid2 = tail - ( tail - head ) / 3;
		(*tracker)++;
		if ( list[mid1] > target ){
			tail = mid1 - 1;
			continue;
		}
		(*tracker)++;
		if ( list[mid2] > target ){
			head = mid1 + 1;
			tail = mid2 - 1;
			continue;
		}
		(*tracker)++;
		if ( list[mid2] < target){
			head = mid2 + 1;
		}
		else{
			return 1;
		}
	}
	return 0;
}
// This program is written by Yeoh Chi Ee
