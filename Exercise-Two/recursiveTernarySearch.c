#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int ternarySearch(int list[], int head, int tail, int target, int *tracker){
	int mid1 = head + ( tail - head ) / 3;
	int mid2 = tail - ( tail - head ) / 3;
	if ( tail < head )
		return 0;
		(*tracker)++;
	if ( list[mid1] > target ) // left 
		return ternarySearch(list, head, mid1-1, target, tracker );
		(*tracker)++; 
	if ( list[mid2] > target ) // mid
		return ternarySearch(list, mid1+1, mid2-1, target, tracker );
		(*tracker)++;
	if ( list[mid2] < target ) // right
		return ternarySearch(list, mid2+1, tail, target, tracker );
	else
		return 1;
}

int data[10000] = { 0 };
int main( void ){
	srand(time(NULL));
	
	int i, t, loop, size, tracker = 0, total;
	loop = 100;
	size = sizeof( data ) / sizeof ( int );
	data[size-1] = 32767;
	for ( i = size-2; i >= 0; i-- )
		data[i] = data[i+1] - rand() % 3;
	for ( i = 0; i < loop; i++ )
	{
		t = rand() % 32767;
		t = data[5000];
		printf("%d was %s in", t, (ternarySearch(data, 0, size-1, t, &tracker)==1)?"found":"not found");
		printf(" %d loop\n", tracker);
		total += tracker;
		tracker = 0;
	}
	printf("total %d, avg %lf\n", total, (double)total/loop);
	getchar();
	return 0; 
}
 
