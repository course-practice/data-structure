typedef struct BRANCH{
	int v;
	struct BRANCH *lt, *rt;
}Branch;

typedef struct TREE{
	Branch *root;
}Tree;

Branch *createBranch(int v)
{
	Branch *branch = (Branch*)malloc(sizeof(Branch));
	if ( branch != NULL )
	{
		branch->lt = NULL;
		branch->rt = NULL;
		branch->v = v;
	}
	return branch;
}

void insert(Branch **root, Branch *branch)
{
	if ( *root == NULL )
	{
		printf(">> Inserted\n");
		*root = branch;
		return;
	}
	if ( branch->v > (*root)->v )
	{	puts(">> Go Right");
		insert(&((*root)->rt), branch);
	}
	else if ( branch->v < (*root)->v )
	{	puts(">> Go Left");
		insert(&((*root)->lt), branch);
	}
	else
	{
		printf(">> Duplication detected\n");
		free(branch);
	}
}

void inOrder(Branch *root)
{
	if ( root == NULL ) return;
	inOrder(root->lt);
	printf("%d >> ", root->v );
	inOrder(root->rt);
}

void preOrder(Branch *root)
{
	if ( root == NULL ) return;
	printf("%d >> ", root->v );
	preOrder(root->lt);
	preOrder(root->rt);
}

void postOrder(Branch *root)
{
	if ( root == NULL ) return;
	postOrder(root->lt);
	postOrder(root->rt);
	printf("%d >> ", root->v );
}

