#include <stdlib.h>
#include <stdio.h>
#include <time.h>
// self define library
#include "TREE.h"
// #include "QUEUE.h"



int main(void)
{
	Tree tree; tree.root = NULL;
	srand(time(NULL));
	int i, v;
	/*
	initT(&tree);
	buildTree(&tree);
	printf("\nBFS Traversal\n");
	BFSTraversal(tree.root);
	printf("\nBFS Traversal Div3\n");
	BFSTraversalDivThree(tree.root);
	*/
	Branch *root = createBranch(7);
	for ( i = 0; i < 20; i++ )
	{
		v = rand() % 20 + 1;
		printf(">> V -> %d\n", v);
		insert(&root, createBranch(v));
		puts("end\n");
	}
	printf("\nIn Order\n");
	inOrder(root);
	printf("\nPre Order\n");
	preOrder(root);
	printf("\nPost Order\n");
	postOrder(root);
	return 0;
}
