#include <time.h>
#include <math.h>
 
typedef struct LEAVE{
	int v, depth;
	struct LEAVE *p1, *p2, *p3, *p4;
}Leave;

typedef struct NODE{
	Leave *leave;
	struct NODE *link;
}Node;

typedef struct QUEUE{
	int count;
	Node *sp, *ep;	
}Queue;


typedef struct TREE{
	int count;
	Leave *root;
}Tree;

void initT(Tree *tree)
{
	tree->count = 0;
	tree->root = NULL;	
}
void initQ(Queue *queue)
{
	queue->count = 0;
	queue->ep = NULL;
	queue->sp = NULL;
}
Leave *createLeave(int depth)
{
	
	Leave *leave = (Leave*)malloc(sizeof(Leave));
	if ( leave != NULL )
	{
		int v = rand() % 30000 + 1;
		v *= (rand()%2==1)?-1:1;
		leave->v = v;
		leave->depth = depth;
		leave->p1 = NULL;
		leave->p2 = NULL;
		leave->p3 = NULL;
		leave->p4 = NULL;
	}
	return leave;
}
Node *createNode(Leave *leave)
{
	Node *node = (Node*)malloc(sizeof(Node));
	if ( node != NULL )
	{
		node->leave = leave;
		node->link = NULL;
	}
	return node;
}
void enqueue(Queue *queue, Node *node)
{
	if ( queue->count == 0 ) 
		queue->sp = node;
	else
		queue->ep->link = node;
	queue->ep = node;
	queue->count += 1;
}
Leave *dequeue(Queue *queue)
{
	Node *node = queue->sp;
	Leave *leave = node->leave;
	if ( queue->count == 1 )
	{
		queue->sp = NULL;
		queue->ep = NULL;
	}
	else
		queue->sp = node->link;
	queue->count -= 1;
	free(node);
	return leave;
}
void printQueue(Queue *queue)
{
	Node *current = queue->sp;
	while ( current != NULL )
	{
		printf("%d >> ", current->leave->v );
		current = current->link;
	} puts("|| EOQ\n");
}
void buildTree(Tree *tree)
{
	srand(time(NULL));
	int height = 0, targetHeight = 2, branch = 0, branchFactor = 4, flag = 0;
	Queue queue; initQ(&queue);
	tree->root = createLeave(0);
	enqueue(&queue, createNode(tree->root));
	while ( 1 )
	{
		Leave *parent = dequeue(&queue);
		printQueue(&queue);
		printf(">> Current : %d\n", parent->v );
		parent->p1 = createLeave(parent->depth+1);
		parent->p2 = createLeave(parent->depth+1);
		parent->p3 = createLeave(parent->depth+1);
		parent->p4 = createLeave(parent->depth+1);
		if ( parent->depth == targetHeight ) branch++;
		else
		{
			enqueue(&queue, createNode(parent->p1));
			enqueue(&queue, createNode(parent->p2));
			enqueue(&queue, createNode(parent->p3));
			enqueue(&queue, createNode(parent->p4));
		}
		if ( branch == pow( branchFactor, targetHeight ) ) break; // power of 3 
	}
	printf(">>>> Outcome : Finish Building Tree\n");
}

void BFSTraversal( Leave *root )
{
	int depth = INT_MIN;
	Queue queue; initQ(&queue);
	enqueue(&queue, createNode(root));
	while ( queue.count != 0 )
	{
		Leave *parent = dequeue(&queue);
		if ( parent->depth > depth )
		{
			depth = parent->depth;
			printf("\n\n# Depth: %d\n\n", depth);
		}
		printf(">> %6d ", parent->v);
		if ( parent->p1 != NULL )
		{
			enqueue(&queue, createNode(parent->p1));
			enqueue(&queue, createNode(parent->p2));
			enqueue(&queue, createNode(parent->p3));
			enqueue(&queue, createNode(parent->p4));
		}
	}
}

void BFSTraversalDivThree( Leave *root )
{
	int depth = INT_MIN;
	Queue queue; initQ(&queue);
	enqueue(&queue, createNode(root));
	while ( queue.count != 0 )
	{
		Leave *parent = dequeue(&queue);
		if ( parent->depth > depth )
		{
			depth = parent->depth;
			printf("\n\n# Depth: %d\n\n", depth);
		}
		printf(">> %6d ", parent->v);
		if ( parent->p1 != NULL )
		{
			if ( parent->p1->v % 3 == 0 )	enqueue(&queue, createNode(parent->p1));
			if ( parent->p2->v % 3 == 0 )	enqueue(&queue, createNode(parent->p2));
			if ( parent->p3->v % 3 == 0 )	enqueue(&queue, createNode(parent->p3));
			if ( parent->p4->v % 3 == 0 )	enqueue(&queue, createNode(parent->p4));
		}
	}
}












