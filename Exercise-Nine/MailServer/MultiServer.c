#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// constant variables
#define ARRIVAL_RATE 60
#define BASE_SPEED 20 
#define TOP_SPEED 60
#define FAULT_RATE 30
#define LOOP 100
#define CORE 1 

typedef struct MAIL{
	int tag, hit;
	struct MAIL *next;
}Mail;

typedef struct PROCESSOR{
	int load, sent, cumQ, maxQ, idle;
	Mail *rear;
}Processor;

typedef struct SERVER{
	Processor cores[CORE];
	int tagSeq, firstAttempt, secondAttempt, other, maxQ, cumulativeLoad, cumulativeSuccess, cumulativeAttempt;
	double avgSentPM, avgQ, avgAttempt;
}Server;

Mail *createMail(int tag)
{
	Mail *mail = (Mail*)malloc(sizeof(Mail));
	if ( mail != NULL )
	{
		mail->hit = 0;
		mail->tag = tag;
		mail->next = NULL;
	}
	return mail;
}

void initP(Processor *proc)
{
	proc->cumQ = proc->load = proc->maxQ = proc->sent = proc->idle = 0;
	proc->rear = NULL;
}

void initS(Server *server)
{
	int seq; for ( seq = 0; seq < CORE; seq++ ) initP(&(server->cores[seq])); 
	server->tagSeq = server->cumulativeAttempt = server->maxQ = server->cumulativeLoad = server->cumulativeSuccess = server->firstAttempt = server->secondAttempt = server->other = 0;
	server->avgAttempt = server->avgQ = server->avgSentPM = 0.0;
}

void enqueue(Processor *proc, Mail *mail)
{
	mail->hit += 1;
	if ( proc->load == 0 )
		mail->next = mail;
	else	{
		mail->next = proc->rear->next;
		proc->rear->next = mail;
	}
	proc->rear = mail;
	proc->load += 1;
}

Mail *dequeue(Processor *proc)
{
	Mail *mail = proc->rear->next;
	if ( proc->load == 1 )
		proc->rear = NULL;
	else
		proc->rear->next = mail->next;
	proc->load -= 1;
	return mail;
}

int New_Messages(Server *server)
{
	printf(">> New Messages()\n");
	int seq, load = rand() % ARRIVAL_RATE + 1;
	for ( seq = 0; seq < load; seq++ )
	{
		server->tagSeq += 1;
		int newTag =  server->tagSeq;
		Mail *mail = createMail(newTag);
		int dist = rand() % CORE;
		enqueue(&(server->cores[dist]), mail); 
	}
	return load;
}

int Server_Free_Sub( Server *server, Processor *proc )
{
	if ( proc->load == 0 )
	{
		proc->idle += 1;
		return 0;
	}
	int seq, stat, tag, batchLoad = rand() % ( TOP_SPEED - BASE_SPEED ) + BASE_SPEED;
	double successRate = 0;
	for ( seq = 0; seq < batchLoad; seq++ )
	{
		if ( proc->load > 0 )
		{
			stat = rand() % 100;
			if ( stat > FAULT_RATE )
			{
				Mail *mail = dequeue(proc);
				if ( mail->hit == 1 )
					server->firstAttempt += 1;
				else if ( mail->hit == 2 )
					server->secondAttempt += 1;
				else
					server->other += 1;
				server->cumulativeAttempt += mail->hit;
				successRate += 1;
			}
			else
				proc->rear->next->hit += 1;
			proc->sent += 1;
			if ( proc->load == 0 ) break;
		}
		else break;
		proc->rear = proc->rear->next;
	}
	return successRate;
}

int Server_Free_Main( Server *server, int run )
{
	int seq, success = 0;
	for ( seq = 0; seq < CORE; seq++ )
	{
		success += Server_Free_Sub(server, &(server->cores[seq]));
	}
	// printf( ">> Success : %d\n", success );
	return success;
}

void SVC_Complete(Server *server, int success, int run)
{
	// rate
	server->cumulativeSuccess += success;
	server->avgSentPM = (double)server->cumulativeSuccess / (double) run;
	// size
	int seq, load, maxQ; load = maxQ = 0;
	for ( seq = 0; seq < CORE; seq++ )
	{
		if ( (server->cores[seq]).load > (server->cores[seq]).maxQ )
			(server->cores[seq]).maxQ = (server->cores[seq]).load;
		// maxQ += (server->cores[seq]).maxQ;
		load += (server->cores[seq]).load;
	}
	if ( server->maxQ < load + success )
		server->maxQ = load + success;
	// avg size
	server->cumulativeLoad += (load + success);
	server->avgQ = (double)server->cumulativeLoad / (double) run;
	// attempt
	server->avgAttempt = (double) server->cumulativeAttempt / (double) server->tagSeq;	
}

void PrintStatistics(Server *server, int iteration)
{
	int seq; 
	printf("\n>> SPEC\n");
	printf(">> Core Count : %d\n", CORE );
	printf(">> [ Base Speed, Top Speed ] : [ %d, %d ]\n", BASE_SPEED, TOP_SPEED );
	printf(">> Fault Rate : %d\n", FAULT_RATE );
	printf("\n>> Senario\n");
	printf(">> Arrival Rate : %d\n", ARRIVAL_RATE );
	printf(">> Iteration : %d\n", iteration );
	printf("Report\n");
	printf(">> Total messages that arrived : %d\n", server->tagSeq );
	printf(">> Total messages sent : %d\n", (server->cores[0]).sent + (server->cores[1]).sent );
	for ( seq = 0; seq < CORE; seq++ )
		printf(">> By C%d : %d\n", seq, (server->cores[seq]).sent );
	printf(">> Average number of messages succesfully sent per minute : %4.6lf\n", server->avgSentPM );
	printf(">> Number of messages sent on the 1st attempt : %d\n", server->firstAttempt );
	printf(">> Number of messages sent on the 2nd attempt : %d\n", server->secondAttempt );
	printf(">> Number of messages sent on more than 2 attempt : %d\n", server->other );
	printf(">> Average number of attempt to send a messages successfully : %4.6lf\n", server->avgAttempt );
	printf(">> Maximum queue size : %d\n", server->maxQ );
	printf(">> Average queue : %4.6lf\n", server->avgQ );
	for ( seq = 0; seq < CORE; seq++ )
		printf(">> Idle C%d : %d\n", seq, (server->cores[seq]).idle );
}

void Driver(Server *server)
{
	int seq, arrival, success;
	for ( seq = 0; seq < LOOP; seq++ )
	{
		printf(">> Iteration : %3d\n", seq);
		arrival = New_Messages(server);
		printf(">> Arrival : %2d\n", arrival );
		
		success = Server_Free_Main(server, seq);
		printf(">> ServerFree : %2d\n", success );
		SVC_Complete( server, success, seq );
		puts("\n");
	}
	PrintStatistics(server, seq);
}

int main(void)
{
	srand(time(NULL));
	Server server; initS(&server);
	Driver(&server);
	return 0;
} 
