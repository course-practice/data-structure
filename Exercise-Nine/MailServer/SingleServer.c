#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// constant variables
#define ARRIVAL_RATE 60
#define BASE_SPEED 20
#define TOP_SPEED 60
#define FAULT_RATE 30
#define LOOP 100

// self define data structures
typedef struct PROC{
	int tag, hit;
	struct PROC *next;
}Proc;

typedef struct QUEUE{
	int load, tagCount, sent, firstAttempt, secondAttempt, other, cumulativeLoad, cumulativeSuccess, cumulativeAttempt, maxQ;
	double avgSentPM, avgQ, avgAttempt;
	Proc *Rear;
}Queue;

// function prototype
void Driver();
int main(int argc, char *argv[])
{
	printf("Welcome %s\n", (argc>1)?argv[1]:"Guest");
	Driver();
	return 0;
}
Proc *createProc(int tag)
{
	Proc *proc = (Proc*)malloc(sizeof(Proc));
	if ( proc != NULL )
	{
		proc->tag = tag;
		proc->hit = 0;
		proc->next = NULL;
	}
	return proc;
}

void initQ(Queue *queue)
{
	queue->load = queue->tagCount = queue->sent = queue->firstAttempt = queue->secondAttempt = queue->cumulativeSuccess = queue->cumulativeLoad = queue->cumulativeAttempt = queue->maxQ = 0;
	queue->avgSentPM = queue->avgQ = queue->avgAttempt = 0.0;
	queue->Rear = NULL;
}

void enqueue(Queue *queue, Proc *proc)
{
	proc->hit += 1;
	if ( queue->load == 0 )
		proc->next = proc;
	else	{
		proc->next = queue->Rear->next;
		queue->Rear->next = proc;
	}
	queue->Rear = proc;
	queue->load += 1;
}

Proc *dequeue(Queue *queue)
{
	Proc *proc = queue->Rear->next;
	if ( queue->load == 1 )
		queue->Rear = NULL;
	else
		queue->Rear->next = proc->next;
	queue->load -= 1;
	return proc;
}

int peek(Queue *queue)
{
	return queue->Rear->next->tag;
}

int New_Messages(Queue *queue)
{
	printf(">> New Messages()\n");
	int seq, batchLoad = rand() % 60 + 1;
	for ( seq = 0; seq < batchLoad; seq++ )
	{
		queue->tagCount += 1;
		int newTag =  queue->tagCount;
		Proc *proc = createProc(newTag);
		enqueue(queue, proc);
	}
	return batchLoad;
}

void tagging(Queue *queue)
{
	// printf(">> Tagging for failed package\n");
	queue->Rear->next->hit += 1;
}

int svcComplete(Queue *queue, int run, int success)
{
	printf(">> Iteration : %d || Service completed\n", run );
	queue->cumulativeSuccess += success;
	queue->avgSentPM = (double) queue->cumulativeSuccess / (double) run;
	// printf(">> Load : %d\n", queue->load );
	if ( queue->maxQ < queue->load + success )
	{
		queue->maxQ = queue->load + success;
		// printf(">> MaxQ : %d", queue->maxQ );
		// getchar();
	}
	queue->cumulativeLoad += queue->load + success;
	queue->avgQ = (double) queue->cumulativeLoad / (double) run;
	queue->avgAttempt = (double) queue->cumulativeAttempt / (double) queue->tagCount;
	return 0;
}

int Server_Free(	Queue *queue, int run )
{
	printf(">> Server Free\n");
	int seq, stat, tag, batchLoad = rand() % ( TOP_SPEED - BASE_SPEED ) + BASE_SPEED;
	double successRate = 0;
	// printf(">> Run : %d\n", run);
	for ( seq = 0; seq < batchLoad; seq++ )
	{
		if ( queue->load > 0 ) 
		{
			stat = rand() % 100;
			// printf(">> stat %d\n", stat);
			// getchar();
			if ( stat > FAULT_RATE ) // success
			{
				Proc *package = dequeue(queue);
				
				int tag = package->tag;
				// printf(">> Package Tag : %d\n", package->tag );
				if ( package->hit == 1 )
					queue->firstAttempt += 1;
				else if ( package->hit == 2 )
					queue->secondAttempt += 1;
				else
					queue->other += 1;
				queue->cumulativeAttempt += package->hit;
				// printf("Seq %d>> Package Tag : %d || Status : SENT!\n", seq, tag );
				successRate += 1;
				free(package);
			}
			else // failed
			{
				tag = peek(queue);
				tagging(queue);
				// printf("Seq %d>> Package Tag : %d || Status : FAILED!\n", seq, tag );
			}
			queue->sent += 1;
		}
		if ( queue->load == 0 )
		{
			// printf(">> Is Empty\n");
			break;
		}
		queue->Rear = queue->Rear->next;
	}
	return successRate;
}

void printStatistics(Queue queue)
{
	printf("\n>> Report\n");
	printf( ">> Total messages that arrived : %d\n", queue.tagCount );
	printf( ">> Total messages sent : %d\n", queue.sent );
	printf( ">> Average number of messages successfully sent per minute : %4.6lf\n", queue.avgSentPM );
	printf( ">> Number of messages sent on the 1st attempt : %d\n", queue.firstAttempt );
	printf( ">> Number of messages sent on the 2nd attempt : %d\n", queue.secondAttempt );
	printf( ">> Number of messages sent on the other attempt : %d\n", queue.other);
	printf( ">> Average number of attempts to send a messages successfully : %4.6lf\n", (double)queue.avgAttempt );
	printf( ">> Maximum queue size : %d\n", queue.maxQ );
	printf( ">> Average queue : %4.6lf\n", queue.avgQ );
}
void Driver()
{
	srand(time(NULL));
	int seq, batchLoad, load, success;
	Queue queue; initQ(&queue);
	int i, total = 0;
	for ( i = 0; i < LOOP || queue.load > 0; i++ )
	{
		printf("\n>> Iteration : %d\n", i);
		// arrival
		if ( i < LOOP ){
			batchLoad = New_Messages(&queue);
			// printf("New Arrival : %d\n", batchLoad );
		}
		total += batchLoad;
		// sending out
		success = Server_Free(&queue, i);
		// printf("Success Proc : %d\n", success );
		// svcComplete
		svcComplete(&queue, i, success);
		printf(">> Size : %d\n", queue.load);
	}
	printf("avg arrival : %4.6lf\n", (double)total/i);
	printStatistics(queue);
}

















