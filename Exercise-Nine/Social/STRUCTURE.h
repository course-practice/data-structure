#define LEN 11

typedef struct POINTER{
	void *pointer;
	struct POINTER *link;
}Pointer;

typedef struct PERSON{
	char name[LEN];
	int depth;
	char type;
	Pointer *friends;
}Person;

typedef struct QUEUE{
	int count;
	Pointer *top, *rear;
}Queue;

typedef struct STACK{
	int count;
	Pointer *top;
}Stack;

void initQ(Queue *queue)
{
	queue->count = 0;
	queue->rear = queue->top = NULL;
}

Queue *createQueue()
{
	Queue *queue = (Queue*)malloc(sizeof(Queue));
	if ( queue != NULL )
	{
		queue->rear = queue->top = NULL;
		queue->count = 0;
	}
	return queue;
}

Person *createPerson(char name[LEN], int len)
{
	Person *person = (Person*)malloc(sizeof(Person));
	if ( person != NULL )
	{
		person->friends = NULL;
		person->depth = 0;
		person->type = 'S';
		memset(person->name,'\0', LEN);
		strcpy(person->name, name);
	}
	return person;
}

Pointer *createPointer(Person *person)
{
	Pointer *pointer = (Pointer*)malloc(sizeof(Pointer));
	if ( pointer != NULL )
	{
		pointer->pointer = person;
		pointer->link = NULL;
		// printf("Debug pointer created\n");
	}
	return pointer;
}
void enqueue(Queue *queue, void *pointer )
{
	if ( queue->count == 0 )
		queue->top = pointer;
	else
		queue->rear->link = pointer;
	queue->rear = pointer;
	queue->count += 1;
}

Pointer *dequeue(Queue *queue)
{
	Pointer *person = (Pointer*)queue->top->pointer;
	if ( queue->count == 1 )
	{
		queue->top = NULL;
	}
	else				
		queue->top = queue->top->link;
	queue->count -= 1;
	return person;
}

Person *getMember(Queue *queue, char name[LEN], int len)
{
	Pointer *current = queue->top;
	while ( current != NULL )
	{
		if ( strcmp(((Person*)current->pointer)->name, name) == 0 )
		{
			// printf("Debug Found\n");
			return (Person*)(current->pointer);
		}
		current = current->link;
	}
}

void addEdge(Queue *queue, char name1[LEN], char name2[LEN], int len1, int len2)
{
	// printf("\nDebug addEdge\n");
	Person *p1 = getMember(queue, name1, len1);
	Person *p2 = getMember(queue, name2, len2);
	// printf("\nDebug got em all\n");
	Pointer *pointer = createPointer(p2);
	// printf("\nDebug pointer\n");
	pointer->link = p1->friends;
	// printf("\nDebug enqueue\n");
	p1->friends = pointer;
	// puts("\nDebug end add");	
}

int contains(Queue *queue, char name[LEN])
{
	Pointer *current = queue->top;
	while ( current != NULL )
	{
		if ( strcmp(((Person*)(current->pointer))->name, name) == 0 ) 
		{
			return 1;
		}
		current = current->link;
	}
	return 0;
}

void BFSTraversalByDepth(Queue *graph, char name[LEN], int len, int depth)
{
	Queue queue; initQ(&queue);
	Queue visited; initQ(&visited);
	Person *member = getMember(graph, name, len);
	Pointer *state = createPointer(member);
	enqueue(&queue,state);
	while ( queue.count != 0 )
	{
		Person *current = (Person*)dequeue(&queue);
		// check visited
		if ( contains(&visited, current->name) )
		{
			// printf(">> Visited\n");
			current->depth = 0;
			continue;
		}
		printf("D : %d >> %s\n", current->depth, current->name );
		if ( current->depth == depth )
		{
			// printf(">> Depth Limit\n");
			current->depth = 0;
			continue;
		}
		// append to visited
		enqueue(&visited, createPointer(member));
		// first child
		Pointer *child = current->friends;
		// expend child
		// printf("\n>> Expand Child\n");
		// getchar();
		while ( child != NULL )
		{
			// increment child depth as parent + 1
			if ( ((Person*)(child->pointer))->depth == 0 ) 
				((Person*)(child->pointer))->depth = current->depth + 1;
			else
			{
				child = child->link;
				continue;
			}
			if ( ((Person*)(child->pointer))->depth <= depth )
			{
				enqueue(&queue, createPointer(child->pointer)); // suspect redundancy
				// printf(">>> enqueue %s\n", ((Person*)(child->pointer))->name);
			}
			child = child->link;
			// getchar();
		}
		// printf("\nNo more child\n");
		// getchar();		
	}
	Pointer *cleaner = graph->top;
	while ( cleaner != NULL )
	{
		((Person*)cleaner->pointer)->depth = 0;
		cleaner = cleaner->link;	
	}	
}


void initS(Stack *stack)
{
	stack->count = 0;
	stack->top = NULL;
}

void push(Stack *stack, Pointer *pointer)
{
	pointer->link = stack->top;
	stack->top = pointer;
	stack->count += 1;
}

Pointer *pop(Stack *stack)
{
	Pointer *pointer = stack->top;
	stack->top = stack->top->link;
	stack->count -= 1;
	return pointer;
}

void DFSTraversal(Queue *graph, char name[LEN], int len)
{
	printf(">>>> Depart from %s\n", name);
	Stack stack; initS(&stack);
	Queue visited; initQ(&visited);
	Person *member = getMember(graph, name, len);
	Pointer *state = createPointer(member);
	push(&stack, state);
	while ( stack.count != 0 )
	{
		Person *parent = (Person*)(stack.top->pointer);	
		if ( contains(&visited, parent->name) )
		{
			// printf(">> Visited\n");
			Person *dumb = (Person*)pop(&stack);
			// printf(">> Remove : %s\n", dumb->name );
			continue;
		}
		printf("\n>> Parent : %d %s", parent->depth, parent->name );
		enqueue(&visited, createPointer(parent));
		// expand child
		Pointer *child = parent->friends;
		// printf("\n>> Child :");
		while ( child != NULL )
		{
			// printf("\n>>> %s", ((Person*)(child->pointer))->name );
			if ( ((Person*)(child->pointer))->depth == 0 ) // ((Person*)(child->pointer))->depth == 0
			{
				((Person*)(child->pointer))->depth = parent->depth + 1;
				push(&stack, createPointer(child->pointer));
				// printf(" # Push");
			}
			child = child->link;
			// getchar();
		}
		// printf("\n>> End Of Expansion\n");
	}
	printf("\nEnd Of Travel\n");
	Pointer *cleaner = graph->top;
	while ( cleaner != NULL )
	{
		((Person*)cleaner->pointer)->depth = 0;
		cleaner = cleaner->link;	
	}
}

void DFSSearch(Queue *graph, char caller[LEN], int len1, char receiver[LEN])
{
	Stack path; initS(&path);
	// Queue visited; initQ(&visited);
	Person *Caller = getMember(graph, caller, strlen(caller));
	Pointer *state = createPointer(Caller);
	push(&path, state);
	while ( path.count != 0 )
	{
		Person *parent = (Person*)(path.top->pointer);
		
		if ( strcmp(parent->name, receiver) == 0 )
		{
			printf("\n>> Parent : %s\n", parent->name);
			printf(">> Found\n");
			printf(">>>> Cost : %d\n", parent->depth);
			Pointer *current = path.top;
			Stack s; initS(&s);
			// push(&s, createPointer(parent));
			while ( current != NULL )
			{
				if ( ((Person*)(current->pointer))->type == 'P' )
				{
					// printf("%s >> ", ((Person*)(current->pointer))->name );
					push(&s,createPointer(current->pointer));
				}
				current = current->link;
			}
			push(&s, createPointer(getMember(graph,caller,strlen(caller))));
			current = s.top;
			while ( current != NULL )
			{
				printf("%s >> ", ((Person*)(current->pointer))->name );
				current = current->link;
			}
			return;
		}/*
		if ( contains(&visited, parent->name) == 1 )
		{
			// printf(">> Visited, so pop()\n");
			// parent->type = 'B';
			Person *dumb = (Person*)pop(&path);
			// dumb->type = 'B';
			free(dumb);
			continue;
		}*/
		printf("\n>> Parent : %s\n", parent->name);
		// enqueue(&visited, createPointer(parent));
		Pointer *child = parent->friends;
		int flag = 0;
		// printf("\nChild\n");
		while ( child != NULL )
		{
			// printf("\n>>> %s", ((Person*)(child->pointer))->name);
			if ( ((Person*)(child->pointer))->depth == 0 && strcmp(((Person*)(child->pointer))->name, caller) != 0 ) // ((Person*)(child->pointer))->depth == 0
			{
				((Person*)(child->pointer))->depth = parent->depth + 1;
				((Person*)(child->pointer))->type = 'B';
				push(&path, createPointer(child->pointer));
				// printf(" # Push");
				flag = 1;
			}/*
			else if ( ((Person*)(child->pointer))->depth > parent->depth + 1 ){
				((Person*)(child->pointer))->type = 'B';
				((Person*)(child->pointer))->depth = parent->depth + 1;
				push(&path, createPointer(child->pointer));
				// printf("\n>>> %s", ((Person*)(child->pointer))->name);
				// printf(" # Push");
				flag = 1;
			}*/
			child = child->link;
		}
		if ( flag == 0 )
		{
			// printf(">> No expansion\n");
			printf(">> Dead END\n");
			Person *dumb = (Person*)pop(&path);
			free(dumb);
		}
		((Person*)(path.top->pointer))->type = 'P';
		// printf(">> End Of Expansion\n");
	}
	printf("\nEnd Of Travel\n");
	Pointer *cleaner = graph->top;
	while ( cleaner != NULL )
	{
		((Person*)cleaner->pointer)->depth = 0;
		cleaner = cleaner->link;	
	}
}


