#define LEN 11

int readMember(Queue *queue, char *path)
{
	FILE *fptr;
	char buff[LEN]; char par[LEN];
	int i = 0, j = 0, row = 0, col = 0;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	puts(">> Reading data...");
   	memset(buff, '\0', LEN);
   	while ( fscanf(fptr, "%s", buff) != EOF ) // parent
   	{
   		printf(">> %s\n", buff);
   		enqueue(queue, createPointer(createPerson(buff, strlen(buff))));
		memset(par, '\0', LEN);
		row++;
   	}
	puts("\n>> EOF\n"); 
   	fclose(fptr);
   	return row;
}
int readRelation(Queue *queue, char *path)
{
	FILE *fptr;
	char buff[LEN]; char par[LEN];
	int i = 0, j = 0, row = 0, col = 0;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	puts(">> Reading data...");
   	memset(par, '\0', LEN);
   	while ( fscanf(fptr, "%s", par) != EOF ) // parent
   	{
   		printf("\n>> Parent : %s\n", par);
	   	fscanf(fptr, "%d", &col); // child count
	   	printf(">> Count : %d\n", col);
	   	for ( i = 0; i < col; i++ )
	   	{
	   		memset(buff, '\0', LEN);
	   		fscanf(fptr, "%s", buff); // child
	   		printf(">>> %s ", buff);
	   		addEdge(queue, par, buff, strlen(par), strlen(buff));
		}
		memset(par, '\0', LEN);
		row++;
   	}
	puts("\n>> EOF\n"); 
   	fclose(fptr);
   	return row;
}
