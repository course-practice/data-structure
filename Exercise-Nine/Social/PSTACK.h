

typedef struct TILE{
	void *pointer;
	struct TILE *link;
	char type;
}Tile;

typedef struct PSTACK{
	int count;
	Tile *top;
}Pstack;


Tile *createTile(Person *person, char type)
{
	Tile *tile = (Tile*)malloc(sizeof(Tile));
	if ( tile != NULL )
	{
		tile->link = NULL;
		tile->pointer = person;
		tile->type = type;
	}
	return tile;
}

void initP(Pstack *path)
{
	path->count = 0;
	path->top = NULL;
}


void pushT(Pstack *path, Tile *tile )
{
	tile->link = path->top;
	path->top = tile;
	path->count += 1;
}

Tile *popT(Pstack *path)
{
	Tile *tile = path->top;
	path->top = path->top->link;
	path->count -= 1;
	return tile;
}

int BackTracking(Pstack *path, Queue *visited)
{
	printf("Backtracking >> ");
	while ( path->count != 0 && path->top->type != 'B' )
	{
		Tile *tile = popT(path);
		Pointer *p = visited->top, *prev = NULL;
		while ( p != NULL )
		{
			
			if ( strcmp(((Person*)(p->pointer))->name, ((Person*)(tile->pointer))->name) == 0 )
			{
				
				break;
			}
			prev = p;
			p = p->link;
		}
		if ( prev == NULL )
		{
			visited->top= visited->top->link;
		}
		else
		{
			prev->link = p->link;
			free(p);
		}
		free(tile);
	}
	return path->count;
}
int findPathLeastCost(Queue *graph, char caller[LEN], char receiver[LEN])
{
	Pstack path; initP(&path);
	Queue visited; initQ(&visited);
	int leastCost = INT_MAX, flag = 0;
	Person *parent = getMember(graph, caller, strlen(caller));
	pushT(&path, createTile(parent, 'S'));
	while ( 1 )
	{
		printf(">>");
		parent = (Person*)(path.top->pointer);
		printf(">> Parent %s\n", parent->name);
		if ( contains(&visited, parent->name) == 1 )
		{
			printf(">> Visited so pop()\n");
			free(popT(&path));
			if ( path.count == 0 ) break;
			continue;
		}
		if ( !strcmp(parent->name, receiver) )
		{
			printf(">> Found! at depth : %d\n", parent->depth);
			if ( leastCost > parent->depth )
				leastCost = parent->depth;
			if ( BackTracking(&path, &visited) == 0 ) break;
			else
				path.top->type = 'S';
			continue; 
		}
		enqueue(&visited, createPointer(parent));
		Tile *child = parent->friends;
		flag = 0;
		while ( child != NULL )
		{
			if ( ((Person*)(child->pointer))->depth == 0 || ((Person*)(child->pointer))->depth > parent->depth + 1 )
			{
				((Person*)(child->pointer))->depth = parent->depth + 1;	
			}
			else if ( ((Person*)(child->pointer))->depth + 1 >= leastCost )
			{
				printf(">> Expected cost is too high\n");
				child = child->link;
				continue;
			}
			pushT(&path, createTile(child->pointer, 'B'));	
			flag = 1;
		}
		if ( flag == 0 )
		{
			printf(">> No branch were added\n");
			if ( BackTracking(&path, &visited) == 0 ) break;
		}
		path.top->type = 'S';
	}
	if ( leastCost != INT_MAX ) printf("Congratulation!!!\n");
	return leastCost;	
}



