#include<stdio.h>
#include <string.h>
#include <stdlib.h>

// self define library
#include "STRUCTURE.h"
#include "RF.h"
 
int main(void)
{
	char *member = "member.txt";
	char *relation = "data.txt";
	Queue graph; initQ(&graph);
	int count = readMember(&graph, member);
	readRelation(&graph, relation);
	// 
	char departure[LEN];
	memset(departure, '\0', LEN);
	scanf("%s", departure);
	while ( contains(&graph, departure) == 0 )
	{
		printf(">> Exception : 404 %s NOT FOUND\n", departure);
		memset(departure, '\0', LEN);
		scanf("%s", departure);
	}
	printf(">> BFS Traversal\n");
	BFSTraversalByDepth(&graph, departure, strlen(departure), count);
	getchar();
	//
	printf(">> BFS Traversal, Depth = 2\n");
	memset(departure, '\0', LEN);
	scanf("%s", departure);
	while ( contains(&graph, departure) == 0 )
	{
		printf(">> Exception : 404 %s NOT FOUND\n", departure);
		memset(departure, '\0', LEN);
		scanf("%s", departure);
	} 
	BFSTraversalByDepth(&graph, departure, strlen(departure), 2);
	//
	printf(">> DFS Traversal\n");
	memset(departure, '\0', LEN);
	scanf("%s", departure);
	while ( contains(&graph, departure) == 0 )
	{
		printf(">> Exception : 404 %s NOT FOUND\n", departure);
		memset(departure, '\0', LEN);
		scanf("%s", departure);
	} 
	DFSTraversal(&graph, departure, strlen(departure));
	//
	printf(">> DFS Search\n");
	char caller[LEN]; memset(caller,'\0', LEN);
	scanf("%s", caller);
	while ( contains(&graph, caller) == 0 )
	{
		printf(">> Exception : 404 %s NOT FOUND\n", caller);
		memset(caller, '\0', LEN);
		scanf("%s", caller);
	} 
	char receiver[LEN]; memset(receiver,'\0', LEN);
	scanf("%s", receiver);
	while ( contains(&graph, receiver) == 0 )
	{
		printf(">> Exception : 404 %s NOT FOUND\n", receiver);
		memset(receiver, '\0', LEN);
		scanf("%s", receiver);
	} 
	DFSSearch(&graph, caller, strlen(caller), receiver);

	return 0;
}

