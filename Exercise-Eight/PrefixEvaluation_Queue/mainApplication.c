#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 512
#define ROW 10 
// self define library
#include "QUEUE.h"
#include "RF.h"
#include "CIRCULAR.h"
// function prototype
double solveQueue(Queue *queue1, Queue *queue2);
double solveChain(Circle *circle);
// main program
int main(void)
{
	printf("Program Started\n");
	char *path = "data.txt";
	char expression[ROW][LEN] = { 0 };
	double result[ROW] = { 0 };
	int rowCount = fgetsData(expression, path), row;
	Queue queue1, queue2; initQ(&queue1); initQ(&queue2);
	Circle circle; initC(&circle);
	printf(">> Row Count : %d\n", rowCount);
	for ( row = 0; row < rowCount; row++ )
	{
		printf("\n\n@Solve Q%d\n", row+1);
		// int outcome = buildQueue(expression[row], &queue1);
		int outcome = buildChain(expression[row], &circle);
		// printQueue(&queue1);
		printChain(&circle);
		// result[row] = solveQueue(&queue1, &queue2);
		result[row] = solveChain(&circle);
		// destroyQueue(&queue1);	destroyQueue(&queue2);
		// initQ(&queue1);	initQ(&queue2);
		initC(&circle);	
	}
	puts("ENDED");
	for ( row = 0; row < rowCount; row++ )
	{
		if ( result[row] == INT_MAX )
			printf(">> Exception\n");
		else
			printf(">> Result : %3.4lf\n", result[row] );
	}
	return 0;
} 

double solveQueue(Queue *queue1, Queue *queue2)
{
	printf("SolveQueue\n");
	Token *current = queue1->sp;
	double A, B;
	char OP;
	while ( current != NULL )
	{
		printf(">> Current %lf OP : %c\n", current->value, current->OP );
		if ( current->OP == 'N' )
			enqueue( queue2, createToken(dequeue(queue1), 'N') );
		else
		{
			if ( queue1->count >= 3 && current->link->OP == 'N' && current->link->link->OP == 'N' )
			{
				OP = dequeue(queue1);
				A = dequeue(queue1);
				B = dequeue(queue1);
				printf(">>> Solving : %lf %c %lf\n", A, OP, B);
				if ( OP == 43 )
					A += B;
				else if ( OP == 45 )
					A -= B;
				else if ( OP == 42 )
					A *= B;
				else if ( OP == 47 )
				{
					if ( B == 0 ) 
					{
						printf(">>>> Exception : Attempt to div by ZERO!!!\n");
						return INT_MAX;
					}
					A /= B;
				}
				printf(">>>> A : %lf\n", A );
				enqueue(queue2, createToken(A, 'N'));
			}
			else
			{
				printf(">>>> Pushing operator\n");
				enqueue(queue2, createToken(dequeue(queue1), 'O'));
			}
		}
		current = queue1->sp;
	}
	if ( queue2->count != 1 )
		return solveQueue(queue2, queue1);
	else
		return dequeue(queue2);	
}

double solveChain(Circle *circle)
{
	printf("SolveChain\n");
	Pearl *marker = circle->ep;
	Pearl *walker = marker->link;
	double A, B; char OP;
	while ( circle->count != 1 )
	{
		printf(">> Current %lf, OP : %c\n", walker->value, walker->OP );
		if ( walker->OP == 'N' )
			circle->ep = walker;
		else 
		{
			printf(">>> OP Code : %c\n", (char)walker->value );
			if ( circle->count >= 3 && walker->link->OP == 'N' && walker->link->link->OP == 'N' )
			{
				OP = unplug(circle);
				A = unplug(circle);
				B = unplug(circle);
				printf(">> Solving : %lf %c %lf\n", A, OP, B);
				if ( OP == 43 )
					A += B;
				else if ( OP == 45 )
					A -= B;
				else if ( OP == 42 )
					A *= B;
				else if ( OP == 47 )
				{
					if ( B == 0 )
					{
						printf(">>>> Exception : Attemp to devide by ZERO!!!\n");
						return 0;
					}
					A /= B;
				}
				printf(">>>> A : %lf\n", A);
				insert(circle, createPearl(A, 'N'));
			}
			else
				circle->ep = walker;
		}
		walker = circle->ep->link;
	}
	return unplug(circle);
}
