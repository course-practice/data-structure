#define LEN 512
#define ROW 10 

int fgetsData(char expression[ROW][LEN], char *path)
{
	FILE *fptr;
	char byte; int i = 0, j = 0, row = 0;
	if ((fptr = fopen(path,"r")) == NULL){
       printf("Error! opening file");
       return 0;
   	}
   	puts(">> Reading data...");
   	while ( fgets(expression[row],LEN,fptr) != NULL ) row++;
   	/*
   	{
   		puts(expression[row]);
   		row++;
	}
	*/
   	fclose(fptr);
   	return row;
}


