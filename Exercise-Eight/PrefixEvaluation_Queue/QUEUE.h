typedef struct TOKEN{
	double value;
	int OP;
	struct TOKEN *link;
}Token;

typedef struct QUEUE{
 	Token *sp, *ep;
 	int count;
}Queue;

void initQ(Queue *queue)
{
	queue->count = 0;
	queue->sp = NULL;
	queue->ep = NULL;
}

Token *createToken(double value, int op)
{
	Token *token = (Token*)malloc(sizeof(Token));
	if ( token != NULL )
	{
		token->link = NULL;
		token->OP = op;
		token->value = value;
	}
	return token;
}

void enqueue(Queue *queue, Token *token)
{
	if ( queue->count == 0 )
	{
		queue->sp = token;
	}
	else
	{
		queue->ep->link = token;
	}
	queue->ep = token;
	queue->count += 1;
}

double dequeue(Queue *queue)
{
	if ( queue->count == 0 ) return INT_MAX;
	Token *token = queue->sp;
	if ( queue->count == 1 )
	{
		queue->sp = NULL;
		queue->ep = NULL;
	}
	else
		queue->sp = queue->sp->link;
	double v = token->value;
	queue->count -= 1;
	free(token);
	return v;
}
void printQueue(Queue *queue)
{
	Token *current = queue->sp;
	while ( current != NULL )
	{
		if ( current->OP == 'N' )
			printf("%lf >> ", current->value );
		else
			printf("%c >> ", (char)current->value );
		current = current->link;
	}
	printf("|| EOQ\n");
}
void destroyQueue(Queue *queue)
{
	while ( queue->count != 0 )
	{
		printf(">> >> dequeue : %lf\n", dequeue(queue) );
	}
}

int buildQueue( char expression[LEN], Queue *queue1 )
{
	puts(">> Build Queue");
	int i; char op;
	for ( i = 0; i < strlen(expression); i++ )
	{
		if ( expression[i] == 32 ) 
		{
			// printf(">> SPACE SKIP\n");
			continue;	
		}
		if ( expression[i] == 10 )
		{
			// printf(">> LF SKIP\n");
			continue;
		}
		op = 'N';
		printf("%c >> ", expression[i] );
		if ( expression[i] >= 48 && expression[i] <= 57 )
			expression[i] -= '0';
		else
			op = 'O';
		enqueue(queue1, createToken(expression[i], op)); // distinguish op and operand;
	} puts("");
	return 1;
}

