typedef struct PEARL{
	int OP;
	double value;
	struct PEARL *link;
}Pearl;
typedef struct CIRCLE{
	Pearl *ep;
	int count;
}Circle;

void initC(Circle *circle)
{
	circle->count = 0;
	circle->ep = NULL;
}

Pearl *createPearl(double value, int op)
{
	Pearl *pearl = (Pearl*)malloc(sizeof(Pearl));
	if ( pearl != NULL )
	{
		pearl->link = NULL;
		pearl->OP = op;
		pearl->value = value;
	}
	return pearl;
}

void insert(Circle *circle, Pearl *pearl) // query to insert directly to the head
{
	if ( circle->count == 0 )
	{
		pearl->link = pearl;
	}
	else
	{
		pearl->link = circle->ep->link;
		circle->ep->link = pearl;
	}
	circle->ep = pearl;
	circle->count += 1;
}

double unplug(Circle *circle)
{
	Pearl *pearl = circle->ep->link;
	if ( circle->count == 1 )
	{
		circle->ep = NULL;
	}
	else
	{
		circle->ep->link = circle->ep->link->link;
	}
	circle->count -= 1;
	double value = pearl->value;
	free(pearl);
	return value;
}

int buildChain( char expression[LEN], Circle *circle )
{
	puts(">> Build Chain");
	int i; char op;
	for ( i = 0; i < strlen(expression); i++ )
	{
		if ( expression[i] == 32 || expression[i] == 10 )
			continue;
		op = 'N';
		printf("%c >> ", expression[i]);
		if ( expression[i] >= 48 && expression[i] <= 57 )
			expression[i] -= '0';
		else
			op = 'O';
		insert(circle, createPearl(expression[i], op));
	} puts("");
	return 1;
}

void printChain(Circle *circle)
{
	Pearl *marker = circle->ep;
	Pearl *walker = marker->link;
	int flag = 0;
	printf("{ ");
	while ( !flag )
	{
		if ( walker == marker ) flag = 1;
		if ( walker->OP == 'N' )
			printf("%3.4lf >> ", walker->value );
		else
			printf("%c >> ", (char)walker->value );
		walker = walker->link;
	} puts(" }");
}

